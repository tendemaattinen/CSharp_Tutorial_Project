# C# Tutorial Project

## Purpose of this software

C# Tutorial Project is a command line software which purpose is to teach its maker to learn C# language.
It contains function from basic 'Hello World' to storage of household expenses . Function ideas are from
application named "Exercises C#" by "Learn to Program". Application is available at https://play.google.com/store/apps/details?id=juanantonioripoll.practiceexercisescsharp&hl=en
