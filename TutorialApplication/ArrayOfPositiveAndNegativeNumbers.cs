﻿using System;

namespace TutorialApplication
{
    class ArrayOfPositiveAndNegativeNumbers
    {
        /* Application takes ten numbers from user, saves them to array and in 
        *  the end shows average of negative and positive numbers in array
        */
        public void Main()
        {
            Console.WriteLine();
            int[] numberArray = CreateArray(); // Creates 
            Console.WriteLine();
            CheckNegativesFromNumbersArray(numberArray); // Checks average of negative numbers in array
            CheckPositivesFromNumbersArray(numberArray); // Checks average of positive numbers in array
            
        }

        // Function which asks numbers from user and saves them to array, returns int array
        private int[] CreateArray()
        {
            int[] array = new int[10]; // Array to storage numbers
            for (int i = 0; i < 10; i++) // For-loop to asks numbers ten times
            {
                Console.Write("Enter a number {0}: ", i + 1);
                Int32.TryParse(Console.ReadLine(), out int number);
                array[i] = number;
            }
            return array;
        }

        // Checks average of negative numbers
        private void CheckNegativesFromNumbersArray(int[] array)
        {
            int sumOfNegatives = 0; // Sum of negative
            int numberOfNegatives = 0; // Number of negative
            foreach (int j in array) // Loop for going through the array
            {
                if (j < 0) // Checks if number is negative
                {
                    sumOfNegatives += j;
                    numberOfNegatives += 1;
                }
            }
            int averageOfNegatives = sumOfNegatives / numberOfNegatives; // Average of numbers
            Console.WriteLine("Average of negative numbers: {0}", averageOfNegatives);
        }

        // Checks average of npositive numbers
        private void CheckPositivesFromNumbersArray(int[] array)
        {
            int sumOfPositives = 0; // Sum of positives
            int numberOfPositives = 0; // Number of positives
            foreach (int j in array) // Loop for going through the array
            {
                if (j >= 0) // Checks if number is positive
                {
                    sumOfPositives += j;
                    numberOfPositives += 1;
                }
            }
            int averageOfPositives = sumOfPositives / numberOfPositives; // Average of numbers
            Console.WriteLine("Average of positive numbers: {0}", averageOfPositives);
        }
    }
}