﻿using System;

namespace TutorialApplication
{
    class Exceptions2
    {
        // Application asks two numbers from user and divides them. Aplication checks with try-catch if user
        // try to divide by zero and in that case, it will give error message.
        public void Main()
        {
            while(true)
            {
                Console.WriteLine();
                Console.Write("Enter the first number or '0' to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number1);
                // Checks if user wants to quit by giving '0'
                if (number1 == 0)
                {
                    break;
                }
                Console.Write("Enter the second number: ");
                Int32.TryParse(Console.ReadLine(), out int number2);
                // Try-catch if user wants to divide by zero
                try
                {
                    int answer = number1 / number2;
                    Console.WriteLine("{0} / {1} = {2}", number1, number2, answer);
                }
                catch (DivideByZeroException)
                {
                    Console.WriteLine("Can't divide by zero!");
                }
            }
        }
    }
}