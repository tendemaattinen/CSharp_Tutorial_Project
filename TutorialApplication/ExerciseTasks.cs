﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Program that can store 2000 "to-do" tasks.
 */
 
using System;

namespace TutorialApplication
{
    class ExerciseTasks
    {
        // Struct for tasks
        struct Task
        {
            public Date date;
            public string description;
            public int levelOfImportance;
            public string category;
        }

        // Nested struct for date
        struct Date
        {
            public int day;
            public int month;
            public int year;
        }

        private readonly Task[] taskArray = new Task[2000]; // Array to store tasks
        private int countOfTasksInArray = 0; // Count of tasks in the array

        // Main function
        public void Main()
        {
            bool swi = true; // Boolean value for looping program
            ExampleArray(); // Example array
            while (swi) // Loop for program
            {
                PrintMenu(); // Prints menu
                Console.Write("Enter your choice: ");
                string userinput = Console.ReadLine();
                swi = ExecuteAction(userinput); // Execute action wanted by user
            }
            
        }

        // Function to print main menu for application
        private void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("*** Main menu ***");
            Console.WriteLine("1) Add new task");
            Console.WriteLine("2) Show all tasks");
            Console.WriteLine("3) Show tasks between certain dates");
            Console.WriteLine("4) Find tasks that contain certain text");
            Console.WriteLine("5) Update task");
            Console.WriteLine("6) Delete task");
            Console.WriteLine("7) Sort tasks alphabetically by date");
            Console.WriteLine("8) Find duplicates");
            Console.WriteLine("0) Exit application");
        }

        // Function to execute users wanted action, return boolean value, which is defaulted to true.
        // In case user wants to quit, boolean value is returned as false.
        private bool ExecuteAction(string input)
        {
            bool swi = true; // Boolean value for case that user wants to quit
            switch (input)
            {
                case "1":
                    AddNewTask();
                    break;
                case "2":
                    ShowTasks();
                    break;
                case "3":
                    ShowTasksBetweenDates();
                    break;
                case "4":
                    FindTask();
                    break;
                case "5":
                    UpdateTask();
                    break;
                case "6":
                    DeleteTask();
                    break;
                case "7":
                    SortByDate();
                    break;
                case "8":
                    FindDuplicates();
                    break;
                // Exit cases
                case "quit":
                case "Quit":
                case "exit":
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine();
                    Console.WriteLine("Invalid input");
                    break;
            }
            return swi;
        }

        // Function to add new task to task list
        private void AddNewTask()
        {
            // Checks if database is already full
            if (countOfTasksInArray >= 2000)
            {
                Console.WriteLine();
                Console.WriteLine("Task list is full!");
            }
            else
            {
                Task tempTask = new Task(); // Creates new empty task

                // Description
                do
                {
                    Console.Write("Enter a description: ");
                    tempTask.description = Console.ReadLine();
                } while (tempTask.description.Length == 0);

                // Level of importance
                do
                {
                    Console.Write("Enter a level of importance (1-10): ");
                    int.TryParse(Console.ReadLine(), out tempTask.levelOfImportance);
                } while (tempTask.levelOfImportance < 1 || tempTask.levelOfImportance > 10);

                // Category
                do
                {
                    Console.Write("Enter a category: ");
                    tempTask.category = Console.ReadLine();
                } while (tempTask.category.Length == 0);

                // Day
                do
                {
                    Console.Write("Enter a day: ");
                    int.TryParse(Console.ReadLine(), out tempTask.date.day);
                } while (tempTask.date.day < 1 || tempTask.date.day > 31);

                // Month
                do
                {
                    Console.Write("Enter a month: ");
                    int.TryParse(Console.ReadLine(), out tempTask.date.month);
                } while (tempTask.date.month < 1 || tempTask.date.month > 12);

                // Year
                do
                {
                    Console.Write("Enter a year: ");
                    int.TryParse(Console.ReadLine(), out tempTask.date.year);
                } while (tempTask.date.year < 1000 || tempTask.date.year > 3000);

                taskArray[countOfTasksInArray] = tempTask; // Adds created task to array
                countOfTasksInArray += 1; // Adds one to count of tasks in the array

                Console.WriteLine("New task added.");

            }
        }

        // Function to show all tasks in task list
        private void ShowTasks()
        {
            Console.WriteLine();
            // Checks if task list is empty
            if (countOfTasksInArray <= 0)
            {
                Console.WriteLine("Task list is empty!");
            }
            else
            {
                for (int i = 0; i < countOfTasksInArray; i++) // Loop for going through the array
                {
                    Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", i + 1, taskArray[i].date.day,
                            taskArray[i].date.month, taskArray[i].date.year, taskArray[i].description, taskArray[i].category, taskArray[i].levelOfImportance);
                }
            }
        }

        // Function to show task between dates
        private void ShowTasksBetweenDates()
        {
            Console.WriteLine();
            // Checks if task list is empty
            if (countOfTasksInArray <= 0)
            {
                Console.WriteLine("Task list is empty!");
            }
            else
            {
                Console.Write("Staring date (xx.xx.xxxx) or leave empty: ");
                string startingDate = Console.ReadLine();
                bool startDateValidity = false; // Boolean value for starting date validity
                bool endDateValidity = false; // Boolean value for ending date validity
                int startingDay = 0, startingMonth = 0, startingYear = 0, endingDay = 0, endingMonth = 0, endingYear = 0;
                if (startingDate == "") // If starting date is empty, sets today as starting sate
                {
                    startingDay = DateTime.Now.Day;
                    startingMonth = DateTime.Now.Month;
                    startingYear = DateTime.Now.Year;
                    startDateValidity = true;
                }
                else
                {
                    (startingDay, startingMonth, startingYear, startDateValidity) = SplitDate(startingDate); // Creates starting date
                }

                if (startDateValidity) // If start date is valid
                {
                    Console.Write("Ending date (xx.xx.xxxx): ");
                    string endingDate = Console.ReadLine();
                    (endingDay, endingMonth, endingYear, endDateValidity) = SplitDate(endingDate); // Creates ending date
                }

                if (endDateValidity) // If ending date is valid
                {
                    int counter = 0; // Counter
                    Console.WriteLine();
                    for (int i = 0; i < countOfTasksInArray; i++) // Loop for going through the array
                    {
                        counter += 1;
                        PrintTasksBetweenDates(startingDay, startingMonth, startingYear, endingDay, endingMonth, endingYear, taskArray[i].date.day, taskArray[i].date.month, taskArray[i].date.year, taskArray[i], counter);
                    }

                    if (counter == 0) // No tasks was found between given dates
                    {
                        Console.WriteLine("No tasks found between given dates!");
                    }
                }
            }
        }

        // Function to search and print tasks between dates
        private void PrintTasksBetweenDates(int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear, int dayInTask, int monthInTask, int yearInTask, Task task, int counter)
        {
            // Year is between starting and ending years
            if (yearInTask > startYear && yearInTask < endYear)
            {
                Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
            }
            // Year is same as starting year but smaller than ending year
            else if (yearInTask == startYear && yearInTask < endYear)
            {
                // Month is bigger than starting month
                if (monthInTask > startMonth)
                {
                    Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                    monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                }
                // Month is the same as starting month
                else if (monthInTask == startMonth)
                {
                    // Day is the same or bigger than starting day
                    if (dayInTask >= startDay)
                    {
                        Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                        monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                    }
                }
            }
            // Year is bigger than starting year and same as ending year
            else if (yearInTask > startYear && yearInTask == endYear)
            {
                // Month is smaller than ending month
                if (monthInTask < endMonth)
                {
                    Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                    monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                }
                // Month is the same as ending month
                else if (monthInTask == endMonth)
                {
                    // Day is same or smaller than ending day
                    if (dayInTask <= endDay)
                    {
                        Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                        monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                    }
                }
            }
            // Year is same as starting and ending years
            else if (yearInTask == startYear && yearInTask == endYear)
            {
                // Month is between starting and ending months
                if (monthInTask > startMonth && monthInTask < endYear)
                {
                    Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                    monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                }
                // Month is same as starting month and smaller than ending month
                else if (monthInTask == startMonth && monthInTask < endMonth)
                {
                    // Day is bigger than starting day
                    if (dayInTask >= startDay)
                    {
                        Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                        monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                    }
                }
                // Month is bigger than starting month and same as ending month
                else if (monthInTask > startMonth && monthInTask == endMonth)
                {
                    // Day is smaller than ending day
                    if (dayInTask <= endDay)
                    {
                        Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                        monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                    }
                }
                // Month is the same as staring and ending months
                else if (monthInTask == startMonth && monthInTask == endMonth)
                {
                    // Day is between starting and ending days
                    if (dayInTask >= startDay && dayInTask <= endDay)
                    {
                        Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", counter, dayInTask,
                        monthInTask, yearInTask, task.description, task.category, task.levelOfImportance);
                    }
                }
            }
        }

        // Function to split dates from format xx.xx.xxxx
        private (int, int, int, bool) SplitDate(string date)
        {
            int day = 0, month = 0, year = 0; //Placeholder values
            bool validity = true; // Boolean value for dates validity, will be returned, standard 'false'
            string[] dateArray = date.Split('.'); // Spits string
            if (dateArray.Length == 3) // If given date is right lenght
            {
                day = Convert.ToInt32(dateArray[0]);
                month = Convert.ToInt32(dateArray[1]);
                year = Convert.ToInt32(dateArray[2]);
                (bool dateCheck, string errorMessage) = CheckDateValidity(day, month, year); // Checks if date is valid
                if (!dateCheck) // If date isn't valid
                {
                    Console.WriteLine("{0}", errorMessage);
                    validity = false;
                }
            }
            else // Format of date is wrong
            {
                Console.WriteLine("Date is in wrong format!");
                validity = false;
            }

            return (day, month, year, validity);
        }

        // Function to check validity of the given date
        private (bool, string) CheckDateValidity(int day, int month, int year)
        {
            string errorM = ""; // Placeholer error message
            bool validity = true; // Boolean value for validity
            // Checks if day is between 1 and 31
            if (day < 1 || day > 31)
            {
                validity = false;
                errorM = "Day is invalid!";
            }
            // Checks if month is between 1 and 12
            else if (month < 1 || month > 12)
            {
                validity = false;
                errorM = "Month is invalid!";
            }
            // Checks if year is between 1000 and 3000
            else if (year < 1000 || year > 3000)
            {
                validity = false;
                errorM = "Year is invalid!";
            }
            return (validity, errorM);
        }

        // Function to find tasks with part of the text
        private void FindTask()
        {
            bool found = false;
            Console.WriteLine();
            // Checks if task list is empty
            if (countOfTasksInArray <= 0)
            {
                Console.WriteLine("Task list is empty!");
            }
            else
            {
                Console.Write("Enter a part of category or description: ");
                string userInput = Console.ReadLine().ToLower();
                Console.WriteLine();
                for (int i = 0; i < countOfTasksInArray; i++) // Loop for going through the array
                {
                    string category = taskArray[i].category.ToLower(); // Category of task in array
                    string description = taskArray[i].description.ToLower(); // Description of task in array
                    if (category.Contains(userInput)
                    || description.Contains(userInput)) // Checks if category or description contains user input
                    {
                        found = true;
                        Console.WriteLine("{0}) {1}.{2}.{3} - {4} - {5} - {6}", i + 1, taskArray[i].date.day,
                        taskArray[i].date.month, taskArray[i].date.year, taskArray[i].description, taskArray[i].category, taskArray[i].levelOfImportance);
                    }
                }

                if (!found) // User input was not found from task list
                {
                    Console.WriteLine("'{0}' not found from task list!", userInput);
                }
            }
        }

        // Function to update task
        private void UpdateTask()
        {
            Console.WriteLine();
            if (countOfTasksInArray <= 0) // Checks if array is empty
            {
                Console.WriteLine("Task list is empty!");
            }
            else
            {
                Console.Write("Enter the number of the task: ");
                int.TryParse(Console.ReadLine(), out int numberOfTask);
                if (numberOfTask > 0 && numberOfTask <= countOfTasksInArray) // Checks if user input is bigger than 0 and smaller than count of tasks in task list
                {
                    Console.Write("Task: {0} (y/n): ", taskArray[numberOfTask - 1].description); // Asks confirmation from user
                    string answer = Console.ReadLine();
                    Console.WriteLine();
                    switch (answer)
                    {
                        // User wants to update task
                        case "y":
                        case "Y":
                        case "yes":
                        case "Yes":
                        case "YES":
                            Console.WriteLine("Enter a new value or press 'Enter' to leave value unchanged");
                            // Update description
                            Console.Write("Description: {0} | New value: ", taskArray[numberOfTask - 1].description);
                            string newDescription = Console.ReadLine();
                            if (newDescription != "") // Checks if new description is not empty
                            {
                                taskArray[numberOfTask - 1].description = newDescription;
                            }
                            // Update category
                            Console.Write("Category: {0} | New value: ", taskArray[numberOfTask - 1].category);
                            string newCategory = Console.ReadLine();
                            if (newCategory != "") // Checks if new category is not empty
                            {
                                taskArray[numberOfTask - 1].category = newCategory;
                            }
                            // Update level of importance
                            Console.Write("Level of importance: {0} | New value: ", taskArray[numberOfTask - 1].levelOfImportance);
                            string newLevelOfImportance = Console.ReadLine();
                            if (newLevelOfImportance != "") // Checks if new level of inportance is not empty
                            {
                                int.TryParse(newLevelOfImportance, out int newLoI);
                                if (newLoI <= 10 && newLoI >= 1)
                                {
                                    taskArray[numberOfTask - 1].levelOfImportance = newLoI;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input, value not changed!");
                                }
                            }
                            // Update day
                            Console.Write("Day: {0} | New Value: ", taskArray[numberOfTask - 1].date.day);
                            string newDay = Console.ReadLine();
                            if (newDay != "") // Checks if new day is not empty
                            {
                                int.TryParse(newDay, out int newD);
                                if (newD >= 1 && newD <= 31)
                                {
                                    taskArray[numberOfTask - 1].date.day = newD;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input, value not changed!");
                                }
                            }
                            // Update month
                            Console.Write("Month: {0} | New Value: ", taskArray[numberOfTask - 1].date.month);
                            string newMonth = Console.ReadLine();
                            if (newMonth != "") // Checks if new month is not empty
                            {
                                int.TryParse(newMonth, out int newM);
                                if (newM >= 1 && newM <= 12)
                                {
                                    taskArray[numberOfTask - 1].date.month = newM;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input, value not changed!");
                                }
                            }
                            // Update year
                            Console.Write("Year: {0} | New Value: ", taskArray[numberOfTask - 1].date.year);
                            string newYear = Console.ReadLine();
                            if (newYear != "") // Checks if new year is not empty
                            {
                                int.TryParse(newYear, out int newY);
                                if (newY >= 1000 && newY <= 3000)
                                {
                                    taskArray[numberOfTask - 1].date.year = newY;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input, value not changed!");
                                }
                            }
                            Console.WriteLine();
                            Console.WriteLine("Task updated!");
                            break;
                        // User doesn't want to update task
                        case "n":
                        case "N":
                        case "no":
                        case "No":
                        case "NO":
                            Console.WriteLine("Task updating aborted!");
                            break;
                        default:
                            Console.WriteLine("Invalid input");
                            break;
                    }
                }
                else // Given number is out of bound
                {
                    Console.WriteLine("Number out of bound!");
                }
            }
        }

        // Function to delete tasks between two positions or single task
        private void DeleteTask()
        {
            Console.WriteLine();
            if (countOfTasksInArray <= 0) // Checks if array is empty
            {
                Console.WriteLine("Task list is empty!");
            }
            else
            {
                Console.WriteLine("1) Delete single task");
                Console.WriteLine("2) Delete tasks between positions");
                Console.WriteLine("0) Back");
                Console.Write("Enter: ");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1": // User wants to delete single task
                        Console.WriteLine();
                        Console.Write("Enter a placement: ");
                        int.TryParse(Console.ReadLine(), out int placementNumber);
                        if (placementNumber > 0 && placementNumber <= countOfTasksInArray)
                        {
                            Console.Write("{0}) {1} | Continue (y/n): ", placementNumber, taskArray[placementNumber - 1].description); // Asks confirmation from user
                            string choice2 = Console.ReadLine();
                            switch (choice2)
                            {
                                case "y":
                                case "Y":
                                case "yes":
                                case "Yes":
                                case "YES":
                                    for (int i = 0; i < countOfTasksInArray; i++) // For-loop for going through the array
                                    {
                                        if (i >= placementNumber) // If number is bigger than number we want to remove
                                        {
                                            taskArray[i - 1] = taskArray[i]; // Moves programs in the array one place backwards
                                        }
                                    }
                                    countOfTasksInArray -= 1; // Removes one from lenght of the array
                                    Console.WriteLine("Task deleted.");
                                    break;
                                case "n":
                                case "N":
                                case "no":
                                case "No":
                                case "NO":
                                    Console.WriteLine("Deletion of task aborted!");
                                    break;
                                default:
                                    Console.WriteLine("Invalid input!");
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Number out of boundary!");
                        }
                        break;
                    case "2": // User wants to delete multiple tasks
                        Console.WriteLine();
                        Console.Write("Enter a start placement: ");
                        int.TryParse(Console.ReadLine(), out int startNumber);
                        if (startNumber < 1 || startNumber > countOfTasksInArray) // Checks validity of starting number
                        {
                            Console.WriteLine("Number out of boundary!");
                            break;
                        }
                        Console.Write("Enter a ending placement: "); 
                        int.TryParse(Console.ReadLine(), out int endingNumber);
                        if (endingNumber < 1 || endingNumber > countOfTasksInArray) // Checks validity of ending number
                        {
                            Console.WriteLine("Number out of boundary!");
                            break;
                        }
                        else if (endingNumber <= startNumber) // Checks that starting number is smaller than ending number
                        {
                            Console.WriteLine("Ending placement have to be bigger than starting placement!");
                            break;
                        }
                        Console.WriteLine();
                        Console.WriteLine("Tasks to remove:");
                        // Prints tasks that are to be removed
                        for (int i = startNumber - 1; i < endingNumber; i++)
                        {
                            Console.WriteLine("{0}) {1}", i + 1, taskArray[i].description);
                        }
                        Console.Write("Continue (y/n):"); // Asks confirmation from user
                        string choice3 = Console.ReadLine();
                        switch (choice3)
                        {
                            // User wants to delete tasks
                            case "y":
                            case "Y":
                            case "yes":
                            case "Yes":
                            case "YES":
                                int differenceBetweenNumber = (endingNumber - startNumber) + 1;
                                for (int i = startNumber - 1; i <= countOfTasksInArray; i++) // For-loop for going through the array
                                {
                                    taskArray[i] = taskArray[i + differenceBetweenNumber]; // Moves tasks in the array backwards
                                }
                                countOfTasksInArray -= differenceBetweenNumber; // Removes one from lenght of the array
                                Console.WriteLine("Tasks deleted.");
                                break;
                            // User doesn't want to delete tasks
                            case "n":
                            case "N":
                            case "no":
                            case "No":
                            case "NO":
                                Console.WriteLine("Deletion of task aborted!");
                                break;
                            default:
                                Console.WriteLine("Invalid input!");
                                break;
                        }
                        break;
                    case "0": // User wants to go back to main menu
                        break;
                    default:
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        // Function to sort tasks in array by date
        private void SortByDate()
        {
            Console.WriteLine();
            if (countOfTasksInArray <= 0) // Checks if array is empty
            {
                Console.WriteLine("Task list is empty!");
            }
            else
            {
                for (int i = (countOfTasksInArray - 1); i >= 0; i--) // Loop to go through array backwards
                {
                    for (int j = 1; j <= i; j++)
                    {
                        if (taskArray[j - 1].date.year == taskArray[j].date.year) // Checks if value of the years are same
                        {
                            if (taskArray[j - 1].date.month == taskArray[j].date.month) // Checks if value of the months are same
                            {
                                if (taskArray[j - 1].date.day > taskArray[j].date.day) // If previous day in array is bigger, changes places of tasks in the array
                                {
                                    Task tempNumber = taskArray[j - 1];
                                    taskArray[j - 1] = taskArray[j];
                                    taskArray[j] = tempNumber;
                                }
                            }
                            else if (taskArray[j - 1].date.month > taskArray[j].date.month) // If previous month in array is bigger, changes places of tasks in the array
                            {
                                Task tempNumber = taskArray[j - 1];
                                taskArray[j - 1] = taskArray[j];
                                taskArray[j] = tempNumber;
                            }
                        }
                        else if (taskArray[j - 1].date.year > taskArray[j].date.year) // If previous year in array is bigger, changes places of tasks in the array
                        {
                            Task tempNumber = taskArray[j - 1];
                            taskArray[j - 1] = taskArray[j];
                            taskArray[j] = tempNumber;
                        }
                    }
                }
                Console.WriteLine("Database sorted.");
            }
        }

        private void FindDuplicates()
        {
            int[] skipNumbers = new int[2000];
            int lenghtOfSkipNumbersArray = 0;
            bool skip;
            Console.WriteLine();
            if (countOfTasksInArray <= 0) // Checks if array is empty
            {
                Console.WriteLine("Task list is empty!");
            }
            else
            {
                for (int i = 0; i < countOfTasksInArray; i++) // Loop for going through the array
                {
                    skip = false; // Sets boolean values for skip to false
                    for (int k = 0; k < lenghtOfSkipNumbersArray; k++) // Second loop for checking if number is in skip list
                    {
                        if (skipNumbers[k] == i) // Checks if number is in skip list
                        {
                            skip = true;
                        }
                    }
                    if (!skip) // If number doesn't exist in skip list
                    {
                        for (int j = 0; j < countOfTasksInArray; j++)
                        {
                            if (i != j)
                            {
                                if (taskArray[i].description == taskArray[j].description) // Ckecks if descriptions are same
                                {
                                    Console.WriteLine("{0}.and {1}. are same tasks: '{2}'", i + 1, j + 1, taskArray[i].description);
                                    skipNumbers[lenghtOfSkipNumbersArray] = j; // Add number to skip list
                                    lenghtOfSkipNumbersArray += 1; // Adds one to lenght of the skip list
                                }
                            }
                        }
                    }
                }
                if (lenghtOfSkipNumbersArray == 0) // There are no duplicates
                {
                    Console.WriteLine("No matches found!");
                }
            }
        }

        // Function to create example task list
        private void ExampleArray()
        {
            Task task1 = new Task()
            {
                description = "Clean",
                levelOfImportance = 9,
                category = "Homework",
            };
            task1.date.day = 13;
            task1.date.month = 9;
            task1.date.year = 2018;

            taskArray[0] = task1;
            countOfTasksInArray += 1;

            Task task2 = new Task()
            {
                description = "Read a book",
                levelOfImportance = 1,
                category = "Free time",
            };
            task2.date.day = 20;
            task2.date.month = 9;
            task2.date.year = 2020;

            taskArray[1] = task2;
            countOfTasksInArray += 1;

            Task task3 = new Task()
            {
                description = "Watch a movie",
                levelOfImportance = 6,
                category = "Free time",
            };
            task3.date.day = 1;
            task3.date.month = 1;
            task3.date.year = 2020;

            taskArray[2] = task3;
            countOfTasksInArray += 1;

            Task task4 = new Task()
            {
                description = "Watch a movie",
                levelOfImportance = 6,
                category = "Free time",
            };
            task4.date.day = 2;
            task4.date.month = 9;
            task4.date.year = 2020;

            taskArray[3] = task4;
            countOfTasksInArray += 1;

            Task task5 = new Task()
            {
                description = "Buy food",
                levelOfImportance = 6,
                category = "Mandatory",
            };
            task5.date.day = 12;
            task5.date.month = 10;
            task5.date.year = 1899;

            taskArray[4] = task5;
            countOfTasksInArray += 1;

            Task task6 = new Task()
            {
                description = "Buy new car",
                levelOfImportance = 2,
                category = "When money",
            };
            task6.date.day = 24;
            task6.date.month = 5;
            task6.date.year = 2016;

            taskArray[5] = task6;
            countOfTasksInArray += 1;

        }
    }
} 