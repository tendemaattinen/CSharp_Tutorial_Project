﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application ask 10 numbers from user, saves them to array, sorts array with bubble sort and
 *              prints sorted array.
 */
 
using System;

namespace TutorialApplication
{
    class SortData
    {
        public void Main()
        {
            Console.WriteLine();
            int[] numberArray = new int[10]; // Creates new int[] -array

            for (int i = 0; i < 10; i++) // Loop to add numbers to arrya
            {
                Console.Write("Enter a number {0}: ", i + 1);
                int.TryParse(Console.ReadLine(), out int number);
                numberArray[i] = number;
            }

            //Array.Sort(numberArray); // Easy way =)

            numberArray = BubbleSort(numberArray, 10); // Bubble sort to sort array

            Console.WriteLine();
            Console.Write("Sorted array: ");
            for (int j = 0; j < 10; j++) // Loop to print array
            {
                Console.Write("{0} ", numberArray[j]);
            }
            Console.WriteLine();
        }

        // Function to sort array with bubble sort, returns sorted int[] -array
        private int[] BubbleSort(int[] array, int lenght)
        {
            for (int i = (lenght - 1); i >= 0; i--) // Loop to go through array backwards
            {
                for (int j = 1; j <= i; j++)
                {
                    if (array[j-1] > array [j]) // If previous number in array is bigger, changes places of numbers in the array
                    {
                        int tempNumber = array[j - 1];
                        array[j - 1] = array[j];
                        array[j] = tempNumber;
                    }
                }
            }
            return array;
        }
    }
} 