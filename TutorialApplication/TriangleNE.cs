﻿using System;

namespace TutorialApplication
{
    class TriangleNE
    {
        // Asks width from user and draws triangle starting from north east corner
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter the desired width: ");
            Int32.TryParse(Console.ReadLine(), out int width);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < 0+i; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < width-i; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine("");
            }

        }
    }
}