﻿using System;

namespace TutorialApplication
{
    class HexadecimalTable
    {
        // Prints hexadecimal table for values from 0 to 255
        public void Main()
        {
            int j = 0;
            Console.WriteLine("");
            // For-loop for printing
            for (int i = 0; i < 256; i++)
            {
                Console.Write(Convert.ToString(i, 16) + " ");
                if (j >= 15)
                {
                    Console.WriteLine("");
                    j = 0;
                }
                j++;
            }

        }
    }
}