﻿using System;

namespace TutorialApplication
{
    class SearchInArray
    {
        /* Application asks a amount of data from user, then data numbers so many time as user gave as amount. After
        * that application asks user to search number from array and application answers if numbre exsist or not exsist in array.
        */
        public void Main()
        {
            Console.WriteLine();
            Console.Write("Enter a amount of data: ");
            Int32.TryParse(Console.ReadLine(), out int amountOfData);
            
            int[] dataArray = saveToArrray(amountOfData); // Array for the numbers

            Console.Write("Enter a number to search: ");
            Int32.TryParse(Console.ReadLine(), out int numberToSearch);

            if (searchFromArray(dataArray, numberToSearch)) // Checks return value from searchFromArray, 'true' or 'false'
            {
                Console.WriteLine("Number {0} exist in array", numberToSearch);
            }
            else
            {
                Console.WriteLine("Number {0} don't exist in array", numberToSearch);
            }
        }

        // Function to save numbers to array, returns int array
        private int[] saveToArrray(int amount)
        {
            int[] array = new int[amount];

            for (int i = 0; i < amount; i++) // Asks a new number as may time as given amount in for-loop
            {
                Console.Write("Enter a number {0}: ", i+1);
                Int32.TryParse(Console.ReadLine(), out int number);
                array[i] = number;
            }
            return array;
        }

        // Search given number from array, return boolean value 'true' if it's found, otherwise returns 'false'
        private bool searchFromArray(int[] array, int searchNumber)
        {
            bool bol = false;
            foreach (int i in array) // Go through array with foreach
            {
                if (i == searchNumber) // If number is found
                {
                    bol = true;
                    break;
                }
            }
            return bol;
        }
    }
}