﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Creates random array of tables and coffeetables with one attached leg.
 */

using System;

namespace TutorialApplication
{
    // Class for table
    class Table2
    {
        protected float Width { get; set; }
        protected float Height { get; set; }
        protected Leg tableLeg; // Attached leg

        // Constructor
        public Table2()
        {

        }

        // Constructor with iunputs
        public Table2(float wid, float hei)
        {
            Width = wid;
            Height = hei;
        }

        // Method to show width and height of table
        public virtual void ShowData()
        {
            Console.WriteLine("Width: {0}, Height: {1}", Width, Height);
            tableLeg.ShowData();
        }

        // Methdod to bound table and leg
        public void AddLeg(Leg leg)
        {
            tableLeg = leg;
            leg.SetTable(this);
        }

    }

    // Class for coffee table
    class CoffeeTable2 : Table2
    {

        // Constructor with input
        public CoffeeTable2(float wid, float hei)
        {
            Width = wid;
            Height = hei;
        }

        // Method to show width and height of coffee table
        public override void ShowData()
        {
            Console.WriteLine("Coffee table: Width: {0}, Height: {0}", Width, Height);
            tableLeg.ShowData();
        }
    }

    // Class for leg
    class Leg
    {
        protected Table2 table; // Table that leg is attached

        // Method to show legs data (only prints that it is a leg)
        public void ShowData()
        {
            Console.WriteLine("I am a leg!");
        }

        // Sets table that leg is bound to
        public void SetTable(Table2 table2)
        {
            table = table2;
        }
    }

    // Testing class with Main-function
    class TestTable1
    {
        public void Main()
        {
            Console.WriteLine();
            Table2[] tableArray = new Table2[10]; // Array to store tables
            Random rand = new Random(); // Random
            // Loop for creating tables
            for (int i = 0; i < 5; i++)
            {
                int width = rand.Next(50, 200); // Random width
                int height = rand.Next(50, 200); // Random height
                Table2 table = new Table2(width, height); // Creates new table with created random numbers
                Leg leg = new Leg(); // Create new leg
                table.AddLeg(leg); // Add leg to table
                tableArray[i] = table; // Adds created table to array
            }
            // Loop for creating coffee tables
            for (int i = 5; i < 10; i++)
            {
                int width = rand.Next(50, 200); // Random width
                int height = rand.Next(50, 200); // Random height
                CoffeeTable2 table = new CoffeeTable2(width, height); // Creates new table with created random numbers
                Leg leg = new Leg(); // Create new leg
                table.AddLeg(leg); // Add leg to table
                tableArray[i] = table; // Adds created table to array
            }
            // Loop for printing all the tables from array
            for (int i = 0; i < 10; i++)
            {
                Console.Write("{0}) ", i + 1);
                tableArray[i].ShowData();
            }
        }
    }
}
