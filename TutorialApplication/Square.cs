﻿using System;

namespace TutorialApplication
{
    class Square
    {
        // Draws square from given parameters, number and width
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter a number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            Console.Write("Enter the width: ");
            Int32.TryParse(Console.ReadLine(), out int width);
            Console.WriteLine("");
            // Fro-loop for drawing
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write(number);
                }
                Console.WriteLine("");
            }
        }
    }
}