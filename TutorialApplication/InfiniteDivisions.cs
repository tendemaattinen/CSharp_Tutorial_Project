﻿using System;

namespace TutorialApplication
{
    class InfiniteDivisions
    {
        // Application divides two number given by user. It also checks if divider is zero
        public void Main()
        {
            Console.WriteLine("");
            Console.WriteLine("Enter 0 as a first number to end");
            while (true)
            {
                int div, rem;
                Console.WriteLine("");
                Console.Write("Enter first number: ");
                Int32.TryParse(Console.ReadLine(), out int number1);
                // Checks if user wants to end application
                if (number1 == 0)
                {
                    Console.WriteLine("Closing");
                    break;
                }
                Console.Write("Enter second number: ");
                Int32.TryParse(Console.ReadLine(), out int number2);
                // Checks if divider is zero, otherwise divides numbers and gives division and remainder
                if (number2 == 0)
                {
                    Console.WriteLine("Cannot divide by 0");
                } 
                else
                {
                    div = number1 / number2;
                    rem = number1 % number2;
                    Console.WriteLine("Division is {0}", div);
                    Console.WriteLine("Remainder is {0}", rem);
                }
            }

        }
    }
}