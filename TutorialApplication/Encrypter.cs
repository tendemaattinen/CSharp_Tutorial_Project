﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software that can encrypt and decrypt text. Encrypt-code is unique every startup. Encrypt happens by increasing ASCII-value of the characters.
 *              Decrypting happens by decreasing ASCII-value by same value.
 */
 
using System;

namespace TutorialApplication
{
    // Class for encrypter
    class Encrypter
    {
        protected int[] numberArray = new int[100];

        // Method for encrypting text
        public string Encrypt(string text)
        {
            char[] arrayText = text.ToCharArray(); // Creates char-array from given text
            int valueOfChar;
            string returnText;
            for (int i = 0; i < arrayText.Length; i++) // Loop for going throght the char-array
            {
                valueOfChar = (int) arrayText[i]; // Changes char to integrar
                valueOfChar += numberArray[i]; // Adds number from random number -array
                arrayText[i] = (char) valueOfChar; // Changes integral to char and saves it to char array
            }
            returnText = new string(arrayText); // Creates new string from char array
            return returnText;
        }

        // Method for decrypting text
        public string Decrypt(string text)
        {
            char[] arrayText = text.ToCharArray(); // Creates char-array from given text
            int valueOfChar;
            string returnText;
            for (int i = 0; i < arrayText.Length; i++) // Loop for going throght the char-array
            {
                valueOfChar = (int) arrayText[i]; // Changes char to integrar
                valueOfChar -= numberArray[i]; // Reduces number from random number -array
                arrayText[i] = (char)valueOfChar; // Changes integral to char and saves it to char array
            }
            returnText = new string(arrayText); // Creates new string from char array
            return returnText;
        }

        // Creates new rendom array for encrypting and decrypting text
        public void CreateNewCode()
        {
            Random rand = new Random(); // Creates new random
            int number;
            for (int i = 0; i < 100; i++) // Adds randon numbers to array
            {
                number = rand.Next(0, 10);
                numberArray[i] = number;
            }
        }
    }

    // Test class including Main-function
    class TextEncrypter
    {
        public void Main()
        {
            string encryptedText, decryptedText, textFromUser;
            bool swi = true; // Boolean value for running menu
            Console.WriteLine();
            Encrypter enc = new Encrypter(); // Creates new encrypter
            enc.CreateNewCode(); // Encrypter-class creates new encrypt-code
            Console.WriteLine("*** Encrypter ***");
            while (swi)
            {
                Console.WriteLine();
                Console.WriteLine("1) Encrypt");
                Console.WriteLine("2) Decrypt");
                Console.WriteLine("0) Quit");
                Console.Write("Enter: ");
                string userInput = Console.ReadLine();
                Console.WriteLine();
                switch (userInput) // Switch-case for user input
                {
                    case "1": // User wants to encrypt text
                        Console.Write("Enter text: ");
                        textFromUser = Console.ReadLine();
                        encryptedText = enc.Encrypt(textFromUser); // Encrypts text
                        Console.WriteLine("Encrypted text: {0}", encryptedText);
                        break;
                    case "2": // User wants to decrypt text
                        Console.Write("Enter text: ");
                        textFromUser = Console.ReadLine();
                        decryptedText = enc.Decrypt(textFromUser); // Decrypts text
                        Console.WriteLine("Decrypted text: {0}", decryptedText);
                        break;
                    case "0": // User wants to quit
                        swi = false;
                        break;
                    default: // USer gives invalid input
                        Console.WriteLine("Invalid input!");
                        break;
                }

            }
        }
    }
} 