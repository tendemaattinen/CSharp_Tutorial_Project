﻿using System;

namespace TutorialApplication
{
    class MultiplyWithInput
    {
        // User gives two number which are multiplyed by application
        public void Main()
        {
            int a, b, c;
            Console.WriteLine("");
            Console.Write("Give first number: ");
            Int32.TryParse(Console.ReadLine(), out a);
            Console.Write("Give second number: ");
            Int32.TryParse(Console.ReadLine(), out b);
            c = a * b;
            Console.WriteLine("Answer: " + c);

        }
    }
}