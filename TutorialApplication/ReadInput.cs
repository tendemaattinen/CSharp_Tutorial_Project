﻿using System;

namespace TutorialApplication
{
    class ReadInput
    {
        public void Main()
        {
            // Asks question, waits for input and gives standard answer
            Console.WriteLine("");
            Console.WriteLine("Whats you favourite color? ");
            Console.ReadLine();
            Console.WriteLine("Mine too!");
        }
    }
}