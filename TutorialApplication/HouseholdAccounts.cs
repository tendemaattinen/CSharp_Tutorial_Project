﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Program that can store up to 10000 expenses.
 */
 
using System;

namespace TutorialApplication
{
    class HouseholdAccounts
    {

        // Struct for expenses
        struct CashTransfer
        {
            public string description;
            public string category;
            public string date;
            public int amount;
        }

        private readonly CashTransfer[] expenseArray = new CashTransfer[10000]; // New array to store expenses
        private int countOfExpenses = 0; // Count of expenses

        public void Main()
        {
            double accountBalance = 0; // Balance of account
            bool swi = true; // Boolean value for looping program
            accountBalance = ExampleArray(); // Creates example array for easier testing
            Console.WriteLine();
            Console.WriteLine("*** Household accounts ***");
            while (swi)
            {
                PrintMenu(); // Prints menu
                Console.Write("Enter your choice: ");
                string userinput = Console.ReadLine();
                (swi, accountBalance) = ChoiceForAction(userinput, accountBalance); // Execute action wanted by user
            }
        }

        // Function to print menu
        private void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("*** Main menu ***");
            Console.WriteLine("1) Show balance of account");
            Console.WriteLine("2) Add new expense");
            Console.WriteLine("3) Show expenses");
            Console.WriteLine("4) Search expense");
            Console.WriteLine("5) Modify expense");
            Console.WriteLine("6) Delete expense");
            Console.WriteLine("7) Sort by date");
            Console.WriteLine("8) Normalize descriptions");
            Console.WriteLine("0) Exit application");
        }

        // Function to execute users wanted action, return boolean value, which is defaulted to true.
        // In case user wants to quit, boolean value is returned as false.
        private (bool, double) ChoiceForAction(string input, double balance)
        {
            bool swi = true;
            switch (input)
            {
                case "1": // Show balance
                    Console.WriteLine("Balance: {0:N2} EUR", balance);
                    break;
                case "2": // Add new expense
                    balance = AddNewExpense(balance);
                    break;
                case "3": // Show expenses
                    ShowExpenses();
                    break;
                case "4": // Search expense
                    SearchExpense();
                    break;
                case "5": // Modify expense
                    ModifyExpense();
                    break;
                case "6": // Delete expense
                    DeleteExpense();
                    break;
                case "7": // Sort by date
                    SortByDate();
                    break;
                case "8": // Normalize descriptions
                    NormalizeDescriptions();
                    break;
                // Quit program
                case "0":
                case "q":
                case "quit":
                case "Q":
                case "Quit":
                    swi = false;
                    break;
                default: // Invalid input
                    Console.WriteLine();
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return (swi, balance);
        }

        // Function to add new expense
        private double AddNewExpense(double balance)
        {
            Console.WriteLine();
            // Checks if database is already full
            if (countOfExpenses >= 10000)
            {
                Console.WriteLine("Can't add new expence: Expence list is full!");
            } else
            {
                CashTransfer tempCashTransfer = new CashTransfer();

                // Add description
                do
                {
                    Console.Write("Enter a description: ");
                    tempCashTransfer.description = Console.ReadLine();
                } while (tempCashTransfer.description.Length == 0);

                // Add category
                do
                {
                    Console.Write("Enter a category: ");
                    tempCashTransfer.category = Console.ReadLine();
                } while (tempCashTransfer.category.Length == 0);

                // Add date
                bool validity = false;
                do
                {
                    Console.Write("Enter a date (YYYYMMDD): ");
                    string input = Console.ReadLine();
                    if (input.Length == 8)
                    {
                        validity = CheckValidityOfDate(input); //Checks validity of input
                        tempCashTransfer.date = input;
                    }
                } while (!validity);

                // Add amount
                int expense = 0;
                do
                {
                    Console.Write("Enter amount: ");
                    int.TryParse(Console.ReadLine(), out expense);
                    tempCashTransfer.amount = expense;
                } while (expense == 0);

                balance += expense; // Adds expense to balance

                expenseArray[countOfExpenses] = tempCashTransfer; // Adds expense to array
                countOfExpenses += 1; // Adds one to count
            }

            Console.WriteLine();
            Console.WriteLine("New expense added!");

            return balance;
        }

        // Function to check validity of date
        private bool CheckValidityOfDate(string input)
        {
            bool validity;
            if (input.Length == 8)
            {
                int day, month, year;
                (day, month, year) = SplitStringDateToIntegrals(input); // Splits input to 3 integrals
                // Checks if date is valid (year between 1000-3000, month between 1-12, day between 1-31)
                if (year >= 1000 && year <= 3000 && month >= 1 && month <= 12 && day >= 1 && day <= 31)
                {
                    validity = true;
                }
                else // Date is invalid
                {
                    validity = false;
                }
            }
            else
            {
                validity = false;
            }
            return validity;
        }

        // Function to split date from string to integrals
        private (int, int, int) SplitStringDateToIntegrals(string input)
        {
            int day, month, year;
            string yearS = input.Substring(0, 4);
            year = Convert.ToInt32(yearS);
            string monthS = input.Substring(4, 2);
            month = Convert.ToInt32(monthS);
            string dayS = input.Substring(6, 2);
            day = Convert.ToInt32(dayS);
            return (day, month, year);
        }

        // Function to show all methods to show all expenses
        private void ShowExpenses()
        {
            Console.WriteLine();
            // Checks if list is empty
            if (countOfExpenses <= 0)
            {
                Console.WriteLine("No expenses!");
            }
            else
            {
                Console.WriteLine("1) Show all expenses");
                Console.WriteLine("2) Show all expenses from certain category");
                Console.WriteLine("3) Show all expenses between dates");
                Console.WriteLine("4) Show all expenses from certain category between dates");
                Console.WriteLine("0) Back");
                Console.Write("Enter: ");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1": // User wants to see all expenses
                        ShowAllExpenses();
                        break;
                    case "2": // User wants to see all expenses from certain category
                        ShowAllExpensesFronCertainCategory();
                        break;
                    case "3": // User wants to see all expenses between two dates
                        ShowAllExpensesBeweenDates();
                        break;
                    case "4": // User wants to see all expenses from certain category between two dates
                        ShowAllExpensesFromCertainCategoryBetweenDates();
                        break;
                    case "0":
                    case "q":
                    case "Q":
                    case "quit":
                    case "Quit":
                        break;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("Invalid input");
                        break;
                }
            }
        }

        // Function to show all expenses
        private void ShowAllExpenses()
        {
            Console.WriteLine();
            for (int i = 0; i < countOfExpenses; i++) // For-loop for going throgh the array
            {
                int day, month, year;
                (day, month, year) = SplitStringDateToIntegrals(expenseArray[i].date); // Splits date
                Console.WriteLine("{0}) {1}/{2}/{3} {4} [{5}] {6:N2} EUR" +
                    "", i + 1, day, month, year, expenseArray[i].description, expenseArray[i].category, expenseArray[i].amount);
            }
        }

        // Function to show all expenses from given category
        private void ShowAllExpensesFronCertainCategory()
        {
            Console.Write("Enter a category: ");
            string inputCategory = Console.ReadLine();
            inputCategory = inputCategory.ToLower(); // Input to lower case
            Console.WriteLine();
            for (int i = 0; i < countOfExpenses; i++) // For-loop for going throgh the array
            {
                if (inputCategory == expenseArray[i].category.ToLower()) // Checks if category match to input
                {
                    int day, month, year;
                    (day, month, year) = SplitStringDateToIntegrals(expenseArray[i].date); // Splits date
                    Console.WriteLine("{0}) {1}/{2}/{3} {4} [{5}] {6:N2} EUR" +
                        "", i + 1, day, month, year, expenseArray[i].description, expenseArray[i].category, expenseArray[i].amount);
                }
            }
        }

        // Function to show all expenses between given dates
        private void ShowAllExpensesBeweenDates()
        {
            bool startingValidity = false, endingValidity = false;
            Console.Write("Enter a starting date (YYYYMMDD): ");
            string startingDate = Console.ReadLine();
            startingValidity = CheckValidityOfDate(startingDate); // Checks validity of starting date
            if (startingValidity) // Starting date is valid
            {
                Console.Write("Enter a ending date (YYYYMMDD): ");
                string endingDate = Console.ReadLine();
                endingValidity = CheckValidityOfDate(endingDate); // Checks validity of ending date
                if (endingValidity) // Ending date is valid
                {
                    int startingDay, startingMonth, startingYear, endingDay, endingMonth, endingYear, arrayDay, arrayMonth, arrayYear;
                    (startingDay, startingMonth, startingYear) = SplitStringDateToIntegrals(startingDate); // Splits starting date
                    (endingDay, endingMonth, endingYear) = SplitStringDateToIntegrals(endingDate); // Splits ending date
                    Console.WriteLine();
                    for (int i = 0; i < countOfExpenses; i++) // For-loop for going throgh the array
                    {
                        (arrayDay, arrayMonth, arrayYear) = SplitStringDateToIntegrals(expenseArray[i].date);
                        PrintExpensesBetween(startingDay, startingMonth, startingYear, endingDay, endingMonth, endingYear, arrayDay, arrayMonth, arrayYear, expenseArray[i]);
                    }
                }
                else
                {
                    Console.WriteLine("Ending date is invalid!");
                }
            }
            else
            {
                Console.WriteLine("Starting date is invalid!");
            }
        }

        // Function to show all expenses from certain category between specific dates
        private void ShowAllExpensesFromCertainCategoryBetweenDates()
        {
            bool startingValidity = false, endingValidity = false;
            Console.Write("Enter a category: ");
            string inputCategory = Console.ReadLine();
            inputCategory = inputCategory.ToLower(); // Input to lower case
            Console.Write("Enter a starting date (YYYYMMDD): ");
            string startingDate = Console.ReadLine();
            startingValidity = CheckValidityOfDate(startingDate); // Checks validity of starting date
            if (startingValidity) // Starting date is valid
            {
                Console.Write("Enter a ending date (YYYYMMDD): ");
                string endingDate = Console.ReadLine();
                endingValidity = CheckValidityOfDate(endingDate); // Checks validity of ending date
                if (endingValidity) // Ending date is valid
                {
                    int startingDay, startingMonth, startingYear, endingDay, endingMonth, endingYear, arrayDay, arrayMonth, arrayYear;
                    (startingDay, startingMonth, startingYear) = SplitStringDateToIntegrals(startingDate); // Splits starting date
                    (endingDay, endingMonth, endingYear) = SplitStringDateToIntegrals(endingDate); // Splits ending date
                    Console.WriteLine();
                    for (int i = 0; i < countOfExpenses; i++) // For-loop for going throgh the array
                    {
                        if (inputCategory == expenseArray[i].category.ToLower()) // Checks if category matches to input
                        {
                            (arrayDay, arrayMonth, arrayYear) = SplitStringDateToIntegrals(expenseArray[i].date);
                            PrintExpensesBetween(startingDay, startingMonth, startingYear, endingDay, endingMonth, endingYear, arrayDay, arrayMonth, arrayYear, expenseArray[i]);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Ending date is invalid!");
                }
            }
            else
            {
                Console.WriteLine("Starting date is invalid!");
            }
        }

        // Function to search and print expenses between dates
        private void PrintExpensesBetween(int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear, int dayInExpense, int monthInExpense, int yearInExpense, CashTransfer expense)
        {
            // Year is between starting and ending years
            if (yearInExpense > startYear && yearInExpense < endYear)
            {
                Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
            }
            // Year is same as starting year but smaller than ending year
            else if (yearInExpense == startYear && yearInExpense < endYear)
            {
                // Month is bigger than starting month
                if (monthInExpense > startMonth)
                {
                    Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                    "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                }
                // Month is the same as starting month
                else if (monthInExpense == startMonth)
                {
                    // Day is the same or bigger than starting day
                    if (dayInExpense >= startDay)
                    {
                        Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                        "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                    }
                }
            }
            // Year is bigger than starting year and same as ending year
            else if (yearInExpense > startYear && yearInExpense == endYear)
            {
                // Month is smaller than ending month
                if (monthInExpense < endMonth)
                {
                    Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                    "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                }
                // Month is the same as ending month
                else if (monthInExpense == endMonth)
                {
                    // Day is same or smaller than ending day
                    if (dayInExpense <= endDay)
                    {
                        Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                        "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                    }
                }
            }
            // Year is same as starting and ending years
            else if (yearInExpense == startYear && yearInExpense == endYear)
            {
                // Month is between starting and ending months
                if (monthInExpense > startMonth && monthInExpense < endYear)
                {
                    Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                    "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                }
                // Month is same as starting month and smaller than ending month
                else if (monthInExpense == startMonth && monthInExpense < endMonth)
                {
                    // Day is bigger than starting day
                    if (dayInExpense >= startDay)
                    {
                        Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                        "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                    }
                }
                // Month is bigger than starting month and same as ending month
                else if (monthInExpense > startMonth && monthInExpense == endMonth)
                {
                    // Day is smaller than ending day
                    if (dayInExpense <= endDay)
                    {
                        Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                        "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                    }
                }
                // Month is the same as staring and ending months
                else if (monthInExpense == startMonth && monthInExpense == endMonth)
                {
                    // Day is between starting and ending days
                    if (dayInExpense >= startDay && dayInExpense <= endDay)
                    {
                        Console.WriteLine("{0}/{1}/{2} {3} [{4}] {5:N2} EUR" +
                        "", dayInExpense, monthInExpense, yearInExpense, expense.description, expense.category, expense.amount);
                    }
                }
            }
        }

        // Function to find expenses with part of the text
        private void SearchExpense()
        {
            bool found = false; // Boolean value to remember if user input has been found from array 
            int day, month, year;
            Console.WriteLine();
            // Checks if array is empty
            if (countOfExpenses <= 0)
            {
                Console.WriteLine("No expenses!");
            }
            else
            {
                Console.Write("Enter a part of category or description: ");
                string userInput = Console.ReadLine().ToLower();
                for (int i = 0; i < countOfExpenses; i++) // For-loop for going through the array
                {
                    string category = expenseArray[i].category.ToLower(); // Category from array
                    string description = expenseArray[i].description.ToLower(); // Description from array
                    if ((category.Contains(userInput)) || (description.Contains(userInput))) // Checks if category or description contains user input
                    {
                        found = true; // Sets boolean value to true
                        (day, month, year) = SplitStringDateToIntegrals(expenseArray[i].date); // Splits date
                        Console.WriteLine("{0}) {1}/{2}/{3} {4} [{5}] {6:N2} EUR" +
                        "", i + 1, day, month, year, expenseArray[i].description, expenseArray[i].category, expenseArray[i].amount);
                    }
                }
                if (!found) // User input was not found from expense list
                {
                    Console.WriteLine("'{0}' not found from list!", userInput);
                }
            }
        }

        private void ModifyExpense()
        {
            Console.WriteLine();
            // Checks if array is empty
            if (countOfExpenses <= 0)
            {
                Console.WriteLine("No expenses!");
            }
            else
            {
                Console.Write("Enter the number of expense: ");
                int.TryParse(Console.ReadLine(), out int numberOfExpense);
                if (numberOfExpense > 0 && numberOfExpense <= countOfExpenses) // Checks if user input is bigger than 0 and smaller than count of expense
                {
                    Console.Write("Expense: {0} (y/n): ", expenseArray[numberOfExpense - 1].description);
                    string answer = Console.ReadLine();
                    Console.WriteLine();
                    switch (answer)
                    {
                        // User wants to update task
                        case "y":
                        case "Y":
                        case "yes":
                        case "Yes":
                        case "YES":
                            Console.WriteLine("Enter a new value or press 'Enter' to leave value unchanged");
                            // Update description
                            Console.Write("Description: {0} | New value: ", expenseArray[numberOfExpense - 1].description);
                            string newDescription = Console.ReadLine();
                            if (newDescription  != "") // Checks if new description is not empty
                            {
                                expenseArray[numberOfExpense - 1].description = newDescription;
                            }
                            // Update category
                            Console.Write("Category: {0} | New value: ", expenseArray[numberOfExpense - 1].category);
                            string newCategory = Console.ReadLine();
                            if (newCategory != "") // Checks if new category is not empty
                            {
                                expenseArray[numberOfExpense - 1].category = newCategory;
                            }
                            // Update date
                            bool validityOfDate = false; // Boolean value for checking validity of date
                            Console.Write("Date: {0} | New value (YYYYMMDD): ", expenseArray[numberOfExpense - 1].date);
                            string newDate = Console.ReadLine();
                            if (newDate != "") // Checks if new date is not empty
                            {
                                validityOfDate = CheckValidityOfDate(newDate); // Checks validity of date
                                if (validityOfDate) // Date is valid
                                {
                                    expenseArray[numberOfExpense - 1].date = newDate;
                                }
                                else // Date is not valid
                                {
                                    Console.WriteLine("Invalid date!");
                                    break;
                                }
                            }
                            // Update amount
                            Console.Write("Amount: {0} | New value: ", expenseArray[numberOfExpense - 1].amount);
                            string newAmount = Console.ReadLine();
                            if (newAmount != "") // Checks if new amount is not empty
                            {
                                int newAmountInt = Convert.ToInt32(newAmount); // Converts string to integral
                                expenseArray[numberOfExpense - 1].amount = newAmountInt;
                            }
                            Console.WriteLine("Expense modified!");
                            break;
                        // User doesn't want to modify expense
                        case "n":
                        case "N":
                        case "no":
                        case "No":
                        case "NO":
                            Console.WriteLine("Modifying aborted!");
                            break;
                        default:
                            Console.WriteLine("Invalid input");
                            break;
                    }
                }
            }
        }

        // Functio to delete specific expense from array
        private void DeleteExpense()
        {
            Console.WriteLine();
            // Checks if array is empty
            if (countOfExpenses <= 0)
            {
                Console.WriteLine("No expenses!");
            }
            else
            {
                Console.Write("Enter a placement of expense: ");
                int.TryParse(Console.ReadLine(), out int numberOfExpense);
                if (numberOfExpense > 0 && numberOfExpense <= countOfExpenses) // Checks if user input is within bounds
                {
                    Console.Write("{0}) {1} | Continue (y/n): ", numberOfExpense, expenseArray[numberOfExpense - 1].description);
                    string userChoice = Console.ReadLine();
                    switch (userChoice)
                    {
                        case "y":
                        case "Y":
                        case "yes":
                        case "Yes":
                        case "YES":
                            for (int i = 0; i < countOfExpenses; i++) // For-loop for going through the array
                            {
                                if (i >= numberOfExpense) // If number is bigger than number we want to remove
                                {
                                    expenseArray[i - 1] = expenseArray[i]; // Moves programs in the array one place backwards
                                }
                            }
                            countOfExpenses -= 1; // Removes one from lenght of the array
                            Console.WriteLine("Expense deleted.");
                            break;
                        case "n":
                        case "N":
                        case "no":
                        case "No":
                        case "NO":
                            Console.WriteLine("Deletion of expense aborted!");
                            break;
                        default:
                            Console.WriteLine("Invalid input!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Number out of boundary!");
                }
            }
        }

        // Function to sort array by date
        private void SortByDate()
        {
            Console.WriteLine();
            // Checks if array is empty
            if (countOfExpenses <= 0)
            {
                Console.WriteLine("No expenses!");
            }
            else
            {
                int day, month, year, dayMinusOne, yearMinusOne, monthMinusOne;
                for (int i = (countOfExpenses - 1); i >= 0; i--) // Loop to go through array backwards
                {
                    for (int j = 1; j <= i; j++)
                    {
                        (day, month, year) = SplitStringDateToIntegrals(expenseArray[j].date); // Splits first date to integrals
                        (dayMinusOne, monthMinusOne, yearMinusOne) = SplitStringDateToIntegrals(expenseArray[j - 1].date); // Splits second date to integrals
                        if (yearMinusOne == year) // Checks if value of the years are same
                        {
                            if (monthMinusOne == month) // Checks if value of the months are same
                            {
                                if (dayMinusOne > day) // If previous day in array is bigger, changes places of tasks in the array
                                {
                                    CashTransfer tempTransfer = expenseArray[j - 1];
                                    expenseArray[j - 1] = expenseArray[j];
                                    expenseArray[j] = tempTransfer;
                                }
                            }
                            else if (monthMinusOne > month) // If previous month in array is bigger, changes places of tasks in the array
                            {
                                CashTransfer tempTransfer = expenseArray[j - 1];
                                expenseArray[j - 1] = expenseArray[j];
                                expenseArray[j] = tempTransfer;
                            }
                        }
                        else if (yearMinusOne > year) // If previous year in array is bigger, changes places of tasks in the array
                        {
                            CashTransfer tempTransfer = expenseArray[j - 1];
                            expenseArray[j - 1] = expenseArray[j];
                            expenseArray[j] = tempTransfer;

                        }
                    }
                }
                Console.WriteLine("Expenses sorted.");
            }
        }

        // Function to normalize descriptions of expenses in the array
        private void NormalizeDescriptions()
        {
            string description, tempDescription, firstLetterAsString;
            char firstLetter;
            for (int i = 0; i < countOfExpenses; i++) // For-loop for going through the array
            {
                description = expenseArray[i].description;

                // Checks if first character is space, and removes it
                if (description.ToCharArray()[0] == (' '))
                {
                    description = description.Substring(1);
                }

                // Changes first letter to upper case and other letters to lower case
                tempDescription = description.Substring(1);
                tempDescription = tempDescription.ToLower();
                firstLetter = description[0];
                firstLetterAsString = firstLetter.ToString().ToUpper();
                tempDescription = firstLetterAsString + tempDescription;
                description = tempDescription;
                // Checks if name contains two spaces in a row and runs that in a while loop so long that the are not anymore more than one space in a row
                while (description.Contains("  "))
                {
                    description = description.Replace("  ", " "); // Replaces two spaces with one space
                }
                expenseArray[i].description = description;
            }
            Console.WriteLine();
            Console.WriteLine("Descriptions normalized!");
        }

        // Function to create example array to ease testing
        private double ExampleArray()
        {
            double balance = 1000; // Sets account balance
            int i = 0; // Counter

            CashTransfer cash1 = new CashTransfer()
            {
                description = "Wage",
                category = "Income",
                date = "20181009",
                amount = 2500,
            };
            balance += cash1.amount;
            expenseArray[i] = cash1;
            countOfExpenses += 1;
            i += 1;

            CashTransfer cash2 = new CashTransfer()
            {
                description = " Pizza",
                category = "Food",
                date = "20181008",
                amount = -10,
            };
            balance += cash2.amount;
            expenseArray[i] = cash2;
            countOfExpenses += 1;
            i += 1;

            CashTransfer cash3 = new CashTransfer()
            {
                description = "beer",
                category = "Drinks",
                date = "20180908",
                amount = -5,
            };
            balance += cash3.amount;
            expenseArray[i] = cash3;
            countOfExpenses += 1;
            i += 1;

            CashTransfer cash4 = new CashTransfer()
            {
                description = "GroCery",
                category = "Food",
                date = "20180810",
                amount = -100,
            };
            balance += cash4.amount;
            expenseArray[i] = cash4;
            countOfExpenses += 1;
            i += 1;

            CashTransfer cash5 = new CashTransfer()
            {
                description = "COMPUTER",
                category = "Technology",
                date = "20171219",
                amount = -1500,
            };
            balance += cash5.amount;
            expenseArray[i] = cash5;
            countOfExpenses += 1;
            i += 1;

            CashTransfer cash6 = new CashTransfer()
            {
                description = "Mobile   phone",
                category = "Technology",
                date = "20150115",
                amount = -899,
            };
            balance += cash6.amount;
            expenseArray[i] = cash6;
            countOfExpenses += 1;
            i += 1;

            CashTransfer cash7 = new CashTransfer()
            {
                description = "Restaurant",
                category = "Food",
                date = "20160911",
                amount = -50,
            };
            balance += cash7.amount;
            expenseArray[i] = cash7;
            countOfExpenses += 1;
            i += 1;

            return balance;
        }
    }
} 