﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software prints greeting with name and farewell 
 */
 
using System;

namespace TutorialApplication
{
    class FunctionWithParameters
    {
        public void Main()
        {
            Console.WriteLine();
            SayHello("John");
            SayGoodBye();
        }

        // Function to print "Hello"
        private void SayHello(string name)
        {
            Console.WriteLine("Hello {0}", name);
        }

        // Function to print "Good Bye!"
        private void SayGoodBye()
        {
            Console.WriteLine("Good Bye!");
        }
    }
} 