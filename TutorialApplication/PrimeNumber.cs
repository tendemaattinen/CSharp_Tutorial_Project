﻿using System;

namespace TutorialApplication
{
    class PrimeNumber
    {
        // Asks a number from user and tells if it is a prime number or not
        public void Main()
        {
            while (true)
            {
                int divider = 2;
                Console.WriteLine();
                Console.Write("Enter a number or '0' to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number);
                // Checks if user wants to quit by giving '0' as a number
                if (number == 0)
                {
                    break;
                }
                // Divides number and adds one to divider while remaider not equals to zero
                while (number % divider != 0)
                {
                    divider++;
                }
                // If divider is same as number when while loop is ended, number is a prime number
                if (divider == number)
                {
                    Console.WriteLine("It's a prime number");
                }
                else
                {
                    Console.WriteLine("It isn't prime number");
                }
            }
        }
    }
}