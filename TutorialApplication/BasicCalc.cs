﻿using System;

namespace TutorialApplication
{
    class BasicCalc
    {
        public void Main()
        {
            int number1, number2;
            Console.WriteLine("");
            Console.Write("Give first number: ");
            Int32.TryParse(Console.ReadLine(), out number1);
            Console.Write("Give second number: ");
            Int32.TryParse(Console.ReadLine(), out number2);
            Console.WriteLine("{0} + {1} = " + (number1 + number2), number1, number2);
            Console.WriteLine("{0} - {1} = " + (number1 - number2), number1, number2);
            Console.WriteLine("{0} * {1} = " + (number1 * number2), number1, number2);
            // Checks if user tries divide by zero
            if(number2 != 0)
            {
                Console.WriteLine("{0} / {1} = " + (number1 / number2), number1, number2);
                Console.WriteLine("{0} mod {1} = " + (number1 % number2), number1, number2);
            } else
            {
                Console.WriteLine("Can not be divided by zero!");
            }
        }
    }
}