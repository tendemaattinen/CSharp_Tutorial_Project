﻿using System;

namespace TutorialApplication
{
    class DigitsInANumber
    {
        // Application tells how many digits is in a number
        public void Main()
        {
            while (true)
            {
                int digits = 0;
                Console.WriteLine();
                Console.Write("Enter a number or 0 to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number);

                // Checks if number is a zero and user wants to quit
                if (number == 0)
                {
                    break;
                }

                Console.WriteLine();

                // Checks if number is a negative. If it is, warns user about it and changes it in a equivalent positive number. 
                if (number < 0)
                {
                    Console.WriteLine("Warning, number is negative.");
                    number = -number;
                }

                // Calculates digits with divide
                while (number > 0)
                {
                    number = (number / 10);
                    digits++;
                }

                Console.WriteLine("{0} digits", digits);
            }
        }
    }
}