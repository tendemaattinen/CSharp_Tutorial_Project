﻿using System;

namespace TutorialApplication
{
    class Conversion
    {
        // User gives celsius and application convers it to kelvin and fahrenheit
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Give celsius: ");
            Int32.TryParse(Console.ReadLine(), out int celsius);
            int kelvin = celsius + 273;
            int fahrenheit = ((celsius * 18) / 10) + 32;
            Console.WriteLine("Kelvin = {0}", kelvin);
            Console.WriteLine("Fahrenheit = {0}", fahrenheit);
        }
    }
}