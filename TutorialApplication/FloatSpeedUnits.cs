﻿using System;

namespace TutorialApplication
{
    class FloatSpeedUnits
    {
        // User gives distance and time travelled, and application gives different speed metrics
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter distance in meters: ");
            float.TryParse(Console.ReadLine(), out float distance);
            Console.Write("Enter hours: ");
            float.TryParse(Console.ReadLine(), out float hour);
            Console.Write("Enter minutes: ");
            float.TryParse(Console.ReadLine(), out float minute);
            Console.Write("Enter seconds: ");
            float.TryParse(Console.ReadLine(), out float second);
            Speed(distance, hour, minute, second);
        }

        // Calculates speeds from given values
        private void Speed(float dis, float h, float min, float sec)
        {
            float timeInSeconds = ((60 * 60 * h) + (60 * min) + sec);
            float timeInHours = h + (min/60) + ((sec/60)/60);
            float distanceInKilometers = dis / 1000;
            float kmh = distanceInKilometers / timeInHours;
            Console.WriteLine("Speed in m/s is {0}", (dis/timeInSeconds));
            Console.WriteLine("Speed in km/h is {0}", kmh);
            Console.WriteLine("Speed in miles/h is {0}", (kmh / 1.609f));
        }
    }
}