﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software replaces 'hello' words from 'text.txt' -file with 'goodbye' and writes them to 'TextReplacer.txt' -file.
 *              Both files are located in 'TutorialApplication\bin\Debug\netcoreapp2.0'.
 */

using System;
using System.IO;

namespace TutorialApplication
{
    class TextReplacer
    {
        public void Main()
        {
            Console.WriteLine();
            Replace("text.txt", "hello", "goodbye");
        }

        // Function to replace given words from given file
        private void Replace(string fileName, string textToReplace, string newText)
        {
            // Try
            try
            {
                StreamReader sR = File.OpenText(fileName); // Opens readable file
                StreamWriter sW = File.CreateText("TextReplacer.txt"); // Creates new writable file
                string line = "";
                // Replaces files as long as line isn't empty
                do
                {
                    line = sR.ReadLine(); // Saves line from readable file
                    if (line != null)
                    {
                        line = line.Replace(textToReplace, newText); // Replaces text
                        sW.WriteLine(line); // Saves replaced line to new file
                    }
                } while (line != null);
                // Closes both opened files
                sR.Close(); 
                sW.Close();
                Console.WriteLine("'Hello' -words are replaced by 'goodbye'!");
            }
            // Catch error
            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
        }
    }
} 