﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Main program. Prints menus and directs to right functions.
 */

using System;

namespace TutorialApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            string userInput;
            // Loop for menu and input (working menu)
            while (true)
            {
                MainMenu();
                Console.Write("Your Choice: ");
                userInput = Console.ReadLine();
                ChoiceForMainMenu(userInput);
            }
        }

        // Prints main menu
        private static void MainMenu()
        {
            Console.WriteLine();
            Console.WriteLine("****** Main Menu ******");
            Console.WriteLine("1) Introduction");
            Console.WriteLine("2) Flow Control 1");
            Console.WriteLine("3) Data Types");
            Console.WriteLine("4) Flow Control 2");
            Console.WriteLine("5) Arrays, Structures and Strings");
            Console.WriteLine("6) Functions");
            Console.WriteLine("7) Object-Oriented Programming");
            Console.WriteLine("8) File Management");
            Console.WriteLine("9) Object Persistence");
            Console.WriteLine("10) Access to Relational Database");
            Console.WriteLine("0) Close application");
        }

        // Prints introduction section
        private static void IntroductionMenu()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Introduction ******");
                Console.WriteLine("1) Hello World");
                Console.WriteLine("2) Print lines in console");
                Console.WriteLine("3) Reading users input");
                Console.WriteLine("4) Sum and division of two numbers (plus multiple operations)");
                Console.WriteLine("5) Multiply with users input");
                Console.WriteLine("6) Usage of {0}");
                Console.WriteLine("7) Basic calculator");
                Console.WriteLine("8) Multiplication table");
                Console.WriteLine("9) Average of numbers");
                Console.WriteLine("10) Equivalent");
                Console.WriteLine("11) Rectangle");
                Console.WriteLine("12) Conversion");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForIntroduction(userInput);
            }
        }

        // Prints flow control section
        private static void FlowControl1()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Flow Control 1 ******");
                Console.WriteLine("1) Positive and negative");
                Console.WriteLine("2) Multiply, if not zero");
                Console.WriteLine("3) Greatest of three number");
                Console.WriteLine("4) Using while");
                Console.WriteLine("5) Using do while");
                Console.WriteLine("6) Counter to 10");
                Console.WriteLine("7) Times table");
                Console.WriteLine("8) Odd numbers, descending");
                Console.WriteLine("9) Sum numbers");
                Console.WriteLine("10) Two negatives");
                Console.WriteLine("11) Multiples");
                Console.WriteLine("12) Number repeated");
                Console.WriteLine("13) Access Control (simple)");
                Console.WriteLine("14) Infinite divisions");
                Console.WriteLine("15) Several multiplication tables");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForFlowControl1(userInput);
            }
        }

        // Prints data types section
        private static void DataTypes()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Data Types ******");
                Console.WriteLine("1) Char");
                Console.WriteLine("2) Triangle");
                Console.WriteLine("3) Access control");
                Console.WriteLine("4) Calculator - if");
                Console.WriteLine("5) Calculator - switch");
                Console.WriteLine("6) Double value");
                Console.WriteLine("7) Calculate values of a function");
                Console.WriteLine("8) Display a function");
                Console.WriteLine("9) Float, speed units");
                Console.WriteLine("10) Sphere, float");
                Console.WriteLine("11) Vowel - switch");
                Console.WriteLine("12) Vowel - if");
                Console.WriteLine("13) Triangle, NorthEast");
                Console.WriteLine("14) Prime factors");
                Console.WriteLine("15) If, symbols");
                Console.WriteLine("16) Char + for");
                Console.WriteLine("17) Perimeter, area, diagonal");
                Console.WriteLine("18) Hexadecimal & binary");
                Console.WriteLine("19) Hexadecimal table");
                Console.WriteLine("20) Binary");
                Console.WriteLine("21) Conditional and boolean");
                Console.WriteLine("22) Exceptions");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForDataTypes(userInput);
            }
        }

        // Prints flow control 2 section
        private static void FlowControl2()
        {
            bool swi = true;
            while (swi)
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Flow Control 2 ******");
                Console.WriteLine("1) Square");
                Console.WriteLine("2) Break and continue");
                Console.WriteLine("3) Retangle");
                Console.WriteLine("4) Repetitive structures");
                Console.WriteLine("5) Digits in a number");
                Console.WriteLine("6) Hollow square");
                Console.WriteLine("7) Product");
                Console.WriteLine("8) Absolute value");
                Console.WriteLine("9) Hollow retangle");
                Console.WriteLine("10) Statistics");
                Console.WriteLine("11) Switch");
                Console.WriteLine("12) Conditional operator, positive & smaller");
                Console.WriteLine("13) Prime number");
                Console.WriteLine("14) Conditional operator");
                Console.WriteLine("15) Give Change");
                Console.WriteLine("16) Exceptions");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForFlowControl2(userInput);
            }
        }

        // Prints arrays, structures and strings section
        private static void ArraysStructuresAndStrings()
        {
            bool swi = true;
            while (swi)
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Arrays, Structures and Strings ******");
                Console.WriteLine("1) Reverse array");
                Console.WriteLine("2) Search in array");
                Console.WriteLine("3) Array, even numbers");
                Console.WriteLine("4) Array of positive and negative numbers");
                Console.WriteLine("5) Two-dimensional array");
                Console.WriteLine("6) Statistics");
                Console.WriteLine("7) Struct");
                Console.WriteLine("8) Array of struct");
                Console.WriteLine("9) Array of struct + menu");
                Console.WriteLine("10) Books database");
                Console.WriteLine("11) Centered triangle");
                Console.WriteLine("12) Cities database");
                Console.WriteLine("13) Banner (WiP)");
                Console.WriteLine("14) Triangle, right side");
                Console.WriteLine("15) Nested structs");
                Console.WriteLine("16) Sort data (with bubble sort)");
                Console.WriteLine("17) Two -dimensional array as buffer for screen");
                Console.WriteLine("18) Two-dimensional array 2: circunference on screen (WiP)");
                Console.WriteLine("19) Computer programs");
                Console.WriteLine("20) Exercise (tasks)");
                Console.WriteLine("21) Household accounts");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForArraysStructuresAndStrings(userInput);
            }
        }

        // Prints functions section
        private static void Functions()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Functions ******");
                Console.WriteLine("1) Functions: greeting + farewell");
                Console.WriteLine("2) Function with parameters");
                Console.WriteLine("3) ");
                Console.WriteLine("4) ");
                Console.WriteLine("5) ");
                Console.WriteLine("6) ");
                Console.WriteLine("7) ");
                Console.WriteLine("8) ");
                Console.WriteLine("9) ");
                Console.WriteLine("10) ");
                Console.WriteLine("11) ");
                Console.WriteLine("12) ");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForFunctions(userInput);
            }
        }

        // Prints object-oriented programming section
        private static void ObjectOrientedProgramming()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Object-Oriented programming ******");
                Console.WriteLine("1) Classes student + teacher");
                Console.WriteLine("2) Photo album tarea");
                Console.WriteLine("3) Array of objects: table");
                Console.WriteLine("4) House");
                Console.WriteLine("5) Table + coffetable + array");
                Console.WriteLine("6) Encrypter with menu");
                Console.WriteLine("7) Complex numbers");
                Console.WriteLine("8) Table + coffeetable + leg");
                Console.WriteLine("9) Random number (WIP)");
                Console.WriteLine("10) ");
                Console.WriteLine("11) ");
                Console.WriteLine("12) ");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForObjectOrientedProgramming(userInput);
            }
        }

        // Prints file management section
        private static void FileManagement()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** File Management ******");
                Console.WriteLine("1) Writing to a text file");
                Console.WriteLine("2) Appending to a text file");
                Console.WriteLine("3) Display file content");
                Console.WriteLine("4) Logger");
                Console.WriteLine("5) More (WIP)");
                Console.WriteLine("6) Text replacer");
                Console.WriteLine("7) Count letters in file");
                Console.WriteLine("8) Reading a binary file");
                Console.WriteLine("9) ");
                Console.WriteLine("10) ");
                Console.WriteLine("11) ");
                Console.WriteLine("12) ");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForFileManagement(userInput);
            }
        }

        // Prints object persistence section
        private static void ObjectPersistence()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Object Persistence ******");
                Console.WriteLine("1) ");
                Console.WriteLine("2) ");
                Console.WriteLine("3) ");
                Console.WriteLine("4) ");
                Console.WriteLine("5) ");
                Console.WriteLine("6) ");
                Console.WriteLine("7) ");
                Console.WriteLine("8) ");
                Console.WriteLine("9) ");
                Console.WriteLine("10) ");
                Console.WriteLine("11) ");
                Console.WriteLine("12) ");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceForObjectPersistence(userInput);
            }
        }

        // Prints database section
        private static void Database()
        {
            bool swi = true;
            while (swi) // Loop for menu
            {
                string userInput;
                Console.WriteLine("");
                Console.WriteLine("****** Access to Relational Database ******");
                Console.WriteLine("1) Database creation");
                Console.WriteLine("2) ");
                Console.WriteLine("3) ");
                Console.WriteLine("4) ");
                Console.WriteLine("5) ");
                Console.WriteLine("6) ");
                Console.WriteLine("7) ");
                Console.WriteLine("8) ");
                Console.WriteLine("9) ");
                Console.WriteLine("10) ");
                Console.WriteLine("11) ");
                Console.WriteLine("12) ");
                Console.WriteLine("0) Back to main menu");
                Console.Write("Your choice: ");
                userInput = Console.ReadLine();
                swi = ChoiceFordatabase(userInput);
            }
        }

        // Handles input which user gives to application in main menu
        private static void ChoiceForMainMenu(string input)
        {
            switch(input)
            {
                case "1":
                    IntroductionMenu();
                    break;
                case "2":
                    FlowControl1();
                    break;
                case "3":
                    DataTypes();
                    break;
                case "4":
                    FlowControl2();
                    break;
                case "5":
                    ArraysStructuresAndStrings();
                    break;
                case "6":
                    Functions();
                    break;
                case "7":
                    ObjectOrientedProgramming();
                    break;
                case "8":
                    FileManagement();
                    break;
                case "9":
                    ObjectPersistence();
                    break;
                case "10":
                    Database();
                    break;
                case "0":
                    Console.WriteLine("");
                    Console.WriteLine("Closing Application");
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
        }

        // Handles input which user gives to application in introduction section
        private static bool ChoiceForIntroduction(string input)
        {
            bool swi = true;
            switch(input)
            {
                case "1":
                    HelloWorld helloWorld = new HelloWorld();
                    helloWorld.Main();
                    break;
                case "2":
                    PrintLines printLines = new PrintLines();
                    printLines.Main();
                    break;
                case "3":
                    ReadInput readInput = new ReadInput();
                    readInput.Main();
                    break;
                case "4":
                    SumOfTwo sumOfTwo = new SumOfTwo();
                    sumOfTwo.Main();
                    break;
                case "5":
                    MultiplyWithInput multiplyWithInput = new MultiplyWithInput();
                    multiplyWithInput.Main();
                    break;
                case "6":
                    UseOf useOf = new UseOf();
                    useOf.Main();
                    break;
                case "7":
                    BasicCalc basicCalc = new BasicCalc();
                    basicCalc.Main();
                    break;
                case "8":
                    MultiplicationTable mpT = new MultiplicationTable();
                    mpT.Main();
                    break;
                case "9":
                    Average average = new Average();
                    average.Main();
                    break;
                case "10":
                    Equivalent equivalent = new Equivalent();
                    equivalent.Main();
                    break;
                case "11":
                    Rectangle rectangle = new Rectangle();
                    rectangle.Main();
                    break;
                case "12":
                    Conversion conversion = new Conversion();
                    conversion.Main();
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in flow control section
        private static bool ChoiceForFlowControl1(string input)
        {
            bool swi = true;
            switch(input)
            {
                case "1":
                    PositiveAndNegative positiveAndNegative = new PositiveAndNegative();
                    positiveAndNegative.Main();
                    break;
                case "2":
                    MultiplyIfNotZero multiplyIfNotZero = new MultiplyIfNotZero();
                    multiplyIfNotZero.Main();
                    break;
                case "3":
                    GreatestOfThreeNumber greatestOfThreeNumber = new GreatestOfThreeNumber();
                    greatestOfThreeNumber.Main();
                    break;
                case "4":
                    UsingWhile usingWhile = new UsingWhile();
                    usingWhile.Main();
                    break;
                case "5":
                    UsingDoWhile usingDoWhile = new UsingDoWhile();
                    usingDoWhile.Main();
                    break;
                case "6":
                    Counter counter = new Counter();
                    counter.Main();
                    break;
                case "7":
                    TimesTable timesTable = new TimesTable();
                    timesTable.Main();
                    break;
                case "8":
                    DescOddNumbers descOddNumbers = new DescOddNumbers();
                    descOddNumbers.Main();
                    break;
                case "9":
                    SumNumbers sumNumbers = new SumNumbers();
                    sumNumbers.Main();
                    break;
                case "10":
                    TwoNegatives twoNegatives = new TwoNegatives();
                    twoNegatives.Main();
                    break;
                case "11":
                    Multiples multiples = new Multiples();
                    multiples.Main();
                    break;
                case "12":
                    NumberRepeated numberRepeated = new NumberRepeated();
                    numberRepeated.Main();
                    break;
                case "13":
                    AccessControl accessControl = new AccessControl();
                    accessControl.Main();
                    break;
                case "14":
                    InfiniteDivisions infiniteDivisions = new InfiniteDivisions();
                    infiniteDivisions.Main();
                    break;
                case "15":
                    SeveralMultiplicationTables severalMultiplicationTables = new SeveralMultiplicationTables();
                    severalMultiplicationTables.Main();
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in data types section
        private static bool ChoiceForDataTypes(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    Char1 char1 = new Char1();
                    char1.Main();
                    break;
                case "2":
                    Triangle triangle = new Triangle();
                    triangle.Main();
                    break;
                case "3":
                    AccessControl accessControl = new AccessControl();
                    accessControl.Main();
                    break;
                case "4":
                    CalculatorIf calculatorIf = new CalculatorIf();
                    calculatorIf.Main();
                    break;
                case "5":
                    CalculatorSwitch calculatorSwitch = new CalculatorSwitch();
                    calculatorSwitch.Main();
                    break;
                case "6":
                    DoubleValue doubleValue = new DoubleValue();
                    doubleValue.Main();
                    break;
                case "7":
                    CalculateValuesFunction calculateValuesFunction = new CalculateValuesFunction();
                    calculateValuesFunction.Main();
                    break;
                case "8":
                    DisplayFunction displayFunction = new DisplayFunction();
                    displayFunction.Main();
                    break;
                case "9":
                    FloatSpeedUnits floatSpeedUnits = new FloatSpeedUnits();
                    floatSpeedUnits.Main();
                    break;
                case "10":
                    SphereFloat sphereFloat = new SphereFloat();
                    sphereFloat.Main();
                    break;
                case "11":
                    VowelSwitch vowelSwitch = new VowelSwitch();
                    vowelSwitch.Main();
                    break;
                case "12":
                    VowelIf vowelIf = new VowelIf();
                    vowelIf.Main();
                    break;
                case "13":
                    TriangleNE triangleNE = new TriangleNE();
                    triangleNE.Main();
                    break;
                case "14":
                    PrimeFactors primeFactors = new PrimeFactors();
                    primeFactors.Main();
                    break;
                case "15":
                    IfSymbols ifSymbols = new IfSymbols();
                    ifSymbols.Main();
                    break;
                case "16":
                    CharPlusFor charPlusFor = new CharPlusFor();
                    charPlusFor.Main();
                    break;
                case "17":
                    PerimeterAreaDiagonal perimeterAreaDiagonal = new PerimeterAreaDiagonal();
                    perimeterAreaDiagonal.Main();
                    break;
                case "18":
                    HexadecimalBinary hexadecimalBinary = new HexadecimalBinary();
                    hexadecimalBinary.Main();
                    break;
                case "19":
                    HexadecimalTable hexadecimalTable = new HexadecimalTable();
                    hexadecimalTable.Main();
                    break;
                case "20":
                    Binary binary = new Binary();
                    binary.Main();
                    break;
                case "21":
                    ConditionalBoolean conditionalBoolean = new ConditionalBoolean();
                    conditionalBoolean.Main();
                    break;
                case "22":
                    Exceptions exceptions = new Exceptions();
                    exceptions.Main();
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in flow control 2 section
        private static bool ChoiceForFlowControl2(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    Square square = new Square();
                    square.Main();
                    break;
                case "2":
                    BreakContinue breakContinue = new BreakContinue();
                    breakContinue.Main();
                    break;
                case "3":
                    Retangle2 retangle2 = new Retangle2();
                    retangle2.Main();
                    break;
                case "4":
                    RepetitiveStructures repetitiveStructures = new RepetitiveStructures();
                    repetitiveStructures.Main();
                    break;
                case "5":
                    DigitsInANumber digitsInANumber = new DigitsInANumber();
                    digitsInANumber.Main();
                    break;
                case "6":
                    HollowSquare hollowSquare = new HollowSquare();
                    hollowSquare.Main();
                    break;
                case "7":
                    Product product = new Product();
                    product.Main();
                    break;
                case "8":
                    AbsoluteValue absoluteValue = new AbsoluteValue();
                    absoluteValue.Main();
                    break;
                case "9":
                    HollowRetangle hollowRetangle = new HollowRetangle();
                    hollowRetangle.Main();
                    break;
                case "10":
                    Statistics statistics = new Statistics();
                    statistics.Main();
                    break;
                case "11":
                    SwitchMark switchMark = new SwitchMark();
                    switchMark.Main();
                    break;
                case "12":
                    ConditionalOPS conditionalOPS = new ConditionalOPS();
                    conditionalOPS.Main();
                    break;
                case "13":
                    PrimeNumber primeNumber = new PrimeNumber();
                    primeNumber.Main();
                    break;
                case "14":
                    ConditionalOperator conditionalOperator = new ConditionalOperator();
                    conditionalOperator.Main();
                    break;
                case "15":
                    GiveChange giveChange = new GiveChange();
                    giveChange.Main();
                    break;
                case "16":
                    Exceptions2 exceptions2 = new Exceptions2();
                    exceptions2.Main();
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in arrays, structures and strings section
        private static bool ChoiceForArraysStructuresAndStrings(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    ReverseArray reverseArray = new ReverseArray();
                    reverseArray.Main();
                    break;
                case "2":
                    SearchInArray searchInArray = new SearchInArray();
                    searchInArray.Main();
                    break;
                case "3":
                    ArrayEvenNumbers arrayEvenNumbers = new ArrayEvenNumbers();
                    arrayEvenNumbers.Main();
                    break;
                case "4":
                    ArrayOfPositiveAndNegativeNumbers arrayOfPositiveAndNegativeNumbers = new ArrayOfPositiveAndNegativeNumbers();
                    arrayOfPositiveAndNegativeNumbers.Main();
                    break;
                case "5":
                    TwoDimensionalArray twoDimensionalArray = new TwoDimensionalArray();
                    twoDimensionalArray.Main();
                    break;
                case "6":
                    Statistics2 statistics2 = new Statistics2();
                    statistics2.Main();
                    break;
                case "7":
                    StructApp structApp = new StructApp();
                    structApp.Main();
                    break;
                case "8":
                    ArrayOfStruct arrayOfStruct = new ArrayOfStruct();
                    arrayOfStruct.Main();
                    break;
                case "9":
                    ArrayOfStructPlusMenu arrayOfStructPlusMenu = new ArrayOfStructPlusMenu();
                    arrayOfStructPlusMenu.Main();
                    break;
                case "10":
                    BooksDatabase booksDatabase = new BooksDatabase();
                    booksDatabase.Main();
                    break;
                case "11":
                    CenteredTriangle centeredTriangle = new CenteredTriangle();
                    centeredTriangle.Main();
                    break;
                case "12":
                    CitiesDatabase citiesDatabase = new CitiesDatabase();
                    citiesDatabase.Main();
                    break;
                case "13":
                    Banner banner = new Banner();
                    banner.Main();
                    break;
                case "14":
                    TriangleRightSide triangleRightSide = new TriangleRightSide();
                    triangleRightSide.Main();
                    break;
                case "15":
                    NestedStructs nestedStructs = new NestedStructs();
                    nestedStructs.Main();
                    break;
                case "16":
                    SortData sortData = new SortData();
                    sortData.Main();
                    break;
                case "17":
                    TwoDimensionalArrayAsBuffer twoDimensionalArrayAsBuffer = new TwoDimensionalArrayAsBuffer();
                    twoDimensionalArrayAsBuffer.Main();
                    break;
                case "18":
                    TwoDimensionalArray2 twoDimensionalArray2 = new TwoDimensionalArray2();
                    twoDimensionalArray2.Main();
                    break;
                case "19":
                    ComputerPrograms computerPrograms = new ComputerPrograms();
                    computerPrograms.Main();
                    break;
                case "20":
                    ExerciseTasks exerciseTasks = new ExerciseTasks();
                    exerciseTasks.Main();
                    break;
                case "21":
                    HouseholdAccounts householdAccounts = new HouseholdAccounts();
                    householdAccounts.Main();
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in functions section
        private static bool ChoiceForFunctions(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    FunctionsGreetingPlusFarewell functionsGreetingPlusFarewell = new FunctionsGreetingPlusFarewell();
                    functionsGreetingPlusFarewell.Main();
                    break;
                case "2":
                    FunctionWithParameters functionWithParameters = new FunctionWithParameters();
                    functionWithParameters.Main();
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in object-oriented programming section
        private static bool ChoiceForObjectOrientedProgramming(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    StudentAndTeacherTest studentAndTeacherTest = new StudentAndTeacherTest();
                    studentAndTeacherTest.Runnable();
                    break;
                case "2":
                    AlbumTest albumTest = new AlbumTest();
                    albumTest.Main();
                    break;
                case "3":
                    TestTable testTable = new TestTable();
                    testTable.Main();
                    break;
                case "4":
                    TestHouse testHouse = new TestHouse();
                    testHouse.Main();
                    break;
                case "5":
                    TestTables testTables = new TestTables();
                    testTables.Main();
                    break;
                case "6":
                    TextEncrypter textEncrypter = new TextEncrypter();
                    textEncrypter.Main();
                    break;
                case "7":
                    ComplexTest complexTest = new ComplexTest();
                    complexTest.Main();
                    break;
                case "8":
                    TestTable1 testTable1 = new TestTable1();
                    testTable1.Main();
                    break;
                case "9":
                    RandomNumberTest randomNumberTest = new RandomNumberTest();
                    randomNumberTest.Main();
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in file management section
        private static bool ChoiceForFileManagement(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    WritingTextFile writingTextFile = new WritingTextFile();
                    writingTextFile.Main();
                    break;
                case "2":
                    AppEndToTextFile appEndToTextFile = new AppEndToTextFile();
                    appEndToTextFile.Main();
                    break;
                case "3":
                    DisplayFileContent displayFileContent = new DisplayFileContent();
                    displayFileContent.Main();
                    break;
                case "4":
                    LoggerTest loggerTest = new LoggerTest();
                    loggerTest.Main();
                    break;
                case "5":
                    More more = new More();
                    more.Main();
                    break;
                case "6":
                    TextReplacer textReplacer = new TextReplacer();
                    textReplacer.Main();
                    break;
                case "7":
                    CountLettersInFile countLettersInFile = new CountLettersInFile();
                    countLettersInFile.Main();
                    break;
                case "8":
                    ReadingBinaryFile readingBinaryFile = new ReadingBinaryFile();
                    readingBinaryFile.Main();
                    break;
                case "9":

                    break;
                case "10":

                    break;
                case "11":

                    break;
                case "12":

                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in object persistence section
        private static bool ChoiceForObjectPersistence(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    
                    break;
                case "2":
                    
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Handles input which user gives to application in database section
        private static bool ChoiceFordatabase(string input)
        {
            bool swi = true;
            switch (input)
            {
                case "1":
                    DatabaseFull databaseCreation = new DatabaseFull();
                    databaseCreation.Main();
                    break;
                case "2":
                    
                    break;
                case "0":
                    swi = false;
                    break;
                default:
                    Console.WriteLine("");
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }
    }
}
