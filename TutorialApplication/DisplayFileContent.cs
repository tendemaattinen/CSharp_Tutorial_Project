﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software read text file and prints int content to command line. User
 *              gives path of file.
 */
 
using System;
using System.IO;

namespace TutorialApplication
{
    class DisplayFileContent
    {
        public void Main()
        {
            Console.WriteLine();
            // Try
            try
            {
                Console.Write("Enter path: ");
                string path = Console.ReadLine();
                Console.WriteLine();
                using (StreamReader sR = new StreamReader(path))
                {
                    string line;
                    while ((line = sR.ReadLine()) != null) // Checks if row is empty
                    {
                        Console.WriteLine(line);
                    }
                }

            }
            // Catch error
            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
            
        }
    }
} 