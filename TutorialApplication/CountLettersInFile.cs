﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software counts given letters from file named 'LoremIpsum.txt'. File is located in 'TutorialApplication\bin\Debug\netcoreapp2.0'.
 */

using System;
using System.IO;

namespace TutorialApplication
{
    // Class that counts given letter from given file
    class LetterCounter
    {
        public int CountLetters(string fileName, string letter, bool allLetters = true)
        {
            int amountOfLetters = 0; // Amount of letters in given file
            StreamReader sR = File.OpenText(fileName); // Opens file with StreamReader
            string line = "";
            // User wants to count all letters
            if (allLetters)
            {
                // Do until line is null
                do
                {
                    line = sR.ReadLine(); // Saves line from file
                    if (line != null) // If line is not null
                    {
                        for (int i = 0; i < line.Length; i++) // Go through all the letters in the line
                        {
                            if (line.Substring(i, 1) != " ") // Letter is not space
                            {
                                amountOfLetters++; // Adds one to count
                            }
                        }
                    }
                } while (line != null);
            }
            // User wants to count specific letter
            else
            {
                // Do until line is null
                do
                {
                    line = sR.ReadLine(); // Saves line from file
                    if (line != null) // If line is not null
                    {
                        for (int i = 0; i < line.Length; i++) // Go through all the letters in the line
                        {
                            if (line.Substring(i, 1) == letter) // Letter matches to given letter
                            {
                                amountOfLetters++; // Adds one to count
                            }
                        }
                    }
                } while (line != null);
            }
            return amountOfLetters;
        }
    }

    class CountLettersInFile
    {
        public void Main()
        {
            Console.WriteLine();
            int amount; // Amount of letters in file
            LetterCounter letterCounter = new LetterCounter();
            Console.Write("Enter a letter or 'all': ");
            string userInput = Console.ReadLine();
            if (userInput == "all") // User wants to count all letters
            {
                amount = letterCounter.CountLetters("LoremIpsum.txt", "a");
            }
            else // User wants to count specific letter
            {
                amount = letterCounter.CountLetters("LoremIpsum.txt", userInput, false);
            }
            Console.WriteLine("Amount of letters: {0}", amount);
        } 
    }
} 