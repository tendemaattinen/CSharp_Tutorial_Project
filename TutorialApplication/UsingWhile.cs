﻿using System;

namespace TutorialApplication
{
    class UsingWhile
    {
        // Asks number in while loop and multiplys given number by 10 until user gives 0
        public void Main()
        {
            Console.WriteLine("");
            Console.WriteLine("Give 0 if you want to stop");
            Console.WriteLine("");
            // Loop for number asking
            while (true)
            {
                Console.Write("Enter number: ");
                Int32.TryParse(Console.ReadLine(), out int number);
                // Breaks while loop if user gives 0
                if(number == 0)
                {
                    Console.WriteLine("Closing");
                    break;
                }
                // Multiplys number by 10 if user input isn't 0
                else
                {
                    Console.WriteLine("{0}", number * 10);
                    Console.WriteLine("");
                }
            }
        }
    }
}