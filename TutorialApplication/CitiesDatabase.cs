﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Database for saving cities names and count of inhabitants. User can add new city, view all cities,
 *              modify cities, insert new cities, delete cities, search cities and correct capitalization of cities.
 */

using System;

namespace TutorialApplication
{
    class CitiesDatabase
    {
        // Struct-structure for storing data from books
        struct City
        {
            public string name;
            public int numberOfInhabitants;
        }

        public void Main()
        {
            City[] cityArray = new City[500]; // Array for storing cities, max amount 500
            int countOfCitiesInArray = 0; // Count of the cities in the array
            (cityArray, countOfCitiesInArray) = CreateTestArray(cityArray, countOfCitiesInArray); // Creates test array for easier testing
            bool swi = true; //Switch for keeping menu looping so long as user wants
            while (swi)
            {
                Console.WriteLine();
                Menu(); // Prints menu
                Console.Write("Enter your choice: ");
                int.TryParse(Console.ReadLine(), out int userChoice);
                switch (userChoice)
                {
                    case 1: // User wants to add a new city
                        (cityArray, countOfCitiesInArray) = AddCityToArray(cityArray, countOfCitiesInArray);
                        break;
                    case 2: // User wants to display all cities
                        DisplayCities(cityArray, countOfCitiesInArray);
                        break;
                    case 3: // User wants to modify specific city from database
                        (cityArray, countOfCitiesInArray) = ModifyCity(cityArray, countOfCitiesInArray);
                        break;
                    case 4: // User wants to insert a new city to database
                        (cityArray, countOfCitiesInArray) = InsertNewCity(cityArray, countOfCitiesInArray);
                        break;
                    case 5: // User wants to delete specific city from database
                        (cityArray, countOfCitiesInArray) = DeleteCityFromArray(cityArray, countOfCitiesInArray);
                        break;
                    case 6: // User wants to search a city from database
                        SearchCityFromArray(cityArray, countOfCitiesInArray);
                        break;
                    case 7: // User wants to correct capitalization of the names of the cities
                        cityArray = CorrectCapitalization(cityArray, countOfCitiesInArray);
                        break;
                    case 0: // User wants to quit application
                        swi = false;
                        break;
                    default: // User gives invalid input
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        // Function to print menu
        private void Menu()
        {
            Console.WriteLine("*** Menu ***");
            Console.WriteLine("1) Add a new city");
            Console.WriteLine("2) View all cities");
            Console.WriteLine("3) Modify a city");
            Console.WriteLine("4) Insert a new city");
            Console.WriteLine("5) Delete a city");
            Console.WriteLine("6) Search a city");
            Console.WriteLine("7) Correct the capitalization");
            Console.WriteLine("0) Exit");
        }

        // Function to add city to array, returns City[] -array and count of items in array 
        private (City[], int) AddCityToArray(City[] array, int lenght)
        {
            if (lenght >= 500) // Checks if database is full
            {
                Console.WriteLine("Database is full!");
            }
            else
            {
                City city; // Creates new City
                Console.Write("Enter name of the city: ");
                city.name = Console.ReadLine();
                Console.Write("Enter number of inhabitants: ");
                int.TryParse(Console.ReadLine(), out city.numberOfInhabitants);
                array[lenght] = city; // Adds city to array
                lenght += 1; // Adds one to count
            }
            return (array, lenght);
        }

        // Function to insert a new city in specific place in database
        private (City[], int) InsertNewCity(City[] array, int lenght) 
        {
            if (lenght >= 500) // Checks if database is full
            {
                Console.WriteLine("Database is full!");
            }
            else
            {
                City city; // Creates new City
                Console.Write("Enter name of the city: ");
                city.name = Console.ReadLine();
                Console.Write("Enter number of inhabitants: ");
                int.TryParse(Console.ReadLine(), out city.numberOfInhabitants);
                Console.Write("Enter number of place in database: ");
                int.TryParse(Console.ReadLine(), out int place);
                place -= 1; // Removes one from place, because array starts from 0
                if (place < 0) // User gives negative number
                {
                    Console.WriteLine("Place in the database can't be negative number!");
                } else
                {
                    lenght += 1; // Adds one to count
                    if (place > lenght) // User gives place that is more than lenght of the database, adds city to end of the database
                    {
                        Console.WriteLine("Given placement is more than lenght of database, city will be added to end of the database.");
                        array[lenght - 1] = city;
                    }
                    else // Adds city to given placement
                    {
                        for (int i = lenght; i > place; i--) // Loop to move other cities one place back
                        {
                            array[i] = array[i - 1];
                        }
                        array[place] = city; // At end, inserts city to its right place
                    }
                }
            }
            return (array, lenght);
        }

        // Function to modify specific city in database
        private (City[], int) ModifyCity(City[] array, int lenght)
        {
            bool found = false; // Boolean value, which tells if city is found
            if (lenght == 0) // Checks if database is empty
            {
                Console.WriteLine("No cities in the database.");
            }
            else
            {
                Console.Write("Enter a name of the city: ");
                string name = Console.ReadLine();
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    if (array[i].name.ToLower() == name.ToLower()) // Checks if name in array is same as searched name
                    {
                        Console.Write("Enter '1' to modify name or '2' to modify inhabitant count: ");
                        int.TryParse(Console.ReadLine(), out int choice);
                        switch (choice)
                        {
                            case 1: // User wants to chance the name of the city
                                Console.Write("Enter a new name of the city: ");
                                array[i].name = Console.ReadLine();
                                Console.WriteLine("Name changed!");
                                break;
                            case 2: // User wants to change count of inhabitants
                                Console.Write("Enter a new inhabitant count: ");
                                int.TryParse(Console.ReadLine(), out array[i].numberOfInhabitants);
                                Console.WriteLine("Inhabitant count changed!");
                                break;
                            default: // User gives invalid input
                                Console.WriteLine("Invalid input");
                                break;
                        }
                        found = true; // Changes boolean value to true when name is found
                        break;
                    }
                }
                if (!found) // City is not found from database after going through the array
                {
                    Console.WriteLine("City '{0}' not found from database.", name);
                }
            }
            return (array, lenght);
        }
         
        // Function to display cities in the array
        private void DisplayCities(City[] array, int lenght)
        {
            if (lenght == 0) // Checks if database is empty
            {
                Console.WriteLine("No cities in the database.");
            }
            else
            {
                Console.WriteLine();
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    Console.WriteLine("{0}) {1}, {2} inhabitants", i + 1, array[i].name, array[i].numberOfInhabitants);
                }
            }
        }

        // Funcion to search specific city from array
        private void SearchCityFromArray(City[] array, int lenght)
        {
            bool found = false; // Boolean value, which tells if city is found
            if (lenght == 0) // Checks if database is empty
            {
                Console.WriteLine("No cities in the database.");
            }
            else
            {
                Console.Write("Enter a name of the city: ");
                string name = Console.ReadLine();
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    if (array[i].name.ToLower() == name.ToLower()) // Checks if name in array is same as searched name
                    {
                        Console.WriteLine("City '{0}' found from database.", name);
                        found = true; // Changes boolean value to true when name is found
                        if (array[i].name == name)
                        {
                            Console.WriteLine("Correct spelling: {0}", array[i].name);
                        }
                        break;
                    }
                }
                if (!found) // City is not found from database after going through the array
                {
                    Console.WriteLine("City '{0}' not found from database.", name);
                }
            }
        }

        // Function to delete specific city from database, returns City[] -array and count of items in array 
        private (City[], int) DeleteCityFromArray(City[] array, int lenght)
        {
            bool found = false; // Boolean value, which tells if city is found
            if (lenght == 0) // Checks if database is empty
            {
                Console.WriteLine("No cities in the database.");
            }
            else
            {
                Console.Write("Enter a name: ");
                string name = Console.ReadLine();
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    if (!found) // City haven't found yet
                    {
                        if (array[i].name.ToLower() == name.ToLower()) // Checks if name in array is same as searched name
                        {
                            found = true; // Changes boolean value to true when city is found
                        }
                    }
                    else // City is already found
                    {
                        array[i - 1] = array[i]; // Removes city by moving city to previous slot
                    }
                }
                if (!found) // City is not found from database after going through the array
                {
                    Console.WriteLine("City '{0}' not found from database.", name);
                }
                else // City is found and removed from database
                {
                    Console.WriteLine("City '{0}' removed from database.", name);
                    lenght -= 1; // Removes one from count of cities in array
                }
            }
            return (array, lenght);
        }

        // Function to corerct capitalizations of the cities in the database, returns City[] -array
        private City[] CorrectCapitalization(City[] array, int lenght)
        {
            if (lenght == 0) // Checks if database is empty
            {
                Console.WriteLine("No cities in the database.");
            }
            else
            {
                for (int i = 0; i < lenght; i++) // Loop for going through the array
                {
                    string splitString = array[i].name; // Temporary storage to save name for splitting
                    string tempString = ""; // Temporary string to make new city name
                    string[] words = splitString.Split(" "); // Splits string by space, creates string array
                    foreach (var word in words) // Loop for going throgh words
                    {
                        bool firstLetter = true; // Boolean value to check if char is first letter of the word
                        foreach (char c in word) // Loop ´for going through characters is the word
                        {
                            if (firstLetter) // Checks if character is first letter
                            {
                                char ch = Char.ToUpper(c); // Changes character to upper
                                firstLetter = false; // First letter boolean value to false
                                tempString = tempString + ch; // Adds character to end of temporary string
                            }
                            else // Character is not first letter of the word
                            {
                                char ch = Char.ToLower(c); //Changes character to lower
                                tempString = tempString + ch; // Adds character to end of temporary string
                            }
                        }
                        tempString = tempString + " "; // Adds space to end of temporary string
                    }
                    tempString = tempString.Remove(tempString.Length - 1); // Removes last white space from temporary string
                    array[i].name = tempString; // Changes citys name in array to temporary string
                }
            }
            Console.WriteLine();
            Console.WriteLine("Capitalizations corrected!");
            return array;
        }

        // Function to create test array for easier testing of the application, returns City[] -array and lenght of it
        private (City[], int) CreateTestArray(City[] array, int lenght)
        {
            City city1, city2, city3, city4, city5, city6, city7, city8, city9, city10;
            city1.name = "helSINki is winner";
            city1.numberOfInhabitants = 230000;
            city2.name = "LontOo is niCe";
            city2.numberOfInhabitants = 1200000;
            city3.name = "TukHolma is aWsoMe";
            city3.numberOfInhabitants = 4355353;
            city4.name = "Moskova";
            city4.numberOfInhabitants = 23450000;
            city5.name = "Pariisi";
            city5.numberOfInhabitants = 7675000;
            city6.name = "Oslo";
            city6.numberOfInhabitants = 353000;
            city7.name = "Madrid";
            city7.numberOfInhabitants = 546556;
            city8.name = "Lappeenranta";
            city8.numberOfInhabitants = 343434234;
            city9.name = "Berliini";
            city9.numberOfInhabitants = 23340000;
            city10.name = "Rooma";
            city10.numberOfInhabitants = 4530000;
            array[0] = city1;
            array[1] = city2;
            array[2] = city3;
            array[3] = city4;
            array[4] = city5;
            array[5] = city6;
            array[6] = city7;
            array[7] = city8;
            array[8] = city9;
            array[9] = city10;
            lenght = 10;
            return (array, lenght);
        }
    }
} 