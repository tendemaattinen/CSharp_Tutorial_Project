﻿using System;

namespace TutorialApplication
{
    class Char1
    {
        // Asks 3 letters from user and prints them in reversed order
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Give first letter: ");
            char letter1 = Convert.ToChar(Console.ReadLine());
            Console.Write("Give second letter: ");
            char letter2 = Convert.ToChar(Console.ReadLine());
            Console.Write("Give third letter: ");
            char letter3 = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("{0} {1} {2}", letter3, letter2, letter1);
        }
    }
}