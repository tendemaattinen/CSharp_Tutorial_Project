﻿using System;

namespace TutorialApplication
{
    class NumberRepeated
    {
        // Asks number from user and prints it as many times as user wants
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter a number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            Console.Write("Enter an amount: ");
            Int32.TryParse(Console.ReadLine(), out int amount);
            for (int i = 0; i < amount; i++)
            {
                Console.Write(number);
            }
            Console.WriteLine("");
        }
    }
}