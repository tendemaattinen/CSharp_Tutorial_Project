﻿using System;

namespace TutorialApplication
{
    class Retangle2
    {
        // Draws retangle from given parameters
        public void Main()
        {
            while (true)
            {
                Console.WriteLine();
                Console.Write("Enter a number or 0 to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number);
                // Checks if user wants to quit application
                if (number == 0)
                {
                    break;
                }
                Console.Write("Enter the width: ");
                Int32.TryParse(Console.ReadLine(), out int width);
                Console.Write("Enter the height: ");
                Int32.TryParse(Console.ReadLine(), out int height);
                Console.WriteLine();
                // Height of retangle
                for (int i = 0; i < height; i++)
                {
                    // Width of retangle
                    for (int j = 0; j < width; j++)
                    {
                        Console.Write(number);
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}