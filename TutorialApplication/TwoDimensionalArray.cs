﻿using System;

namespace TutorialApplication
{
    class TwoDimensionalArray
    {
        // Application ask marks for 20 students, divided to two groups and shows average marks of those groups
        public void Main()
        {
            Console.WriteLine();
            int[,] dataArray = CreateArray(); // Array for marks
            Console.WriteLine();
            Console.WriteLine("Average of group 1: {0}", AverageOfArray(dataArray, 0));
            Console.WriteLine("Average of group 2: {0}", AverageOfArray(dataArray, 1));
        }

        // Creates array for the marks, returns two-dimensional array
        private int[,] CreateArray()
        {
            int[,] array = new int[2, 10]; // Array to storage marks
            for (int i = 0; i < 10; i++) // For-loop to asks marks ten times for group 1
            {
                Console.Write("Enter the puntuation {0} group 1: ", i + 1);
                Int32.TryParse(Console.ReadLine(), out int number);
                array[0 , i] = number;
            }
            for (int j = 0; j < 10; j++) // For-loop to asks marks ten times for group 2
            {
                Console.Write("Enter the puntuation {0} group 2: ", j + 1);
                Int32.TryParse(Console.ReadLine(), out int number1);
                array[1, j] = number1;
            }
            return array;
        }

        // Calculates average of numbers in array, returns average as double
        private double AverageOfArray(int[,] array, int groupNumber)
        {
            double sumOfArray = 0; // Sum of array
            double numberOfNumbers = 0; // Number of numbers in array
            for (int i = 0; i < 10; i++) // For-loop to calculate average
            {
                sumOfArray += array[groupNumber, i];
                numberOfNumbers += 1;
            }
            double averageOfArray = sumOfArray / numberOfNumbers; // Average of nmarks
            return averageOfArray;
        }
    }
}