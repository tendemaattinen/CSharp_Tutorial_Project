﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software which contains different photo albums
 */ 
 
using System;

namespace TutorialApplication
{
    // Class for photo album
    class PhotoAlbum
    {

        protected int numberOfPages; // Number of pages in the photo album

        // Method to get numbers of the photo album
        public int GetNumberOfPages()
        {
            return numberOfPages;
        }

        // Default constructor
        public PhotoAlbum()
        {
            numberOfPages = 16;
        }

        // Constructor with change to make different size photo album
        public PhotoAlbum(int pages)
        {
            numberOfPages = pages;
        }
    }

    // Class for big photo album, inheritance of photo album
    class BigPhotoAlbum : PhotoAlbum
    {
        public BigPhotoAlbum()
        {
            numberOfPages = 64;
        }
    }

    // Class with runnable
    class AlbumTest
    {
        public void Main()
        {
            Console.WriteLine();
            PhotoAlbum defaultPhotoAlbum = new PhotoAlbum();
            int pagesOfDefaultPhotoAlbum = defaultPhotoAlbum.GetNumberOfPages();
            Console.WriteLine("Number of pages in default photo album: {0}", pagesOfDefaultPhotoAlbum);
            PhotoAlbum biggerPhotoAlbum = new PhotoAlbum(24);
            int pagesOfBiggerPhotoAlbum = biggerPhotoAlbum.GetNumberOfPages();
            Console.WriteLine("Number of pages in bigger photo album: {0}", pagesOfBiggerPhotoAlbum);
            BigPhotoAlbum bigPhotoAlbum = new BigPhotoAlbum();
            int pagesOfBigPhotoAlbum = bigPhotoAlbum.GetNumberOfPages();
            Console.WriteLine("Number of pages in big photo album: {0}", pagesOfBigPhotoAlbum);
        }
    }
} 