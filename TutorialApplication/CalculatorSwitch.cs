﻿using System;

namespace TutorialApplication
{
    class CalculatorSwitch
    {
        // Basic calculator with switch-statement
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Enter operation: ");
            string operation = Console.ReadLine();
            Console.Write("Enter second number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            switch (operation)
            {
                case "+":
                    Console.WriteLine("{0} + {1} = {2}", number1, number2, (number1 + number2));
                    break;
                case "-":
                    Console.WriteLine("{0} - {1} = {2}", number1, number2, (number1 - number2));
                    break;
                case "*":
                    Console.WriteLine("{0} * {1} = {2}", number1, number2, (number1 * number2));
                    break;
                case "/":
                    // Checks if divided by zero
                    if (number2 != 0)
                    {
                        Console.WriteLine("{0} / {1} = {2}", number1, number2, (number1 / number2));
                    }
                    else
                    {
                        Console.WriteLine("Cannot divide by zero!");
                    }
                    break;
                case "%":
                    // Checks if divided by zero
                    if (number2 != 0)
                    {
                        Console.WriteLine("{0} % {1} = {2}", number1, number2, (number1 % number2));
                    }
                    else
                    {
                        Console.WriteLine("Cannot divide by zero!");
                    }
                    break;
                default:
                    Console.WriteLine("Operation unknown!");
                    break;
            }
        }
    }
}