﻿using System;

namespace TutorialApplication
{
    class ConditionalBoolean
    {
        // Checks if given numbers are even or odd
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter the first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Enter the second number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            if ((number1 % 2 == 0) && (number2 % 2 == 0))
            {
                Console.WriteLine("Both");
            }
            else
            {
                Console.WriteLine("Odd");
            }
        }
    }
}