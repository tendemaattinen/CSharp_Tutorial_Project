﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Small database to store books. User can add books, display added books, search books
 *              and remove books from database.
 */
 
using System;

namespace TutorialApplication
{
    class BooksDatabase
    {
        // Struct-structure for storing data from books
        struct Book
        {
            public string title;
            public string author;
        }

        public void Main()
        {
            Book[] bookArray = new Book[1000]; // Array for storing Books, max amount 1000
            int countOfBooksInArray = 0; // Count of the books in the array
            bool swi = true; //Switch for keeping menu looping so long as user wants
            while (swi)
            {
                Console.WriteLine();
                Menu(); // Prints menu
                Console.Write("Enter your choice: ");
                int.TryParse(Console.ReadLine(), out int userChoice);
                switch (userChoice)
                {
                    case 1: // User wants to add book
                        (bookArray, countOfBooksInArray) = AddBookToArray(bookArray, countOfBooksInArray);
                        break;
                    case 2: // User wants to display books
                        DisplayBooks(bookArray, countOfBooksInArray);
                        break;
                    case 3: // User wants to search for specific book from database
                        SearchBookFromArray(bookArray, countOfBooksInArray);
                        break;
                    case 4: // User wants to delete specific book from database
                        (bookArray, countOfBooksInArray) = DeleteBookFromArray(bookArray, countOfBooksInArray);
                        break;
                    case 0: // User wants to quit application
                        swi = false;
                        break;
                    default: // User gives invalid input
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        // Function to print menu
        private void Menu()
        {
            Console.WriteLine("*** Menu ***");
            Console.WriteLine("1) Add book");
            Console.WriteLine("2) Display books");
            Console.WriteLine("3) Search book");
            Console.WriteLine("4) Delete book");
            Console.WriteLine("0) Exit");
        }

        // Function to add book to array, returns Book[] -array and count of items in array 
        private (Book[], int) AddBookToArray(Book[] array, int lenght)
        {
            Book book; // Creates new Book
            Console.Write("Title of the book: ");
            book.title = Console.ReadLine();
            Console.Write("Author of the book: ");
            book.author = Console.ReadLine();
            array[lenght] = book; // Adds book to array
            lenght += 1; // Adds one to count
            return (array, lenght);
        }

        // Function to display books in the array
        private void DisplayBooks(Book[] array, int lenght)
        {
            if (lenght == 0) // Checks ifdatabase is empty
            {
                Console.WriteLine("No books in the database.");
            }
            else
            {
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    Console.WriteLine("{0}) '{1}' by {2}", i+1, array[i].title, array[i].author);
                }
            }
        }

        // Funcion to search specific book from array
        private void SearchBookFromArray(Book[] array, int lenght)
        {
            bool found = false; // Boolean value, which tells if book is found
            if (lenght == 0) // Checks if database is empty
            {
                Console.WriteLine("No books in the database.");
            }
            else
            {
                Console.Write("Enter a title: ");
                string title = Console.ReadLine();
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    if (array[i].title == title) // Checks if title in array is same as searched title
                    {
                        Console.WriteLine("Book '{0}' found from database.", title);
                        found = true; // Changes boolean value to true when book is found
                    }
                }
                if (!found) // Book is not found from database after going through the array
                {
                    Console.WriteLine("Book '{0}' not found from database.", title);
                }
            }
        }

        private (Book[], int) DeleteBookFromArray(Book[] array, int lenght)
        {
            bool found = false; // Boolean value, which tells if book is found
            if (lenght == 0) // Checks if database is empty
            {
                Console.WriteLine("No books in the database.");
            }
            else
            {
                Console.Write("Enter a title: ");
                string title = Console.ReadLine();
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    if (!found) // Book haven't found yet
                    {
                        if (array[i].title == title) // Checks if title in array is same as searched title
                        {
                            found = true; // Changes boolean value to true when book is found
                        }
                    }
                    else // Book is already found
                    {
                        array[i - 1] = array[i]; // Removes book by moving book to previous slot
                    }
                }
                if (!found) // Book is not found from database after going through the array
                {
                    Console.WriteLine("Book '{0}' not found from database.", title);
                }
                else // Book is found and removed from database
                {
                    Console.WriteLine("Book '{0}' removed from database.", title);
                    lenght -= 1; // Removes one from count of books in array
                }
            }
            return (array, lenght);
        }
    }
}