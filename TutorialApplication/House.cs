﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software creates house, door and person-classes.
 */
 
using System;

namespace TutorialApplication
{
    // Class for house
    class House
    {
        protected int Area { get; set; }
        public Door Door { get; set; }

        // Constuctor
        public House(int a)
        {
            Area = a;
            Door = new Door();
        }

        // Method to show houses data
        public virtual void ShowData()
        {
            Console.WriteLine("I am a house, my area is {0}m2", Area);
        }
    }

    // Class for door
    class Door
    {
        protected string Color { get; set; }

        // Constructor
        public Door()
        {
            Color = "Brown";
        }

        // Constructor with input
        public Door(string col)
        {
            Color = col;
        }

        // Method to show doors data
        public void ShowData()
        {
            Console.WriteLine("I am a door, my color is {0}.", Color);
        }
    }

    // Class for small aparment, inheritance of house
    class SmallApartment : House
    {
        // Constructor
        public SmallApartment() : base(50)
        {
            
        }

        // Method to show small apartments data, overrides houses ShowData-method
        public override void ShowData()
        {
            Console.WriteLine("I am an apartment, my area is {0}m2", Area);
        }
    }

    // Class for person
    class Person1
    {
        public string Name { get; set; }
        public House House { get; set; }

        // Constructor
        public Person1()
        {
            Name = "Juan";
            House = new House(150);
        }

        // Constuctor with input
        public Person1(string name1, House house1)
        {
            Name = name1;
            House = house1;
        }

        // Method to show persons data
        public void ShowData()
        {
            Console.WriteLine("My name is {0}.", Name);
            House.ShowData();
            House.Door.ShowData();
        }

    }

    // Class for testing, includes Main-function
    class TestHouse
    {
        public void Main()
        {
            Console.WriteLine();
            SmallApartment smallApartment = new SmallApartment();
            Person1 person = new Person1
            {
                Name = "Kalle",
                House = smallApartment
            };
            person.ShowData();
        }
    }
} 