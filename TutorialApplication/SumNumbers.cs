﻿using System;

namespace TutorialApplication
{
    class SumNumbers
    {
        // Sums given numbers together until user gives 0
        public void Main()
        {
            int sum = 0;
            Console.WriteLine("");
            Console.WriteLine("Give a number of give 0 to end");
            while(true)
            {
                Console.WriteLine("");
                Console.Write("Enter a number: ");
                Int32.TryParse(Console.ReadLine(), out int number);
                if(number == 0)
                {
                    Console.WriteLine("Closing");
                    break;
                }
                else
                {
                    sum = sum + number;
                    Console.WriteLine("Sum: {0}", sum);
                }
            }

        }
    }
}