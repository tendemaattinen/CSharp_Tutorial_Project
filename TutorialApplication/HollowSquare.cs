﻿using System;

namespace TutorialApplication
{
    // Application draws hollow square from given symbol and width
    class HollowSquare
    {
        public void Main()
        {
            while (true)
            {
                Console.WriteLine();
                Console.Write("Enter a symbol or 'exit' to quit: ");
                string symbol = Console.ReadLine();
                // Checks if input is 'exit' and user wants to quit
                if (symbol == "exit")
                {
                    Console.WriteLine("Exiting");
                    break;
                }
                Console.Write("Enter the width: ");
                Int32.TryParse(Console.ReadLine(), out int width);
                Console.WriteLine();

                // Draws square, height loop
                for (int i = 0; i < width; i++)
                {
                    // Width loop
                    for (int j = 0; j < width; j++)
                    {
                        // Checks if current row is first or last, in which case it draws only symbols
                        if ((i == 0) || (i == width-1))
                        {
                            Console.Write(symbol);
                        }
                        // Row isn't first or last, in which case draws symbol in the first and last place
                        else
                        {
                            if ((j == 0) || (j == width - 1))
                            {
                                Console.Write(symbol);
                            }
                            else
                            {
                                Console.Write(" ");
                            }
                        }
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}