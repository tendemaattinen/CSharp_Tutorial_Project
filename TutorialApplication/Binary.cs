﻿using System;

namespace TutorialApplication
{
    class Binary
    {
        // Asks a number from user and converts it to binary without usage of Convert.ToString()
        public void Main()
        {
            while (true)
            {
                int remainder;
                string binary = "", temp;
                Console.WriteLine("");
                Console.Write("Enter a number or exit: ");
                string str = Console.ReadLine();
                // Breaks loop if user enters 'exit'
                if (str == "exit")
                {
                    break;
                } else
                {
                    Int32.TryParse(str, out int number);
                    // While-loop for division to get binary
                    while (number != 1)
                    {
                        remainder = number % 2;
                        number = number / 2;
                        temp = remainder.ToString();
                        binary += remainder;
                    }
                    binary += "1";
                    Console.WriteLine(Reverse(binary));
                }
            }
        }

        // Reverses string
        private  string Reverse(string str)
        {
            char[] charArray = str.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}