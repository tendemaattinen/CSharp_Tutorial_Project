﻿using System;

namespace TutorialApplication
{
    class DisplayFunction
    {
        // Draws function y = (x-4)2
        public void Main()
        {
            Console.WriteLine("");
            for (int startNumber = -1; startNumber < 9; startNumber++)
            {
                Draw(Calc(startNumber));
            }

        }

        // Calculates the value in given number
        private int Calc(int number)
        {
            int answer = ((number - 4) * (number - 4));
            return answer;
        }

        // 'Draws' line with *
        private void Draw(int number)
        {
            for (int i = 0; i < number; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine("");
        }
    }
}