﻿/* 
 * Author: Teemu Tynkkynen
 * Description:
 */
 
using System;
using System.IO;
using System.Data.SQLite;


namespace TutorialApplication
{
    class DatabaseFull
    {
        public void Main()
        {
            SQLiteConnection connection;
            SQLiteCommand command;
            bool swi = true;

            Console.WriteLine();
            Console.WriteLine("*** Books database ***");

            try
            {
                connection = new SQLiteConnection("Data Source = myDatabase.sqlite; Version = 3;");
                connection.Open();

                if (!File.Exists("myDatabase.sqlite"))
                {
                    SQLiteConnection.CreateFile("myDatabase.sqlite");
                    string sql = "CREATE TABLE books (title VARCHAR(50), author VARCHAR(50), genre VARCHAR(50), summary VARCHAR(250))";
                    command = new SQLiteCommand(sql, connection);
                    command.ExecuteNonQuery();
                }

                while (swi)
                {
                    Console.WriteLine();
                    PrintMenu();
                    Console.Write("Enter: ");
                    string userInput = Console.ReadLine();
                    switch (userInput)
                    {
                        case "1":
                            AddNewBook(connection);
                            break;
                        case "2":
                            ShowAllBooks(connection);
                            break;
                        case "0":
                            swi = false;
                            break;
                        default:
                            Console.WriteLine("Invalid input!");
                            break;
                    }
                }

                Console.WriteLine("");
                Console.WriteLine("Closing...");
                connection.Close();
                Console.WriteLine("Database closed.");
            }

            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
        }

        private void PrintMenu()
        {
            Console.WriteLine("1) Enter a new book");
            Console.WriteLine("2) Show all books");
            Console.WriteLine("0) Exit");
        }

        private void AddNewBook(SQLiteConnection conn)
        {
            string title, author, genre, summary, sql;
            Console.WriteLine();
            do
            {
                Console.Write("Title: ");
                title = Console.ReadLine();
            } while (title == "");
            do
            {
                Console.Write("Author: ");
                author = Console.ReadLine();
            } while (author == "");
            do
            {
                Console.Write("Genre: ");
                genre = Console.ReadLine();
            } while (genre == "");

            Console.Write("Summary: ");
            summary = Console.ReadLine();
            if (summary == "")
            {
                summary = "Summary not provided.";
            }

            sql = "INSERT INTO books (title, author, genre, summary) VALUES ('" + title + "', '" + author + "', '" + genre + "', '" + summary + "')";

            SQLiteCommand command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();
            Console.WriteLine();
            Console.WriteLine("Book added to database!");
        }

        private void ShowAllBooks(SQLiteConnection conn)
        {
            string sql = "SELECT * FROM books";
            int i = 0;

            SQLiteCommand command = new SQLiteCommand(sql, conn);
            SQLiteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                i++;
                Console.WriteLine("{0}) Title: " + reader["title"] + " | Author: " + reader["author"] + " | Genre: " + reader["genre"] + " | Summary: " + reader["summary"], i);
            }
        }
    }
} 