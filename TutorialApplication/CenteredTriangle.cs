﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application asks string from user and print centered triangle from
 *              given string.
 */
 
using System;

namespace TutorialApplication
{
    class CenteredTriangle
    {
        public void Main()
        {
            Console.WriteLine();
            Console.Write("Enter a string: ");
            string userInput = Console.ReadLine();
            if (userInput.Length % 2 == 0) // Checks if lenght of string is even, if it is, adds space to end of the string
            {
                userInput += " ";
            }

            int maxRows = ((userInput.Length) / 2) + 1; // Number of rows have to print
            int lenght = 1; // Lenght of printable portion of the string
            int placement = (userInput.Length) / 2; // Place from where to start printing string

            for (int i = 0; i < maxRows; i++) // Prints rows with for-loop
            {
                for (int j = 0; j < userInput.Length; j++) // Prints characters with for-loop
                {
                    if (j == placement) // Checks if it is right place to start printing string
                    {
                        Console.Write(userInput.Substring(placement, lenght));
                    }
                    else // Prints spce if it isn't right place to start
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
                lenght += 2; // Adds two to lenght
                placement -= 1; // Reduce one from placement
            }
        }
    }
}