﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Prints greetings and farewell to user.
 */
 
using System;

namespace TutorialApplication
{
    class FunctionsGreetingPlusFarewell
    {
        public void Main()
        {
            Console.WriteLine();
            SayHello();
            SayGoodBye();
            
        }

        // Function to print "Hello"
        private void SayHello()
        {
            Console.WriteLine("Hello");
        }

        // Function to print "Good Bye!"
        private void SayGoodBye()
        {
            Console.WriteLine("Good Bye!");
        }
    }
} 