﻿using System;

namespace TutorialApplication
{
    class CharPlusFor
    {
        // Prints letters from B to N
        public void Main()
        {
            Console.WriteLine("");
            for (char i = 'B'; i <= 'N'; i++)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine("");
        }
    }
}