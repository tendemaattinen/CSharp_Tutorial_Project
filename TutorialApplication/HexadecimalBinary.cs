﻿using System;

namespace TutorialApplication
{
    class HexadecimalBinary
    {
        // Asks number from user and displays it as hexadecimal and binary
        public void Main()
        {
            while(true)
            {
                Console.WriteLine("");
                Console.Write("Enter a number: ");
                Int32.TryParse(Console.ReadLine(), out int number);
                // Checks if user wants to quit application
                if (number == 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("");
                    Hexa(number);
                    Bina(number);
                }
            }
        }

        // Converter to hexadecimal
        private void Hexa(int num)
        {
            Console.WriteLine(Convert.ToString(num, 16));
        }

        // Converter to binary
        private void Bina(int num)
        {
            Console.WriteLine(Convert.ToString(num, 2));
        }
    }
}