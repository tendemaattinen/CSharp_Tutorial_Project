﻿using System;

namespace TutorialApplication
{
    class GreatestOfThreeNumber
    {
        // Asks three numbers from user and tells which of those numbers is biggest
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Give the first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Give the second number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            Console.Write("Give the third number: ");
            Int32.TryParse(Console.ReadLine(), out int number3);
            Console.WriteLine("{0} is the biggest number", Math.Max(Math.Max(number1, number2), number3));
        }
    }
}