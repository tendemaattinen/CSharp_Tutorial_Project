﻿using System;

namespace TutorialApplication
{
    class DoubleValue
    {
        // Calculates perimeter, area and diagonal from given numbers
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter the width: ");
            Double.TryParse(Console.ReadLine(), out double width);
            Console.Write("Enter the height: ");
            Double.TryParse(Console.ReadLine(), out double height);
            Console.WriteLine("");
            Console.WriteLine("Perimeter: {0}", ((width * 2) + (height * 2)));
            Console.WriteLine("Area: {0}", (width * height));
            Console.WriteLine("Diagonal: {0}", (Math.Sqrt((width * width) + (height * height))));
        }
    }
}