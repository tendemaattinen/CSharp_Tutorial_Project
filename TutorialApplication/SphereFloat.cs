﻿using System;

namespace TutorialApplication
{
    class SphereFloat
    {
        // Calculates surface and volume of sphere from its radius
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter a radius: ");
            float.TryParse(Console.ReadLine(), out float radius);
            float surface = (4 * (float)Math.PI * (float)Math.Pow(radius, 2));
            float volume = ((4 / 3) * (float)Math.PI * (float)Math.Pow(radius, 3));
            Console.WriteLine("Surface: {0}", surface);
            Console.WriteLine("Volume: {0}", volume);
        }
    }
}