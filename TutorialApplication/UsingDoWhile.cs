﻿using System;

namespace TutorialApplication
{
    class UsingDoWhile
    {
        // Asks number from user and multiplys it by 10 until user gives zero
        public void Main()
        {
            int number;
            Console.WriteLine("");
            Console.WriteLine("Give 0 if you want to stop");
            Console.WriteLine("");
            do
            {
                Console.Write("Enter number: ");
                Int32.TryParse(Console.ReadLine(), out number);
                Console.WriteLine("{0}", number * 10);
            }
            while (number != 0);

        }
    }
}