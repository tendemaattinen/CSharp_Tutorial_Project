﻿using System;

namespace TutorialApplication
{
    class BreakContinue
    {
        // Aplication that prints even numbers from 10 to 20 three different ways
        public void Main()
        {
            Console.WriteLine("");

            // Incrementing 2 in each step and 'continue' to skip 16
            for (int i = 10; i <= 20; i += 2)
            {
                if (i == 16)
                {
                    continue;
                }
                Console.Write("{0} ", i);
            }

            Console.WriteLine("");

            // Incrementing 1 in each step and 'continue'
            for (int j = 10; j <= 20; j++)
            {
                if ((j == 16) || (j % 2 == 1))
                {
                    continue;
                }
                Console.Write("{0} ", j);
            }

            Console.WriteLine("");

            // Endless loop
            for (int k = 10; ; k += 2)
            {
                if (k > 20)
                {
                    break;
                }
                if (k == 16)
                {
                    continue;
                }
                Console.Write("{0} ", k);
            }

            Console.WriteLine("");
        }
    }
}