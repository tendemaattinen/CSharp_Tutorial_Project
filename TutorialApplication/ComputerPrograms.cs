﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Program that can store up to 1000 computer programs. User can add new, show all, show specific, update,
 *              delete, sort alphabetically and fix redundant spaces.
 */
 
using System;

namespace TutorialApplication
{
    class ComputerPrograms
    {
        // Struct for programs
        struct Program
        {
            public string name;
            public string category;
            public string description;
            public Version version;
        }

        // Nested struct for version
        struct Version
        {
            public string versionNumber;
            public byte launchMonth;
            public ushort launchYear;
        }

        private readonly Program[] programArray = new Program[1000]; // Array to store programs
        private int countOfProgramsInArray = 0; // Count of programs in the array

        // Main function
        public void Main()
        {
            bool swi = true; // Boolean value to run which-loop
            // PremadeArray(); // Example array
            Console.WriteLine();
            Console.WriteLine("*** Computer Programs ***");
            while (swi) // Loop for running menu
            {
                PrintMainMenu(); // Prints main menu
                Console.Write("Enter your choice: ");
                string userinput = Console.ReadLine();
                swi = ChoiceForAction(userinput); // Execute action wanted by user
            }

        }

        // Function to print main menu for application
        private void PrintMainMenu()
        {
            Console.WriteLine();
            Console.WriteLine("*** Main menu ***");
            Console.WriteLine("1) Add a new program");
            Console.WriteLine("2) Show all programs");
            Console.WriteLine("3) Show all data from program");
            Console.WriteLine("4) Update program");
            Console.WriteLine("5) Delete program");
            Console.WriteLine("6) Sort programs alphabetically by name");
            Console.WriteLine("7) Fix redundant spaces");
            Console.WriteLine("0) Exit application");
        }

        // Function to execute users wanted action, return boolean value, which is defaulted to true.
        // In case user wants to quit, boolean value is returned as false.
        private bool ChoiceForAction(string choice)
        {
            bool swi = true;
            switch (choice)
            {
                case "1": // USer wants to add new program
                    AddNewProgram();
                    break;
                case "2": // User wants to see all programs in the database
                    ShowAllPrograms();
                    break;
                case "3": // User wants to see all data from specific program
                    ShowAllDataFromProgram();
                    break;
                case "4": // User wants to update program that is already in the database
                    UpdateProgram();
                    break;
                case "5": // User wants to delete a program
                    DeleteProgram();
                    break;
                case "6": // User wants to sort database alphabetically by name
                    SortDatabase();
                    break;
                case "7": // USer wants to fix redundant spaces
                    FixSpaces();
                    break;
                case "0": // User wants to quit program
                    swi = false;
                    break;
                default: // User gives invalid input
                    Console.WriteLine();
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return swi;
        }

        // Function to add new program to database
        private void AddNewProgram()
        {
            if (countOfProgramsInArray >= 1000) // Checks if database is already full
            {
                Console.WriteLine();
                Console.WriteLine("Database is full!");
            }
            else
            {
                Program program = new Program(); // Creates a new empty program

                do // Asks name as long as input isn't empty 
                {
                    Console.Write("Enter a name: ");
                    program.name = Console.ReadLine();
                } while (program.name.Length == 0);

                do // Asks category as long as string isn't over 30 char
                {
                    Console.Write("Enter a category: ");
                    program.category = Console.ReadLine();
                } while (program.category.Length > 30);

                Console.Write("Enter a description: ");
                program.description = Console.ReadLine();

                if (program.description.Length > 100) // If description is over 100 char, reduces it to 100 char
                {
                    program.description = program.description.Substring(0, 100);
                }

                Console.Write("Enter a version number: ");
                program.version.versionNumber = Console.ReadLine();
                Console.Write("Enter a launch month: ");
                program.version.launchMonth = Convert.ToByte(Console.ReadLine());
                Console.Write("Enter a launch year: ");
                program.version.launchYear = Convert.ToUInt16(Console.ReadLine());

                programArray[countOfProgramsInArray] = program; // Adds created program to array
                countOfProgramsInArray += 1; // Adds one to counter
            }
        }

        // Function to show all names of the programs in the database
        private void ShowAllPrograms()
        {
            if (countOfProgramsInArray <= 0) // Checks if database is empty
            {
                Console.WriteLine();
                Console.WriteLine("Database is empty!");
            }
            else
            {
                int i = 0; // Counter
                while (i < countOfProgramsInArray)
                {
                    Console.WriteLine();
                    for (int j = 0; j < 20; j++)
                    {
                        Console.WriteLine("{0}) {1}", i + 1, programArray[i].name);
                        i++;
                        if (i == countOfProgramsInArray)
                        {
                            break;
                        }
                    }
                    Console.WriteLine();
                    Console.Write("Press enter to continue...");
                    Console.ReadLine();
                }
            }
        }

        // Function to search specific program from database
        private void ShowAllDataFromProgram()
        {
            bool found = false;
            Console.WriteLine();
            if (countOfProgramsInArray <= 0)
            {
                Console.WriteLine("Database is empty!");
            }
            else
            {
                Console.Write("Enter a part of name, category or description (case sensetive): ");
                string userInput = Console.ReadLine();
                Console.WriteLine();
                for (int i = 0; i < countOfProgramsInArray; i++)
                {
                    if (programArray[i].name.Contains(userInput) 
                    || programArray[i].category.Contains(userInput)
                    || programArray[i].description.Contains(userInput)) // Checks if name, category or description contains user input
                    {
                        found = true;
                        Console.WriteLine("Location: '{0}'   Name: '{1}'   Category: '{2}'   Description: '{3}'",
                        i + 1, programArray[i].name, programArray[i].category, programArray[i].description); // Should add more details about program??
                    }
                }
                if (!found)
                {
                    Console.WriteLine("'{0}' not found from database", userInput);
                }
            }
        }

        // Function to update program which alraedy exist in database
        private void UpdateProgram()
        {
            Console.WriteLine();
            if (countOfProgramsInArray <= 0) // Checks if database is empty
            {
                Console.WriteLine("Database is empty!");
            }
            else
            {
                Console.Write("Enter the number of the program: ");
                int.TryParse(Console.ReadLine(), out int numberOfProgram);
                if (numberOfProgram > 0 && numberOfProgram <= countOfProgramsInArray)
                {
                    Console.WriteLine("Enter a new value or press 'Enter' to not change value");
                    Console.Write("Name: {0} | New value: ", programArray[numberOfProgram - 1].name);
                    string newName = Console.ReadLine();
                    if (newName != "") // Checks if new name is not empty
                    {
                        programArray[numberOfProgram - 1].name = newName;
                    }
                    Console.Write("Category: {0} | New value: ", programArray[numberOfProgram - 1].category);
                    string newCategory = Console.ReadLine();
                    if (newCategory != "") // Checks if new category is not empty
                    {
                        programArray[numberOfProgram - 1].category = newCategory;
                    }
                    Console.Write("Description: {0} | New value: ", programArray[numberOfProgram - 1].description);
                    string newDescription = Console.ReadLine();
                    if (newDescription != "") // Checks if new description is not empty
                    {
                        if (newDescription.Length > 100) // Checks if new description is over 100 char
                        {
                            newDescription = newDescription.Substring(0, 100); // Substring description to 100 char
                        }
                        programArray[numberOfProgram - 1].description = newDescription;
                    }
                    Console.Write("Version number: {0} | New value: ", programArray[numberOfProgram - 1].version.versionNumber);
                    string newVersionNumber = Console.ReadLine();
                    if (newVersionNumber != "") // Checks if new date is not empty
                    {
                        programArray[numberOfProgram - 1].version.versionNumber = newVersionNumber;
                    }
                    Console.Write("Launch month: {0} | New value: ", programArray[numberOfProgram - 1].version.launchMonth);
                    string newMonth = Console.ReadLine();
                    if (newMonth != "") // Checks if new month is not empty
                    {
                        programArray[numberOfProgram - 1].version.launchMonth = Convert.ToByte(newMonth);
                    }
                    Console.Write("Launch year: {0} | New value: ", programArray[numberOfProgram - 1].version.launchYear);
                    string newYear = Console.ReadLine();
                    if (newYear != "") // Checks if new year is not empty
                    {
                        programArray[numberOfProgram - 1].version.launchYear = Convert.ToUInt16(newYear);
                    }
                    Console.WriteLine();
                    Console.WriteLine("Program updated.");
                }
                else // Given number is out of bound
                {
                    Console.WriteLine("Number out of bound!");
                }
            }
        }

        // Function to delete program from database
        private void DeleteProgram()
        {
            Console.WriteLine();
            if (countOfProgramsInArray == 0) // Checks if database is empty
            {
                Console.WriteLine("Database is empty!");
            }
            else
            {
                Console.Write("Enter a position number: ");
                int.TryParse(Console.ReadLine(), out int numberToRemove);
                for (int i = 0; i < countOfProgramsInArray; i++) // For-loop for going through the array
                {
                    if (i >= numberToRemove) // If number is bigger than number we want to remove
                    {
                        programArray[i - 1] = programArray[i]; // Moves programs in the array one place backwards
                    }
                }
                countOfProgramsInArray -= 1; // Removes one from lenght of the array
                Console.WriteLine("Program deleted.");
            }
        }

        // Function to sort database (array) alphabetically by name with bubble sort
        private void SortDatabase()
        {
            for (int i = (countOfProgramsInArray - 1); i >= 0; i--) // Loop to go through array backwards
            {
                for (int j = 1; j <= i; j++)
                {
                    if (String.Compare(programArray[j - 1].name, programArray[j].name) > 0) // If previous name in array is bigger, changes places of programs in the array
                    {
                        Program tempNumber = programArray[j - 1];
                        programArray[j - 1] = programArray[j];
                        programArray[j] = tempNumber;
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("Database sorted.");
        }

        // Function to fix redundant spaces from names
        private void FixSpaces()
        {
            for (int i = 0; i < countOfProgramsInArray; i++) // For-loop for going through the array
            {
                while (programArray[i].name.Contains("  ")) // Checks if name contains two spaces in a row and runs that in a while loop so long that the are not anymore more than one space in a row
                {
                    programArray[i].name = programArray[i].name.Replace("  ", " "); // Replaces two spaces with one space
                }
            }
            Console.WriteLine();
            Console.WriteLine("Redundant spaces fixed.");
        }


        // Function to make premade array for easier testing
        private void PremadeArray()
        {
            Program program1 = new Program
            {
                name = "Google  Chrome",
                category = "Browser",
                description = "Google's internet browser."
            };
            program1.version.versionNumber = "1.1";
            program1.version.launchMonth = 12;
            program1.version.launchYear = 2005;
            programArray[0] = program1;

            Program program2 = new Program
            {
                name = "Google    Docs",
                category = "Office",
                description = "Google's Word"
            };
            program2.version.versionNumber = "1.1";
            program2.version.launchMonth = 12;
            program2.version.launchYear = 2005;
            programArray[1] = program2;

            Program program3 = new Program
            {
                name = "Microsoft   Words",
                category = "Office",
                description = "Word processor"
            };
            program3.version.versionNumber = "1.1";
            program3.version.launchMonth = 12;
            program3.version.launchYear = 2005;
            programArray[2] = program3;

            Program program4 = new Program
            {
                name = "Microsoft   Excel",
                category = "Office",
                description = "Spreadsheet"
            };
            program4.version.versionNumber = "1.1";
            program4.version.launchMonth = 12;
            program4.version.launchYear = 2005;
            programArray[3] = program4;

            Program program5 = new Program
            {
                name = "Matlab",
                category = "Calc",
                description = ""
            };
            program5.version.versionNumber = "1.1";
            program5.version.launchMonth = 12;
            program5.version.launchYear = 2005;
            programArray[4] = program5;

            Program program6 = new Program
            {
                name = "Visual  Studio",
                category = "IDE",
                description = "IDE"
            };
            program6.version.versionNumber = "1.1";
            program6.version.launchMonth = 12;
            program6.version.launchYear = 2005;
            programArray[5] = program6;

            Program program7 = new Program
            {
                name = "Notepad++",
                category = "Text editor",
                description = "Edits text"
            };
            program7.version.versionNumber = "1.1";
            program7.version.launchMonth = 12;
            program7.version.launchYear = 2005;
            programArray[6] = program7;

            Program program8 = new Program
            {
                name = "Visual Studio Code",
                category = "Text editor",
                description = "Edits text"
            };
            program8.version.versionNumber = "1.1";
            program8.version.launchMonth = 12;
            program8.version.launchYear = 2005;
            programArray[7] = program8;

            Program program9 = new Program
            {
                name = "Slide",
                category = "",
                description = ""
            };
            program9.version.versionNumber = "1.1";
            program9.version.launchMonth = 12;
            program9.version.launchYear = 2005;
            programArray[8] = program9;

            Program program10 = new Program
            {
                name = "Steam",
                category = "",
                description = ""
            };
            program10.version.versionNumber = "1.1";
            program10.version.launchMonth = 12;
            program10.version.launchYear = 2005;
            programArray[9] = program10;

            Program program11 = new Program
            {
                name = "Origin",
                category = "",
                description = ""
            };
            program11.version.versionNumber = "1.1";
            program11.version.launchMonth = 12;
            program11.version.launchYear = 2005;
            programArray[10] = program11;

            Program program12 = new Program
            {
                name = "Uplay",
                category = "",
                description = ""
            };
            program12.version.versionNumber = "1.1";
            program12.version.launchMonth = 12;
            program12.version.launchYear = 2005;
            programArray[11] = program12;

            Program program13 = new Program
            {
                name = "Git",
                category = "",
                description = ""
            };
            program13.version.versionNumber = "1.1";
            program13.version.launchMonth = 12;
            program13.version.launchYear = 2005;
            programArray[12] = program13;

            Program program14 = new Program
            {
                name = "Gimp",
                category = "",
                description = ""
            };
            program14.version.versionNumber = "1.1";
            program14.version.launchMonth = 12;
            program14.version.launchYear = 2005;
            programArray[13] = program14;

            Program program15 = new Program
            {
                name = "Photoshop",
                category = "",
                description = ""
            };
            program15.version.versionNumber = "1.1";
            program15.version.launchMonth = 12;
            program15.version.launchYear = 2005;
            programArray[14] = program15;

            Program program16 = new Program
            {
                name = "Krita",
                category = "",
                description = ""
            };
            program16.version.versionNumber = "1.1";
            program16.version.launchMonth = 12;
            program16.version.launchYear = 2005;
            programArray[15] = program16;

            Program program17 = new Program
            {
                name = "Netflix",
                category = "",
                description = ""
            };
            program17.version.versionNumber = "1.1";
            program17.version.launchMonth = 12;
            program17.version.launchYear = 2005;
            programArray[16] = program17;

            Program program18 = new Program
            {
                name = "Powerpoint",
                category = "",
                description = ""
            };
            program18.version.versionNumber = "1.1";
            program18.version.launchMonth = 12;
            program18.version.launchYear = 2005;
            programArray[17] = program18;

            Program program19 = new Program
            {
                name = "Firefox",
                category = "",
                description = ""
            };
            program19.version.versionNumber = "1.1";
            program19.version.launchMonth = 12;
            program19.version.launchYear = 2005;
            programArray[18] = program19;

            Program program20 = new Program
            {
                name = "Outlook",
                category = "",
                description = ""
            };
            program20.version.versionNumber = "1.1";
            program20.version.launchMonth = 12;
            program20.version.launchYear = 2005;
            programArray[19] = program20;

            Program program21 = new Program
            {
                name = "VMware",
                category = "",
                description = ""
            };
            program21.version.versionNumber = "1.1";
            program21.version.launchMonth = 12;
            program21.version.launchYear = 2005;
            programArray[20] = program21;

            countOfProgramsInArray = 21;

        }

    }
} 