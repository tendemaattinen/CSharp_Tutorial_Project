﻿using System;

namespace TutorialApplication
{
    class VowelSwitch
    {
        // Checks if user gives a lowercase vowel, digit or something else with switch statement
        public void Main()
        {
            while (true)
            { 
                string input;
                Console.WriteLine("");
                Console.Write("Enter letter or number (0 to stop): ");
                input = Console.ReadLine();
                Console.WriteLine("");
                // Checks if input is zero, and ends application if it is
                if (input == "0")
                {
                    break;
                }
                switch (input)
                {
                    case "a":
                        Print(1);
                        break;
                    case "e":
                        Print(1);
                        break;
                    case "i":
                        Print(1);
                        break;
                    case "o":
                        Print(1);
                        break;
                    case "u":
                        Print(1);
                        break;
                    case "1":
                        Print(2);
                        break;
                    case "2":
                        Print(2);
                        break;
                    case "3":
                        Print(2);
                        break;
                    case "4":
                        Print(2);
                        break;
                    case "5":
                        Print(2);
                        break;
                    case "6":
                        Print(2);
                        break;
                    case "7":
                        Print(2);
                        break;
                    case "8":
                        Print(2);
                        break;
                    case "9":
                        Print(2);
                        break;
                    default:
                        Print(3);
                        break;
                }
            }   
        }

        // Function for printing sentence
        private void Print(int number)
        {
            if (number == 1)
            {
                Console.WriteLine("It's a lowercase vowel.");
            }
            if (number == 2)
            {
                Console.WriteLine("It's a digit.");
            } 
            if (number == 3)
            {
                Console.WriteLine("It's another symbol.");
            }
        }
    }
}