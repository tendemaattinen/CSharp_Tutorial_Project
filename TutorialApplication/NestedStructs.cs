﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application creates two persons from user input and prints them.
 *              Application is using nested structs to save persons data.
 */
 
using System;

namespace TutorialApplication
{
    class NestedStructs
    {
        // Struct to save persons data
        struct Person
        {
            public string name;
            public DateOfBirth dateOfBirth; // Nested struct to save date of birth
        }

        // Struct to save date of birth
        struct DateOfBirth
        {
            public int date;
            public int month;
            public int year;
        }

        public void Main()
        {
            Console.WriteLine();
            Person[] personsArray = new Person[2]; // Creates new array for persons
            int countOfPersonsInArray = 0; // Count of persons in created array
            (personsArray, countOfPersonsInArray) = CreateNewPerson(personsArray, countOfPersonsInArray); // Creates a new person
            (personsArray, countOfPersonsInArray) = CreateNewPerson(personsArray, countOfPersonsInArray); // Creates a new person
            for (int i = 0; i < countOfPersonsInArray; i++) // Loop for printing all persons in the array
            {
                Console.WriteLine("Name: {0}, Date of Birth: {1}.{2}.{3}", personsArray[i].name, personsArray[i].dateOfBirth.date,
                                  personsArray[i].dateOfBirth.month, personsArray[i].dateOfBirth.year);
            }
        }

        // Function to create a new person, returns Person[] - array and lenght of the array
        private (Person[], int) CreateNewPerson(Person[] array, int lenght)
        {
            Person person = new Person(); // Creates a new empty person
            Console.Write("Enter a name: ");
            person.name = Console.ReadLine();
            Console.Write("Enter a day: ");
            int.TryParse(Console.ReadLine(), out person.dateOfBirth.date);
            Console.Write("Enter a month: ");
            int.TryParse(Console.ReadLine(), out person.dateOfBirth.month);
            Console.Write("Enter a year: ");
            int.TryParse(Console.ReadLine(), out person.dateOfBirth.year);
            array[lenght] = person; // Adds person to array
            lenght += 1; // Adds one to lenght of the array
            Console.WriteLine();
            return (array, lenght);
        }
    }
} 