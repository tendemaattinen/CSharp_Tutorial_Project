﻿using System;

namespace TutorialApplication
{
    class MultiplicationTable
    {
        // User gives number and application gives multiplication table for that number
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Give number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            Console.WriteLine("{0} * 1 = " + (number * 1), number);
            Console.WriteLine("{0} * 2 = " + (number * 2), number);
            Console.WriteLine("{0} * 3 = " + (number * 3), number);
            Console.WriteLine("{0} * 4 = " + (number * 4), number);
            Console.WriteLine("{0} * 5 = " + (number * 5), number);
            Console.WriteLine("{0} * 6 = " + (number * 6), number);
            Console.WriteLine("{0} * 7 = " + (number * 7), number);
            Console.WriteLine("{0} * 8 = " + (number * 8), number);
            Console.WriteLine("{0} * 9 = " + (number * 9), number);
            Console.WriteLine("{0} * 10 = " + (number * 10), number);
        }
    }
}