﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application creates 'struct' to store data of 2D points, saves points to array and prints them.
 */

using System;

namespace TutorialApplication
{
    class ArrayOfStruct
    {
        // Struct-structure for storing data
        struct Point
        {
            public short xCoordinate;
            public short yCoordinate;
            public byte r;
            public byte g;
            public byte b;
        }

        public void Main()
        {
            Point[] arrayOfPoints = new Point[1000]; // Array to store 'Point'-structs
            int lenghtOfArray = 0; // Count of items in the array
            Point point1, point2;
            Console.WriteLine();
            Console.Write("Enter x for first point: ");
            point1.xCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter y for first point: ");
            point1.yCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter red for first point: ");
            point1.r = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter green for first point: ");
            point1.g = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter blue for first point: ");
            point1.b = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter x for second point: ");
            arrayOfPoints[0] = point1; // Saves point 1 to array
            lenghtOfArray += 1; //Adds one to leght of array
            point2.xCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter y for second point: ");
            point2.yCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter red for second point: ");
            point2.r = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter green for second point: ");
            point2.g = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter blue for second point: ");
            point2.b = Convert.ToByte(Console.ReadLine());
            Console.WriteLine();
            arrayOfPoints[1] = point2; // Saves point 2 to array
            lenghtOfArray += 1; // Adds one to leght of array
            DisplayPoint(arrayOfPoints, lenghtOfArray);
        }

        // Function to print data about point
        private void DisplayPoint(Point[] array, int lenght)
        {
            for (int i = 0; i < lenght; i++)
            {
                Console.WriteLine("P{0} is located in ({1},{2}), colour ({3},{4},{5})", i + 1, array[i].xCoordinate,
                array[i].yCoordinate, array[i].r, array[i].g, array[i].b);
            }
        }
    }
} 