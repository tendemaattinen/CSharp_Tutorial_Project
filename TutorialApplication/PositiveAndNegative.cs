﻿using System;

namespace TutorialApplication
{
    class PositiveAndNegative
    {
        // Ask number from user and checks if it is a positive or negative
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Give a number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            if (number >= 0)
            {
                Console.WriteLine("{0} is a positive number", number);
            } else
            {
                Console.WriteLine("{0} is a negative number", number);
            }

        }
    }
}