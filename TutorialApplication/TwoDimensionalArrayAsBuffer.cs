﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Creates two-dimensional array, which includes 80 random X-characters and prints it.
 */
 
using System;

namespace TutorialApplication
{
    class TwoDimensionalArrayAsBuffer
    {
        public void Main()
        {
            Console.WriteLine();
            char[,] array = new char[20, 70]; // New two-dimensional array for characters
            Random randomGenerator = new Random(); // New random generator
            for (int i = 0; i < 80; i++) // Adds random X-characters to array 80 times
            {
                array[randomGenerator.Next(0, 20), randomGenerator.Next(0, 70)] = 'X';
            }
            for (int j = 0; j < 20; j++) // Loop for printing two-dimensional array
            {
                for (int k = 0; k < 70; k++)
                {
                    Console.Write(array[j,k]);
                }
                Console.WriteLine();
            }
        }
    }
} 