﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software writes user input to file named "sentences.txt" which is located in My Documents -folder. This version
 *              can also write to exist version of the file.
 */

using System;
using System.IO;

namespace TutorialApplication
{
    class AppEndToTextFile
    {
        public void Main()
        {
            // Try
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // Saves path of My Documents -folder
                string text;
                string[] array = new string[50]; // New array for saving strings
                int i = 0; // Counter
                path = path + "\\sentences.txt";
                Console.WriteLine(path);
                Console.WriteLine();
                // Asks strings as long as user writes something
                do
                {
                    Console.Write("Enter text or leave empty to quit: ");
                    text = Console.ReadLine();
                    array[i] = text; // Saves text to array
                    i += 1; // Adds one to counter
                } while (text != "");
                // Checks if file exists
                if (!File.Exists(path))
                {
                    // Writes all strings for array to text file using StreamWriter
                    using (StreamWriter sW = new StreamWriter(path))
                    {
                        int j = 0; // Counter
                        foreach (string line in array)
                        {
                            
                            if (j < i)
                            {
                                sW.WriteLine(line);
                            }
                            j++;
                        }
                    }
                }

                else
                {
                    // Writes all strings for array to text file using StreamWriter
                    using (StreamWriter sW = File.AppendText(path))
                    {
                        int j = 0; // Counter
                        foreach (string line in array)
                        {
                            if (j < i)
                            {
                                sW.WriteLine(line);
                            }
                            j++;
                        }
                    }
                }
            }

            // Catch error
            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
        }
    }
} 