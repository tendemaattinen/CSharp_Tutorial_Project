﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application can save data (numbers) to array, show saved data from array, find data from array
 *              and show statistics from numbers of array (amount of data, sum, average, minimum and maximum).
 */

using System;

namespace TutorialApplication
{
    class Statistics2
    {
        int[] dataArray = new int[1000]; // Array to save data (lenght 1000)
        int countOfData = 0; // Count of data in array

        // Main-function
        public void Main()
        {
            bool loop = true; // Boolean value to run application in loop
            Console.WriteLine("*** Statistical program ***");
            while (loop) // Loop for application
            {
                Console.WriteLine();
                Console.WriteLine("*** Main menu ***");
                MainMenu(); // Function to print main menu
                Console.Write("Your choice: ");
                Int32.TryParse(Console.ReadLine(), out int choice); // Users choice of action
                loop = ChoiceForFunction(choice); // Opens function which selects right action, return boolean value for loop
            }
        }

        // Function that prints main menu
        private void MainMenu()
        {
            Console.WriteLine("1) Add data");
            Console.WriteLine("2) See data");
            Console.WriteLine("3) Find data");
            Console.WriteLine("4) Statistics");
            Console.WriteLine("0) Exit");
        }

        // Function which selects right action, returns false if user wants to quit, otherwise returns true
        private bool ChoiceForFunction(int number)
        {
            bool bol = true; // Boolean value which will be returned
            switch (number)
            {
                case 1: // User wants to add data to array
                    AddToData();
                    break;
                case 2: // User wants to see data in array
                    SeeData();
                    break;
                case 3: // User wants to find specific data from array
                    FindData();
                    break;
                case 4: // User wants to see statistic of array
                    SeeStatistics();
                    break;
                case 0: // User wants to quit
                    bol = false;
                    break;
                default: // User gives invalid input
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return bol;
        }

        // Function to add data to array
        private void AddToData()
        {
            Console.Write("Enter a number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            dataArray[countOfData] = number; // Saves given number to next available slot in array
            countOfData += 1; // Adds one to count of data in array
        }

        // Function to show all data in the array
        private void SeeData()
        {
            for (int i = 0; i < countOfData; i++) // Loop for going through the array
            {
                Console.Write("{0} ", dataArray[i]);
            }
        }

        // Function to find specific data from array
        private void FindData()
        {
            int amount = 0; // Integral value of amount of found numbers
            Console.Write("Enter a searchable number: ");
            Int32.TryParse(Console.ReadLine(), out int searchableNumber);
            for (int i = 0; i < countOfData; i++) // Loop for going through the array
            {
                if (dataArray[i] == searchableNumber) // Checks if value in array match to searchable value
                {
                    amount += 1; // Adds one to amount
                }
            }
            if (amount != 0) // Searchable number is found if amount is bigger than zero
            {
                Console.WriteLine("Number {0} was found {1} times.", searchableNumber, amount);
            }
            else // Searchable number is not found from array
            {
                Console.WriteLine("Number {0} was not found from array.", searchableNumber);
            }
            
        }

        // Fiunction to show statistics from array
        private void SeeStatistics()
        {
            if (countOfData == 0) // Checks if array is empty
            {
                Console.WriteLine("There is not data in array!");
            }
            else
            {
                int sumOfData = SumOFDataInArray(); // Sum of data in array
                int averageOfData = sumOfData / countOfData; // Average of data in array
                (int minimum, int maximum) = MinimumAndMaximumNuberInArray(); // Minimum and maximum values of array
                Console.WriteLine();
                Console.WriteLine("Total data: {0}", countOfData);
                Console.WriteLine("Sum: {0}", sumOfData);
                Console.WriteLine("Average: {0}", averageOfData);
                Console.WriteLine("Minimum: {0}", minimum);
                Console.WriteLine("Maximum: {0}", maximum);
            }
        }

        // Function to calculate sum of numbers in array, returns integral value of sum
        private int SumOFDataInArray()
        {
            int sum = 0; // Sum of the numbers in array
            for (int i = 0; i < countOfData; i++) // Loop for going through the array
            {
                sum += dataArray[i]; // Adds value from array to sum
            }
            return sum;
        }

        // Function to search minimum and maximum values from array, returns integral values of minimum and maximum
        private (int, int) MinimumAndMaximumNuberInArray()
        {
            int minimum = dataArray[0]; // Adds first number of array as minimum value
            int maximum = dataArray[0]; // Adds first number of array as maximum value
            for (int i = 0; i < countOfData; i++) // Loop for going through the array
            {
                if (dataArray[i] < minimum) // Checks if value in array is smaller than minimum number
                {
                    minimum = dataArray[i];
                }
                else if (dataArray[i] > maximum) // Checks if value in array is bigger than maximum number
                {
                    maximum = dataArray[i];
                }
            }
            return (minimum, maximum);
        }
    }
}