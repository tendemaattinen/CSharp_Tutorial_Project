﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software takes complex number at start from user. After that user can choose to print it, get magnitude of it
 *              or add another complex number to it.
 */
 
using System;

namespace TutorialApplication
{
    // Class for complex number
    class ComplexNumber
    {
        public int RealNumber { get; set; }
        public int ImaginaryNumber { get; set; }

        // Constructor with inputs
        public ComplexNumber(int real, int imag)
        {
            RealNumber = real;
            ImaginaryNumber = imag;
        }

        // Method to return complex number as string
        public string ToStringFormat()
        {
            string text = "(" + RealNumber + "," + ImaginaryNumber + ")";
            return text;
        }

        // Method to return mangnitude of complex number as double 
        public double GetMagnitude()
        {
            double magnitude = Math.Sqrt((RealNumber ^ 2) + (ImaginaryNumber ^ 2));
            return magnitude;
        }

        // Method to add complex number to another complex number
        public void Add(int realToAdd, int imagToAdd)
        {
            RealNumber = RealNumber + realToAdd;
            ImaginaryNumber = ImaginaryNumber + imagToAdd;
        }
    }

    // Class for testing, includes Main-function
    class ComplexTest
    {
        public void Main()
        {
            bool swi = true; // Boolean value for looping menu
            Console.WriteLine();
            Console.WriteLine("*** Complex numbers ***");
            Console.WriteLine();
            // Real number
            Console.Write("Enter real number: ");
            int.TryParse(Console.ReadLine(), out int realNumber);
            // Imaginary number
            Console.Write("Enter imaginary number: ");
            int.TryParse(Console.ReadLine(), out int imaginaryNumber);
            ComplexNumber complexNumber = new ComplexNumber(realNumber, imaginaryNumber); // Creates new complex number
            while (swi)
            {
                Console.WriteLine();
                Console.WriteLine("1) Print complex number");
                Console.WriteLine("2) Get magnitude of complex number");
                Console.WriteLine("3) Add complex number to other complex number");
                Console.WriteLine("0) Quit");
                Console.Write("Enter: ");
                string userInput = Console.ReadLine();
                Console.WriteLine();
                switch (userInput)
                {
                    case "1": // User wants to print complex number
                        string text = complexNumber.ToStringFormat();
                        Console.WriteLine("{0}", text);
                        break;
                    case "2": // User wants magnitude of complex number
                        double magnitude = complexNumber.GetMagnitude();
                        Console.WriteLine("Magnitude: {0}", magnitude);
                        break;
                    case "3": // User wants to add another complex number to complex number
                        AddComplexNumber(complexNumber);
                        break;
                    case "0": // User wants to quit
                        swi = false;
                        break;
                    default: // User gives invalid input
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        // Function to add complex number to another complex number
        private void AddComplexNumber(ComplexNumber complex)
        {
            Console.Write("Enter real number: ");
            int.TryParse(Console.ReadLine(), out int real);
            Console.Write("Enter imaginary number: ");
            int.TryParse(Console.ReadLine(), out int imag);
            complex.Add(real, imag);
            Console.WriteLine("Numbers added.");
        }
    }
} 