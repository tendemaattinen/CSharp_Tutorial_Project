﻿using System;

namespace TutorialApplication
{
    class ReverseArray
    {
        // Ask five numbers from user, makes array from them and then reverse it in for-loop
        public void Main()
        {
            int[] numberArray = new int[5];  // Array for asked numbers
            Console.WriteLine();
            Console.Write("Enter the first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Enter the second number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            Console.Write("Enter the third number: ");
            Int32.TryParse(Console.ReadLine(), out int number3);
            Console.Write("Enter the fourth number: ");
            Int32.TryParse(Console.ReadLine(), out int number4);
            Console.Write("Enter the fifth number: ");
            Int32.TryParse(Console.ReadLine(), out int number5);
            numberArray = makeArray(number1, number2, number3, number4, number5); // Makes array 
            reverseArray(numberArray); // Prints array in reversed order
            Console.WriteLine();
        }

        // Function to make array from given numbers, return int array
        private int[] makeArray(int num1, int num2, int num3, int num4, int num5)
        {
            int[] array = { num1, num2, num3, num4, num5 };
            return array;
        }

        // Function to print array in reversed order with for-loop
        private void reverseArray(int[] arr)
        {
            for (int j = 4; j >= 0; j--)
            {
                Console.Write("{0} ", arr[j]);
            }
        }
    }
}