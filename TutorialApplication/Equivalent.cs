﻿using System;

namespace TutorialApplication
{
    class Equivalent
    {
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Give first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Give second number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            Console.Write("Give third number: ");
            Int32.TryParse(Console.ReadLine(), out int number3);
            int result1 = (number1 + number2) * number3;
            int result2 = (number1 * number3) + (number2 * number3); ;
            Console.WriteLine("(a + b) * c = {0}", result1);
            Console.WriteLine("(a * c) + (b * c) = {0}", result2);
        }
    }
}