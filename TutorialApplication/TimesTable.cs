﻿using System;

namespace TutorialApplication
{
    class TimesTable
    {
        // Asks number from user and gives times table for that number
        public void Main()
        {
            int i = 0, j;
            Console.WriteLine("");
            Console.Write("Enter number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            while (i < 10)
            {
                i++;
                j = number * i;
                Console.WriteLine("{0} * {1} = {2}", number, i, j);
            }
        }
    }
}