﻿using System;

namespace TutorialApplication
{
    // Prints number from 1 to 500 if it is multiple of 3 and 5
    class Multiples
    {
        public void Main()
        {
            Console.WriteLine("");
            for (int i = 1; i <= 500; i++)
            {
                if ((i % 3 == 0) && (i % 5 == 0))
                {
                    Console.Write("{0} ", i);
                }
            }
        }
    }
}