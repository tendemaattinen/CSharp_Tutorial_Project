﻿using System;

namespace TutorialApplication
{
    class PerimeterAreaDiagonal
    {
        // Asks width and height from user, and calculate perimeter, area and diagonal from given numbers
        public void Main()
        {
            while (true)
            {
                double area, perimeter, width, height, diagonal;
                Console.WriteLine("");
                Console.Write("Enter the width (0 to end): ");
                Double.TryParse(Console.ReadLine(), out width);
                // Ends application if user gives 0 as width
                if (width == 0)
                {
                    break;
                } else
                {
                    Console.Write("Enter the height: ");
                    Double.TryParse(Console.ReadLine(), out height);
                    Console.WriteLine("");
                    perimeter = ((width * 2) + (height * 2));
                    area = (width * height);
                    diagonal = Math.Sqrt((width * width) + (height * height));
                    Console.WriteLine("Perimeter: {0}", perimeter);
                    Console.WriteLine("Area: {0}", area);
                    Console.WriteLine("Diagonal: {0}", diagonal);
                }
            }
        }
    }
}