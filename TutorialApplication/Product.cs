﻿using System;

namespace TutorialApplication
{
    class Product
    {
        // Asks to integers from user and multiplicates them without using *
        public void Main()
        {
            while (true)
            {
                Console.WriteLine();
                Console.Write("Enter the first number or 0 to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number1);
                // Checks if user wants to quit
                if (number1 == 0)
                {
                    break;
                }
                Console.Write("Enter the second number: ");
                Int32.TryParse(Console.ReadLine(), out int number2);
                int answer = Multiplication(number1, number2);
                Console.WriteLine("{0} X {1} = {2}", number1, number2, answer);
            }
        }

        // Function for multiplication, multiplication happens by adding first number to 0 number 2 times in if-statement
        private int Multiplication(int num1, int num2)
        {
            int ans = 0;
            for (int i = 0; i < num2; i++)
            {
                ans += num1;
            }
            return ans;
        }
    }
}