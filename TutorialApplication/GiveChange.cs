﻿using System;

namespace TutorialApplication
{
    class GiveChange
    {
        // Asks from user a price of product and how much customer paid, and gives what user have to give to customer as change
        public void Main()
        {
            while(true)
            {
                Console.WriteLine();
                Console.Write("Enter a price: ");
                Int32.TryParse(Console.ReadLine(), out int price);
                Console.Write("Enter a paid: ");
                Int32.TryParse(Console.ReadLine(), out int paid);
                int change = paid - price;
                Change(change);
                Console.WriteLine();

                // Checking if user wants to continue or quit
                Console.Write("Enter '1' to continue or '0' to quit: ");
                Int32.TryParse(Console.ReadLine(), out int checkNumber);
                if (checkNumber != 0)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
        }

        // Function to print what to give as change
        private void Change(int change)
        {
            while (change >= 100)
            {
                Console.Write("100 ");
                change -= 100;
            }
            while (change >= 50)
            {
                Console.Write("50 ");
                change -= 50;
            }
            while (change >= 20)
            {
                Console.Write("20 ");
                change -= 20;
            }
            while (change >= 10)
            {
                Console.Write("10 ");
                change -= 10;
            }
            while (change >= 5)
            {
                Console.Write("5 ");
                change -= 5;
            }
            while (change >= 2)
            {
                Console.Write("2 ");
                change -= 2;
            }
            while (change >= 1)
            {
                Console.Write("1 ");
                change -= 1;
            }
        }
    }
}