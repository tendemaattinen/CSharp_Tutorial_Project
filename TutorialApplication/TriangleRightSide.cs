﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application prints given string as string which starts from right side.
 */
 
using System;

namespace TutorialApplication
{
    class TriangleRightSide
    {
        public void Main()
        {
            Console.WriteLine();
            Console.Write("Enter a string: ");
            string userInput = Console.ReadLine();
            int maxRows = userInput.Length; // Maximum number of rows
            int placement = userInput.Length - 1; // Placement from where to start
            int lenghtOfPrint = 1; // Lenght of printable part
            Console.WriteLine();
            for (int i = 0; i < maxRows; i++) // Loop for rows
            {
                for (int j = 0; j < userInput.Length; j++) // Loop for columns
                {
                    if (j == placement) // Checks if placement match to column
                    {
                        Console.Write(userInput.Substring(placement, lenghtOfPrint)); // Prints printable part
                    }
                    else // Placement won't match to column
                    {
                        Console.Write(" "); // Prints whitespace
                    }
                }
                placement -= 1; // Removes one from placement
                lenghtOfPrint += 1; // Adds one to lenght
                Console.WriteLine();
            }
        }
    }
} 