﻿using System;

namespace TutorialApplication
{
    class CalculateValuesFunction
    {
        // Calculates values for function y = x2 + 2x + 1 from -10 to 10
        public void Main()
        {
            Console.WriteLine("");
            // Loop for calculation
            for (int i = - 10; i <= 10; i++)
            {
                Console.Write("{0} ", (Calc(i)));
            }
        }

        // Function to calculate function
        private int Calc(int x)
        {
            int y = ((x * x) - (2 * x) + 1);
            return y;
        }
    }
}