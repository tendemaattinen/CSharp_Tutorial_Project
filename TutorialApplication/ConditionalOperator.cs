﻿using System;

namespace TutorialApplication
{
    class ConditionalOperator
    {
        // Asks two number from user and gives amount of positive numbers two ways, with if-statements and with conditional operator
        public void Main()
        {
            while (true) {
                int amount;
                Console.WriteLine();
                Console.Write("Enter first number or '0' to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number1);
                // Checks if user wants to quit application by giving '0'
                if (number1 == 0)
                {
                    break;
                }
                Console.Write("Enter senond number: ");
                Int32.TryParse(Console.ReadLine(), out int number2);

                // Checking amount of positive numbers with if-statement
                if (number1 > 0 && number2 > 0)
                {
                    amount = 2;
                }
                else
                {
                    if (number1 > 0 || number2 > 0)
                    {
                        amount = 1;
                    }
                    else
                    {
                        amount = 0;
                    }
                }
                Console.WriteLine("{0}", amount);

                // Checking amount of positive numbers with conditional operator
                amount = number1 > 0 && number2 > 0 ? 2 : number1 > 0 || number2 > 0 ? 1 : 0;
                Console.WriteLine("{0}", amount);
            }

        }
    }
}