﻿using System;

namespace TutorialApplication
{
    class SwitchMark
    {
        // User gives a numerical mark and application tells which kind oof mark it is (good or bad)
        public void Main()
        {
            while (true)
            {
                Console.WriteLine();
                Console.Write("Enter a numerical mark or '-1' to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number);
                // Checks if user wants to quit application by giving '-1'
                if (number == -1)
                {
                    break;
                }
                else
                {
                    // Switch-statement to check mark
                    switch (number)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            Console.WriteLine("Fail");
                            break;
                        case 5:
                            Console.WriteLine("Pass");
                            break;
                        case 6:
                            Console.WriteLine("Good");
                            break;
                        case 7:
                        case 8:
                            Console.WriteLine("Notable");
                            break;
                        case 9:
                        case 10:
                            Console.WriteLine("Excellent");
                            break;
                        default:
                            Console.WriteLine("Invalid input!");
                            break;
                    }
                }
            }
        }
    }
}