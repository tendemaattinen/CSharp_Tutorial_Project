﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Creates random array of tables.
 */
 
using System;

namespace TutorialApplication
{
    class Table1
    {
        protected float Width { get; set; }
        protected float Height { get; set; }

        // Constructor
        public Table1()
        {

        }

        // Constructor with iunputs
        public Table1(float wid, float hei)
        {
            Width = wid;
            Height = hei;
        }

        // Method to show width and height of table
        public virtual void ShowData()
        {
            Console.WriteLine("Width: {0}, Height: {1}", Width, Height);
        }

    }

    class CoffeeTable : Table1
    {
        // Constructor with input
        public CoffeeTable(float wid, float hei)
        {
            Width = wid;
            Height = hei;
        }

        // Method to show width and height of coffee table
        public override void ShowData()
        {
            Console.WriteLine("Coffee table) Width: {0}, Height: {0}", Width, Height);
        }
    }

    // Testing class with Main-function
    class TestTables
    {
        public void Main()
        {
            Console.WriteLine();
            Table1[] tableArray = new Table1[10]; // Array to store tables
            Random rand = new Random(); // Random
            // Loop for creating tables
            for (int i = 0; i < 5; i++)
            {
                int width = rand.Next(50, 200); // Random width
                int height = rand.Next(50, 200); // Random height
                Table1 table = new Table1(width, height); // Creates new table with created random numbers
                tableArray[i] = table; // Adds created table to array
            }
            // Loop for creating coffee tables
            for (int i = 5; i < 10; i++)
            {
                int width = rand.Next(50, 200); // Random width
                int height = rand.Next(50, 200); // Random height
                CoffeeTable table = new CoffeeTable(width, height); // Creates new table with created random numbers
                tableArray[i] = table; // Adds created table to array
            }
            // Loop for printing all the tables from array
            for (int i = 0; i < 10; i++)
            {
                Console.Write("{0}) ", i + 1);
                tableArray[i].ShowData();
            }
        }
    }
} 