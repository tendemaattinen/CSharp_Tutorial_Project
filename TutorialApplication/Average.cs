﻿using System;

namespace TutorialApplication
{
    class Average
    {
        public void Main()
        {
            int sum = 0;
            float average;
            Console.WriteLine("");
            Console.Write("Number of numbers: ");
            if(Int32.TryParse(Console.ReadLine(),out int numberOfNumbers))
            {
                for(int i = 0; i < numberOfNumbers; i++)
                {
                    int o = i + 1;
                    Console.Write("{0}. number: ", o);
                    Int32.TryParse(Console.ReadLine(), out int num);
                    sum = sum + num;
                }
                average = ((float)sum / (float)numberOfNumbers);
                Console.WriteLine("Average: {0}", average);
            } else
            {
                Console.Write("Invalid number");
            }

        }
    }
}