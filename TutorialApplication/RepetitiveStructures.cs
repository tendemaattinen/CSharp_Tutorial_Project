﻿using System;

namespace TutorialApplication
{
    class RepetitiveStructures
    {
        // Repeats numbers from number 1 to number 2 three different ways
        public void Main()
        {
            while (true)
            {
                Console.WriteLine();
                Console.Write("Enter the first number or 0 to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number1);
                // Checks if zero is given to quit application
                if (number1 == 0)
                {
                    break;
                }
                Console.Write("Enter the second number: ");
                Int32.TryParse(Console.ReadLine(), out int number2);
                Console.WriteLine();
                ForLoop(number1, number2);
                Console.WriteLine();
                WhileLoop(number1, number2);
                Console.WriteLine();
                DoWhileLoop(number1, number2);
                Console.WriteLine();
            }
        }

        // Function for For-loop to repeat numbers
        private void ForLoop(int num1, int num2)
        {
            for (int i = num1; i <= num2; i++)
            {
                Console.Write("{0} ", i);
            }
        }

        // Function for While-loop to repeat numbers
        private void WhileLoop(int num1, int num2)
        {
            while(num1 <= num2)
            {
                Console.Write("{0} ", num1);
                num1 += 1;
            }
        }

        // Function for Do While -loop to repeat numbers
        private void DoWhileLoop(int num1, int num2)
        {
            do
            {
                Console.Write("{0} ", num1);
                num1 += 1;
            }
            while (num1 <= num2);
        }
    }
}