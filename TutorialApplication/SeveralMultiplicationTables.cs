﻿using System;

namespace TutorialApplication
{
    class SeveralMultiplicationTables
    {
        // Prints multiplication tables from 2 to 6
        public void Main()
        {
            Console.WriteLine("");
            Multiplication(2);
            Console.WriteLine("");
            Multiplication(3);
            Console.WriteLine("");
            Multiplication(4);
            Console.WriteLine("");
            Multiplication(5);
            Console.WriteLine("");
            Multiplication(6);
        }

        // Function for multiplication
        private void Multiplication(int number)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0} * {1} = {2}", number, i+1, ((i + 1) * number));
            }
        }
    }
}