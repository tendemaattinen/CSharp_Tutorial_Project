﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software writes user input to file named "sentences.txt" which is located in My Documents -folder.
 */
 
using System;
using System.IO;

namespace TutorialApplication
{
    class WritingTextFile
    {
        public void Main()
        {
            // Try
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // Saves path of My Documents -folder
                string text;
                string[] array = new string[50]; // New array for saving strings
                int i = 0; // Counter
                Console.WriteLine();
                // Asks strings as long as user writes something
                do
                {
                    Console.Write("Enter text or leave empty to quit: ");
                    text = Console.ReadLine();
                    array[i] = text; // Saves text to array
                    i += 1; // Adds one to counter
                } while (text != "");

                // Writes all strings for array to text file using StreamWriter
                using (StreamWriter sW = new StreamWriter(Path.Combine(path, "sentences.txt")))
                {
                    foreach (string line in array)
                    {
                        if (line != "")
                        {
                            sW.WriteLine(line);
                        }
                    }
                }
            }
            // Catch error
            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
        }
    }
} 