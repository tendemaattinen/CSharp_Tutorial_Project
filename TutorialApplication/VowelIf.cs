﻿using System;

namespace TutorialApplication
{
    class VowelIf
    {
        // Checks if user gives a lowercase vowel, digit or something else with if statement
        public void Main()
        {
            while (true)
            {
                string input;
                Console.WriteLine("");
                Console.Write("Enter letter or number (0 to stop): ");
                input = Console.ReadLine();
                Console.WriteLine("");
                // Checks if input is zero, and ends application if it is
                if (input == "0")
                {
                    break;
                }
                if (input == "a" || input == "e" || input == "i" || input == "o" || input == "u")
                {
                    Print(1);
                }
                else if (input == "1" || input == "2" || input == "3" || input == "4" || input == "5" || input == "6" || input == "7" || input == "8" || input == "9")
                {
                    Print(2);
                } else
                {
                    Print(3);
                }
            }
        }

        // Function for printing sentence
        private void Print(int number)
        {
            if (number == 1)
            {
                Console.WriteLine("It's a lowercase vowel.");
            }
            if (number == 2)
            {
                Console.WriteLine("It's a digit.");
            }
            if (number == 3)
            {
                Console.WriteLine("It's another symbol.");
            }
        }
    }
}