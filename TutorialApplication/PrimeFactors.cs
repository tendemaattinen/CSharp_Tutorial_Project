﻿using System;

namespace TutorialApplication
{
    class PrimeFactors
    {
        // Asks number and shows it as a product of its prime factors
        public void Main()
        {
            int d = 2;
            Console.WriteLine("");
            Console.Write("Enter number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            while (number > 1)
            {
                while (number % d == 0)
                {
                    Console.Write(d);
                    Console.Write(" * ");
                    number = number / d;
                }
                d++;
            }
        }
    }
}