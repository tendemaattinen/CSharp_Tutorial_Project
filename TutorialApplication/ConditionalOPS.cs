﻿using System;

namespace TutorialApplication
{
    class ConditionalOPS
    {
        // Checks if numbers are positive with conditional (?) operator
        public void Main()
        {
            Console.WriteLine();
            Console.Write("Enter the first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Enter the first number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            Console.WriteLine((number1 > 0) ? "The first number is positive" : "The first number is not positive");
            Console.WriteLine((number2 > 0) ? "The second number is positive" : "The second number is not positive");
            Console.WriteLine((number2 > 0) && (number1 > 0) ? "Both numbers are positive" : "Not both numbers are positive");
        }
    }
}