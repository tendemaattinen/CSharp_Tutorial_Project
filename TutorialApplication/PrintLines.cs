﻿using System;

namespace TutorialApplication
{
    class PrintLines
    {
        public void Main()
        {
            /* Just prints two lines to console */
            Console.WriteLine("");
            Console.WriteLine("Hello");
            Console.WriteLine("Jonny");
        }
    }
}