﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software writes date and user input to text file named "myLog.txt". It is located in "My Documents" -folder.
 */
 
using System;
using System.IO;

namespace TutorialApplication
{
    // Logger-class
    class Logger
    {
        // Method to write to text file
        public void Write(string file, string text)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // Path to "My Documents" -folder
                path = path + "\\" + file; // Adds files name to path
                if (!File.Exists(path)) // Checks if file exists
                {
                    using (StreamWriter sW = new StreamWriter(path))
                    {
                        sW.WriteLine(DateTime.Now + " - " + text);
                    }
                }
                else // File exists
                {
                    using (StreamWriter sW = File.AppendText(path))
                    {
                        sW.WriteLine(DateTime.Now + " - " + text);
                    }
                }
            }
            // Catches error
            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
        }
    }

    // Class for testing Logger-class, includes Main-function
    class LoggerTest
    {
        public void Main()
        {
            Console.WriteLine();
            Logger log = new Logger(); // New Logger
            bool swi = true;
            while (swi) // Loop for chance to write multiple times
            {
                Console.Write("Enter a text or '0' to quit: ");
                string userInput = Console.ReadLine();
                if (userInput == "0") // "0" ends software
                {
                    swi = false;
                }
                else
                {
                    log.Write("myLogger.txt", userInput);
                }
            }
           
            
        }
    }
}