﻿using System;

namespace TutorialApplication
{
    class CalculatorIf
    {
        // Basic calculator with if-clause
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Enter operation: ");
            string operation = Console.ReadLine();
            Console.Write("Enter second number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            if (operation == "+")
            {
                Console.WriteLine("{0} + {1} = {2}", number1, number2, (number1 + number2));
            }
            else if (operation == "-")
            {
                Console.WriteLine("{0} - {1} = {2}", number1, number2, (number1 - number2));
            }
            else if (operation == "*")
            {
                Console.WriteLine("{0} * {1} = {2}", number1, number2, (number1 * number2));
            } 
            else if (operation == "/")
            {
                // Checks if divided by zero
                if (number2 != 0)
                {
                    Console.WriteLine("{0} / {1} = {2}", number1, number2, (number1 / number2));
                } else
                {
                    Console.WriteLine("Cannot divide by zero!");
                }
            }
            else if (operation == "%")
            {
                // Checks if divided by zero
                if (number2 != 0)
                {
                    Console.WriteLine("{0} % {1} = {2}", number1, number2, (number1 % number2));
                }
                else
                {
                    Console.WriteLine("Cannot divide by zero!");
                }
            }
            // Operation is unknown
            else
            {
                Console.WriteLine("Operation unknown!");
            }
        }
    }
}