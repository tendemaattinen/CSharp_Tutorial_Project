﻿using System;

namespace TutorialApplication
{
    class Statistics
    {
        // Asks numbers from user and gives statistics from them, including total, amount, average, maximum and minimum
        public void Main()
        {
            double total = 0, amount = 0, average = 0;
            double maximum = 0, minimum = 0;
            while (true)
            {
                Console.WriteLine();
                Console.Write("Enter a number or '0' to quit: ");
                Double.TryParse(Console.ReadLine(), out double number);
                // Checks if user wants to quit by giving 0 as number
                if (number == 0)
                {
                    break;
                }
                total += number;
                amount += 1;
                average = total / amount;
                // Checks if given number is first
                if (maximum == 1)
                {
                    maximum = number;
                    minimum = number;
                }
                // Checks if given number is either maximum or minumum number
                else
                {
                    if (number > maximum)
                    {
                        maximum = number;
                    }
                    else if (number < minimum) {
                        minimum = number;
                    }
                }
                // Prints answers
                Console.WriteLine("Total = {0} Amount = {1} Average = {2}", total, amount, average);
                Console.WriteLine("Maximum = {0} Minimum = {1}", maximum, minimum);
            }
        }
    }
}