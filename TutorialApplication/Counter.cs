﻿using System;

namespace TutorialApplication
{
    class Counter
    {
        // Application counts to 10 in while loop
        public void Main()
        {
            int i = 0;
            Console.WriteLine("");
            while (i < 10)
            {
                i++;
                Console.Write("{0} ", i);
            }
            Console.WriteLine("");
        }
    }
}