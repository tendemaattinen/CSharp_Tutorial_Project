﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Prints Hello World and includes main frame of applications for copying purposes.
 */

using System;

namespace TutorialApplication
{
    class HelloWorld
    {
        public void Main()
        {
            Console.WriteLine("");
            Console.WriteLine("Hello World!");
        }
    }
}

/* For copying purpose

/* 
 * Author: Teemu Tynkkynen
 * Description:
 *
 
using System;

namespace TutorialApplication
{
    class ph
    {
        public void Main()
        {
            Console.WriteLine();
            
        }
    }
} 

*/
