﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application creates 'struct' to store data of 2D points and prints it.
 */

using System;

namespace TutorialApplication
{
    class StructApp
    {
        // Struct-structure for storing data
        struct Point
        {
            public short xCoordinate;
            public short yCoordinate;
            public byte r;
            public byte g;
            public byte b;
        }

        public void Main()
        {
            Point point1, point2;
            Console.WriteLine();
            Console.Write("Enter x for first point: ");
            point1.xCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter y for first point: ");
            point1.yCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter red for first point: ");
            point1.r = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter green for first point: ");
            point1.g = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter blue for first point: ");
            point1.b = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter x for second point: ");
            point2.xCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter y for second point: ");
            point2.yCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter red for second point: ");
            point2.r = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter green for second point: ");
            point2.g = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter blue for second point: ");
            point2.b = Convert.ToByte(Console.ReadLine());
            Console.WriteLine();
            DisplayPoint("Point 1", point1);
            DisplayPoint("Point 2", point2);
        }

        // Function to print data about point
        private void DisplayPoint(string nameOfThePoint, Point point)
        {
            Console.WriteLine("{0} is located in ({1},{2}), colour ({3},{4},{5})", nameOfThePoint, point.xCoordinate,
            point.yCoordinate, point.r, point.g, point.b);
        }
    }
} 