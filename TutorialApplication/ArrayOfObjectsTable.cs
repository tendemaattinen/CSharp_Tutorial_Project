﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software that contains different sizes tables
 */
 
using System;

namespace TutorialApplication
{
    // Class for table
    class Table
    {
        protected int height;
        protected int width;

        // Constructor
        public Table(int wid, int hei)
        {
            height = hei;
            width = wid;
        }

        // Method to show width and height of table
        public void ShowData()
        {
            Console.WriteLine("Width: {0} | Height: {1}", width, height);
        }
    }

    // Class with Main-function
    class TestTable
    {
        public void Main()
        {
            Console.WriteLine();
            Table[] tableArray = new Table[10]; // Array to store tables
            Random rand = new Random(); // Random

            // Loop for creating tables
            for (int i = 0; i < 10; i++)
            {
                int width = rand.Next(50, 200); // Random width
                int height = rand.Next(50, 200); // Random height
                Table table = new Table(width, height); // Creates new table with created random numbers
                tableArray[i] = table; // Adds created table to array
            }

            // Loop for printing all the tables from array
            for (int i = 0; i < 10; i++)
            {
                Console.Write("{0}) ", i + 1);
                tableArray[i].ShowData();
            }
        }
    }
} 