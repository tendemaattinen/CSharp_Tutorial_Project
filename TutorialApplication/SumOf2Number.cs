﻿using System;

namespace TutorialApplication
{
    class SumOfTwo
    {
        public void Main()
        {
            Console.WriteLine("");
            Console.WriteLine("Sum: 12 + 13 = " + (12+13));
            Console.WriteLine("Division: 24 / 5 = " + (24/5));
            Console.WriteLine("");
            Console.WriteLine("Multiple different operations:");
            Console.WriteLine("-6 + 3 * 5 = " + (-6 + 3 * 5));
            Console.WriteLine("(13 - 2) * 7 = " + ((13 - 2) * 7));
            Console.WriteLine("(5 + (-2)) * (20 / 10) = " + ((5 + (-2)) * (20 / 10)));
            Console.WriteLine("(12 + 4) / (5 - 4) = " + ((12 + 4) / (5 - 4)));
        }
    }
}