﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Software have Person-class and two inheritance of it: Teacher and Person. They have different methods.
 */
 
using System;

namespace TutorialApplication
{
        // Person class
        public class Person
        {
            protected int age;
            public void SetAge(int n)
            {
                age = n;
            }
            public void Greet()
            {
                Console.WriteLine("Hello!");
            }
        }

        // Teacher class, inheritance of Person
        public class Teacher : Person
        {
            private string subject;

            public void Explain()
            {
                Console.WriteLine("Explanation begins.");
            }
        }

        // Student class, inheritance of Person
        public class Student : Person
        {
            public void GoToClasses()
            {
                Console.WriteLine("I'm going to class.");
            }

            public void ShowAge()
            {
                Console.WriteLine("My age is: {0} years old", age);
            }
        }

        // Test class which includes Main-function
        class StudentAndTeacherTest
        {
             public void Runnable()
             {
                Console.WriteLine();
                Person person = new Person();
                person.Greet();

                Student student = new Student();
                student.SetAge(21);
                student.Greet();
                student.ShowAge();

                Teacher teacher = new Teacher();
                teacher.SetAge(30);
                teacher.Greet();
                teacher.Explain();
             }
        }
}