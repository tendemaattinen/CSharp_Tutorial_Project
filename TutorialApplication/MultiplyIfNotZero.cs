﻿using System;

namespace TutorialApplication
{
    /* 
     * Multiply users inputs if they aren't zero. If either is zero,
     * application tells that answer is always zero in that case.
     */
    class MultiplyIfNotZero
    {
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Give the first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            if (number1 != 0)
            {
                Console.Write("Give the second number: ");
                Int32.TryParse(Console.ReadLine(), out int number2);
                if (number2 != 0)
                {
                    int answer = number1 * number2;
                    Console.WriteLine("{0} * {1} = {2}", number1, number2, answer);
                } else
                {
                    Console.WriteLine("The second number is zero, answer is always 0");
                }
            } else
            {
                Console.WriteLine("The first number is zero, answer is always 0");
            }
        }
    }
}