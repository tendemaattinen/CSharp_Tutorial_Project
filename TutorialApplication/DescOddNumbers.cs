﻿using System;

namespace TutorialApplication
{
    class DescOddNumbers
    {
        // Prints odd numbers from 49 to 3 in while loop
        public void Main()
        {
            int oddNumber = 49;
            Console.WriteLine("");
            while(oddNumber >= 3)
            {
                Console.WriteLine(oddNumber);
                oddNumber = oddNumber - 2;
            }
        }
    }
}