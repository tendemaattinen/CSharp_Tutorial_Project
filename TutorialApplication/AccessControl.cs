﻿using System;

namespace TutorialApplication
{
    class AccessControl
    {
        // User creates account and tries to login to it. User have only 3 tries.
        public void Main()
        {
            int attempt = 0, trigger = 0;
            string username, password, un, pw;
            Console.WriteLine("");
            Console.WriteLine("*** Creare new account ***");
            Console.Write("Enter username: ");
            username = Console.ReadLine();
            Console.Write("Enter password: ");
            password = Console.ReadLine();
            Console.WriteLine("Account created successfully");
            Console.WriteLine("");
            // While loop counts attempts
            while(attempt < 3)
            {
                Console.Write("Enter username: ");
                un = Console.ReadLine();
                Console.Write("Enter password: ");
                pw = Console.ReadLine();
                Console.WriteLine("");
                // Successful login
                if ((un == username) && (pw == password))
                {
                    Console.WriteLine("Login successful!");
                    Console.WriteLine("");
                    trigger = 1;
                    break;
                }
                // Failed login
                else
                {
                    Console.WriteLine("Login error");
                    Console.WriteLine("");
                    attempt++;
                }
            }
            // Shows message if there were too failed attempts
            if (trigger != 1) {
                Console.WriteLine("Too many attempts, closing");
            }
        }

    }
}