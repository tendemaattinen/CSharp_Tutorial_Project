﻿using System;

namespace TutorialApplication
{
    class Triangle
    {
        // Creates triangle from user input
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter a number: ");
            Int32.TryParse(Console.ReadLine(), out int number);
            Console.Write("Enter the desired width: ");
            Int32.TryParse(Console.ReadLine(), out int width);
            // Loop for printing lines
            for (int i = width; i >= 0; i--)
            {
                // Loop for printing one line
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(number);
                }
                Console.WriteLine("");
            }

        }
    }
}