﻿using System;

namespace TutorialApplication
{
    class Exceptions
    {
        // Calculates square root of given number if user gives number, otherwise caughts error with try-catch
        public void Main()
        {
            float answer, number;
            while (true)
            {
                Console.WriteLine("");
                Console.Write("Enter a number or 0 to exit: ");
                try
                {
                    number = Convert.ToSingle(Console.ReadLine());
                    // Checks if user gives zero and wants to end application
                    if (number == 0)
                    {
                        break;
                    }
                    else
                    {
                        answer = (float)Math.Sqrt(number);
                        Console.WriteLine("The answer is {0}.", answer);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Error caught!");
                }
            }
        }
    }
}