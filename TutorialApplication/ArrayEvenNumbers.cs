﻿using System;

namespace TutorialApplication
{
    class ArrayEvenNumbers
    {
        // Application takes ten numbers from user, saves them to array and in the end shows even numbers from array
        public void Main()
        {
            Console.WriteLine();
            int[] numberArray = createArray(); // Creates array
            checkEvenFromNumbersArray(numberArray); // Checks even numbers
            Console.WriteLine();
        }

        // Function which asks numbers from user and saves them to array, returns int array
        private int[] createArray()
        {
            int[] array = new int[10]; // Array to storage numbers
            for (int i = 0; i < 10; i++) // For-loop to asks numbers ten times
            {
                Console.Write("Enter a number {0}: ", i+1);
                Int32.TryParse(Console.ReadLine(), out int number);
                array[i] = number;
            }
            return array;
        }

        // Checks even numbers
        private void checkEvenFromNumbersArray(int[] array)
        {
            foreach (int j in array) // Loop for going through the array
            {
                if ((j % 2) == 0) // Checking if remainder is 0 when dividing by 2
                {
                    Console.Write("{0} ", j);
                }
            }
        }
    }
}