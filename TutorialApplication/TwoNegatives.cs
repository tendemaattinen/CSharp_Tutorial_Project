﻿using System;

namespace TutorialApplication
{
    class TwoNegatives
    {
        // Checks if given numbers are both negatives
        public void Main()
        {
            Console.WriteLine("");
            Console.Write("Enter first number: ");
            Int32.TryParse(Console.ReadLine(), out int number1);
            Console.Write("Enter second number: ");
            Int32.TryParse(Console.ReadLine(), out int number2);
            if ((number1 < 0) && (number2 < 0))
            {
                Console.WriteLine("Are negative");
            }
            else
            {
                Console.WriteLine("Aren't negative");
            }
        }
    }
}