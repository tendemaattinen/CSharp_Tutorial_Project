﻿/* 
 * Author: Teemu Tynkkynen
 * Description: Application creates 'struct' to store data of 2D points, saves points to array and prints them. User can choose
 *              desired action from menu.
 */

using System;

namespace TutorialApplication
{
    class ArrayOfStructPlusMenu
    {
        // Struct-structure for storing data
        struct Point
        {
            public short xCoordinate; // X-coordinate
            public short yCoordinate; // Y-coordinate
            public byte r; // Red
            public byte g; // Green
            public byte b; // Blue
        }

        public void Main()
        {
            Point[] arrayOfPoints = new Point[1000]; // Array to store 'Point'-structs
            int lenghtOfArray = 0; // Count of items in the array
            bool swi = true; // Boolenan value to make loop to loop
            // Loop for menu
            while (swi)
            {
                Console.WriteLine();
                PrintMenu();
                Console.Write("Enter your choice: ");
                int.TryParse(Console.ReadLine(), out int userChoice);
                switch (userChoice)
                {
                    case 1: // User wants to add point
                        (arrayOfPoints, lenghtOfArray) = AddPointToArray(arrayOfPoints, lenghtOfArray);
                        break;
                    case 2: // User wants to display saved points
                        DisplayPoint(arrayOfPoints, lenghtOfArray);
                        break;
                    case 3: // User wants to see averages of x- and y-coordinates
                        CalculateAverage(arrayOfPoints, lenghtOfArray);
                        break;
                    case 0: // User wants to quit
                        swi = false;
                        break;
                    default: // User gives invalid input
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        // Function to print menu
        private void PrintMenu()
        {
            Console.WriteLine("*** Menu ***");
            Console.WriteLine("1) Add 2D-point");
            Console.WriteLine("2) Display all stored points");
            Console.WriteLine("3) Average values for X & Y coordinates");
            Console.WriteLine("0) Exit");
        }

        // Function to add point to array, returns Point-array and integral value of lenght of the array
        private (Point[], int) AddPointToArray(Point[] array, int lenght)
        {
            Point point;
            Console.Write("Enter x for first point: ");
            point.xCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter y for first point: ");
            point.yCoordinate = Convert.ToInt16(Console.ReadLine());
            Console.Write("Enter red for first point: ");
            point.r = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter green for first point: ");
            point.g = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter blue for first point: ");
            point.b = Convert.ToByte(Console.ReadLine());
            array[lenght] = point; // Saves point 1 to array
            lenght += 1; // Adds one to leght of array
            return (array, lenght);
        }

        // Function to print data about point
        private void DisplayPoint(Point[] array, int lenght)
        {
            if (lenght == 0) // Checks if array is emepty
            {
                Console.WriteLine("There is no saved points.");
            }
            else
            {
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    Console.WriteLine("P{0} is located in ({1},{2}), colour ({3},{4},{5})", i + 1, array[i].xCoordinate,
                    array[i].yCoordinate, array[i].r, array[i].g, array[i].b);
                }
            }
        }

        // Function to calculate averages of x- and y-coordinates
        private void CalculateAverage(Point[] array, int lenght)
        {
            if (lenght == 0) // Checks if array is empty
            {
                Console.WriteLine("There is no saved points.");
            }
            else
            {
                int xCoordinationSum = 0; // Sum of x-coordinates
                int yCoordinationSum = 0; // Sum of y-coordinates
                for (int i = 0; i < lenght; i++) // For-loop for going through the array
                {
                    xCoordinationSum += array[i].xCoordinate; // Adds value from array to sum
                    yCoordinationSum += array[i].yCoordinate; // Adds value from array to sum
                }
                Console.WriteLine("Average of x-coordinates: {0}", (xCoordinationSum/lenght));
                Console.WriteLine("Average of y-coordinates: {0}", (yCoordinationSum / lenght));
            }
        }
    }
}