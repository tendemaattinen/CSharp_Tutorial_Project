﻿using System;

namespace TutorialApplication
{
    class UseOf
    {
        // Asks three numbers from user and multiplys them.
        // Shows numbers using {0} method
        public void Main()
        {
            int number1, number2, number3, answer;
            Console.WriteLine("");
            Console.Write("Give first number: ");
            Int32.TryParse(Console.ReadLine(), out number1);
            Console.Write("Give second number: ");
            Int32.TryParse(Console.ReadLine(), out number2);
            Console.Write("Give third number: ");
            Int32.TryParse(Console.ReadLine(), out number3);
            answer = number1 * number2 * number3;
            Console.WriteLine("Answer: {0} * {1} * {2} = " + answer, number1, number2, number3);

        }
    }
}