﻿using System;

namespace TutorialApplication
{
    class AbsoluteValue
    {
        // Gives absolute value of users inpput
        public void Main()
        {
            while(true)
            {
                Console.WriteLine();
                Console.Write("Enter a number or 0 to quit: ");
                Int32.TryParse(Console.ReadLine(), out int number);
                // Checks if user wants to quit
                if (number == 0)
                {
                    break;
                }
                // If given number is negative, changes it to positive
                if (number < 0)
                {
                    number = -number;
                }
                Console.WriteLine("Absolute value is {0}", number);
            }
        }
    }
}